<!--#Include File="../Inc/Const.Asp"-->
<!--#Include File="Inc/Class_Setting.Asp"-->
<!--#Include File="Inc/Class_Diypage.Asp"-->
<%
Select Case Request("Sub")
Case "Menu"
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GB2312" />
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<style type="text/css">
<!--
body { margin: 0px; }
a { font-size: 14px; color: #50691B; text-decoration: none; border-top-color: #739726; border-right-color: #739726; border-bottom-color: #739726; border-left-color: #739726; margin:0px; font-weight: bold; }
a:hover { text-decoration: none; color: #00008C; background-color: #EEEEFF; }
-->
</style>
</head>
<body>
<table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#F2F9E8">
	<tr>
		<td><table width="95%" height="40" border="0" align="center" cellpadding="0" cellspacing="0">
				<tr>
					<td id=menus><a href="Index.Asp?Sub=Main" target="main">首页</a>
						<%If ChkLevel("setting") Then%>
						<a href="Admin_Setting.Asp" target="main">配置</a>
						<%End If%>
						<%If ChkLevel("channel") Then%>
						<a href="Admin_Channel.Asp" target="main">栏目</a>
						<%End If%>
						<%If Len(getLogin("admin","managechannel")) > 0 Then%>
						<a href="Admin_Content.Asp" target="main">内容管理</a><a href="Admin_Content.Asp?Act=Guide" target="main"><font color=red>[添加]</font></a>
						<%End If%>
						<%If ChkLevel("sitelink") Then%>
						<a href="Admin_Sitelink.Asp" target="main">链接</a>
						<%End If%>
						<%If ChkLevel("label") Then%>
						<a href="Admin_Label.Asp" target="main">标签</a>
						<%End If%>
						<%If ChkLevel("admin") Then%>
						<a href="Admin_Admin.Asp" target="main">管理员</a>
						<%End If%>
						<%If ChkLevel("upload") Then%>
						<a href="Admin_Upload.Asp" target="main">上传文件</a>
						<%End If%>
						<%If ChkLevel("diypage") Then%>
						<a href="Admin_Diypage.Asp" target="main">自定义页面</a>
						<%End If%>
						<%If ChkLevel("create") Then%>
						<a href="Admin_Createhtml.Asp" target="main">静态发布</a>
						<%End If%>
						<%If ChkLevel("tags") Then%>
						<a href="Tags.Asp" target="main">标签生成器</a>
						<%End If%>
						
						<a href="Login.Asp?Act=Logout" target="main">退出</a>  <a href="<%=Indexpath%>" target="_blank">访问首页</a></td>
				</tr>
			</table></td>
	</tr>
	<tr>
		<td height="2" bgcolor="#92CC39"></td>
	</tr>
</table>
</body>
</html>
<%
Case "Main"
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GB2312" />
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<title>WelCome</title>
<link href="Images/Style.Css" rel="stylesheet" type="text/css" />
<script language="JavaScript" type="text/javascript" src="Images/ajax.js"></script>
<script type="text/javascript" src="images/jquery-1.3.2.min.js"></script>
</head>
<body>
<%
Dim Rs,i,j
If Request("Act") = "ClsCache" Then
	Call ClsCache()
	Call ClsCache()
	Dim U
	Set U = New Cls_Setting
	Call U.Refresh()
	Set U = Nothing
	Set U = New Cls_Diypage
	Set Rs = DB("Select [ID] From [{pre}Diypage]",1)
	Do While Not Rs.Eof
	U.vID = Rs(0) : Call U.Rebuild()
	Rs.MoveNext
	Loop
	Rs.Close
End If
If Request("Act") = "Createindex" Then
	Call ClsCache()
	Call CreateIndex(1)
end if
%>
<div id="getVersion" style="display:none;">
</div>
<table width=100% border=0 cellpadding=3 cellspacing=1 class=css_table bgcolor='#E1E1E1'>
	<tr class=css_menu>
		<td colspan=3><table width=100% border=0 cellpadding=4 cellspacing=0 class=css_main_table>
				<tr>
					<td class=css_main><a href=#>系统信息</a> <span style="color:Red" id="announce"></span></td>
					<td class=css_main><div align="right"><%if createhtml=1 or Createhtml = 3 then%><a href="Index.Asp?Sub=Main&Act=Createindex" title="重新生成首页">更新首页</a> <%end if%><a href="Index.Asp?Sub=Main&Act=ClsCache" title="更新缓存同时更新自定义页面">更新缓存</a> <a href="http://www.5ucms.org/" target="_blank">技术支持</a></div></td>
				</tr>
			</table></td>
	</tr>
	<tr>
		<td class="css_col12" width="33%">文章总数：<%=DB("Select Count([ID]) From [{pre}Content]",1)(0)%></td>
		<td class="css_col12">当前版本：v <%=SysVersion%> &nbsp;<%=Response.Charset%></td>
		<td class="css_col12" width="33%">系统信息：<span id="getVersionhelp">正在查询，请稍等...</span></td>
	</tr>
	<tr>
		<td class="css_col11">浏览模式：<%Select Case CreateHTML:Case 0 : Response.Write "Asp" : Case 1 : Response.Write "HTML" : Case 2 : Response.Write "Rewrite" : Case 3 Response.Write "Asp & HTML" : End Select%></td>
		<td class="css_col11">标题拼音：<%=IIF(Autopinyin=1,"启用","禁用")%></td>
		<td class="css_col11">远程抓图：<%=IIF(Remotepic=1,"启用","禁用")%></td>
	</tr>
</table>
<table width="100%" border="0" cellpadding="3" cellspacing="1" class="css_table" bgcolor='#E1E1E1'>
	<tr class="css_menu">
		<td colspan="3"><table width="100%" border="0" cellpadding="4" cellspacing="0" class="css_main_table">
				<tr>
					<td class="css_main"><a href="#">蜘蛛统计</a></td>
				</tr>
			</table></td>
	</tr>
	<%
	j = 1
	Set Rs = DB("Select [BotName],[LastDate] From [{pre}Bots] Order By [LastDate] Desc",1)
	Do While Not Rs.Eof
	j = j + 1
	%>
	<tr>
		<%For i = 1 To 3%>
		<%If Not Rs.Eof Then%>
		<td class="css_col1<%=j%>"><strong><font color=#50691B><%=Rs(0)%></font></strong>&nbsp;&nbsp;<%=IIF(Month(Rs(1))=Month(Date) And Day(Rs(1))=Day(Date),Rs(1) & " <font color=red size=1>New</font>",Rs(1))%></td>
		<%Else%>
		<td class="css_col1<%=j%>">&nbsp;</td>
		<%End If%>
		<%If Not Rs.Eof Then Rs.MoveNext%>
		<%Next%>
	</tr>
	<%
	If j >=2 Then j = 0
	Loop
	Rs.Close : Set Rs = Nothing
	%>
</table>
<table width="100%" border="0" cellpadding="3" cellspacing="1" class="css_table" bgcolor='#E1E1E1'>
	<tr class="css_menu">
		<td colspan="3"><table width="100%" border="0" cellpadding="4" cellspacing="0" class="css_main_table">
				<tr>
					<td class="css_main"><a href="#">插件信息</a></td>
				</tr>
			</table></td>
	</tr>
	<%
	Dim PlusName,Plus
	PlusName = Split(getplus,"/")
	for i = 0 to ubound(PlusName)
		Set Plus = New Cls_Plus
		Call Plus.Open(PlusName(i))
		If Plus.ErrorCode = 0 Then
			If ChkLevel("setting") Or ChkManagePlus(PlusName(i)) Then
				j = j + 1
				%>
	<tr>
		<td class=css_col1<%=j%>><table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="100">
					<%If ChkLevel("setting") Then%>
						<a href=Admin_PlusSetting.Asp?Name=<%=PlusName(i)%>><font style=" font-size: 14px; color: #666;" ><%=Plus.Main("name")%> <font style="font-size: 12px; color: red;">配置</font></font></a>
					<%Else%>
						<font style=" font-size: 14px; color: #666;" ><%=Plus.Main("name")%></font>
					<%End If%>
					</td>
					<td><%=IIF(Plus.Config("state")=0 ,"<font color=#999999>禁用</font>" ,"<font color=#669933>启用</font>")%>
						<%
						If Len(Plus.Main("managepage")) > 0 And Plus.Config("state")=1 Then Response.Write " <a href=../Plus/" & PlusName(i) & "/" & Plus.Main("managepage") & "><font color=#669933>管理</font></a>"
						If DB("Select [ID] From [{pre}Plus] Where [Name]='" & PlusName(i) &"'",1).Eof Then
							If (Len(Plus.Main("install")) > 5 Or Len(Plus.Main("uninstall")) > 5) And ChkLevel("setting") Then Response.write " <span id='plus" & PlusName(i) & "'><a href=# onclick=""if(confirm('您确定要安装此插件吗?')){PlusInstall('" & PlusName(i) & "');}""><font color=#669933>安装</font></a></span>"
						Else
							
							If (Len(Plus.Main("install")) > 5 Or Len(Plus.Main("uninstall")) > 5) And ChkLevel("setting") Then Response.write " <span id='plus" & PlusName(i) & "'><a href=# onclick=""if(confirm('您确定要卸载此插件吗?\n\n卸载后请删除模板里关于本插件的相关代码!\n\n除非你很了解本插件,否则我推荐你禁用本插件而非卸载!')){if(confirm('确定要卸载吗?\n\n卸载后数据不可恢复!')){PlusUnInstall('" & PlusName(i) & "');}}""><font color=#999999>卸载</font></a></span>"
						End If

						%></td>
				</tr>
			</table>
		</td>
		<td class=css_col1<%=j%>><table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td><a href=<%=Plus.Main("authorurl")%> target=_blank><font style=" font-size: 14px; color: #666;" ><%=Plus.Main("author")%></font></a> (<font color=#777777><%=Replace(Plus.Main("authorurl"),"Http://","",1,1,1)%>)</font> </td>
					<td width=90 align=right>版本: <%=Plus.Main("version")%></td>
				</tr>
			</table>
		</td>
		<td class=css_col1<%=j%>><%=Plus.Main("description")%> </td>
	</tr>
	<%
			End If
			If j >=2 Then j = 0
		End If
	Next
	%>
</table>
<table width="100%" border="0" cellpadding="3" cellspacing="1" class="css_table" bgcolor='#E1E1E1'>
	<tr class="css_menu">
		<td colspan="3"><table width="100%" border="0" cellpadding="4" cellspacing="0" class="css_main_table">
				<tr>
					<td class="css_main"><a href="#">环境信息</a></td>
				</tr>
			</table></td>
	</tr>
	<tr>
		<td class='css_col12'><div align='left' width="33%">服务器IP地址：<%=Request.ServerVariables("LOCAL_ADDR")%></div></td>
		<td class='css_col12'><div align='left'>脚本解释引擎：<%=ScriptEngine & "/" & ScriptEngineMajorVersion & "." & ScriptEngineMinorVersion & "." & ScriptEngineBuildVersion & "," & "JScript/" & GetJVer()%></div></td>
		<td class='css_col12'><div align='left' width="33%">服务器Session数量：<%=Session.Contents.Count%></div></td>
	</tr>
	<tr>
		<td class='css_col11'><div align='left'>服务器当前时间：<%=Now()%></div></td>
		<td class='css_col11'><div align='left'>站点物理路径：<%=Request.ServerVariables("APPL_PHYSICAL_PATH")%></div></td>
		<td class='css_col11'><div align='left'>IIS版本名称：<%=Request.ServerVariables("SERVER_SOFTWARE")%></div></td>
	</tr>
	<%
	Dim InstallObj(6)
	InstallObj(0) = "ADODB.Connection"
	InstallObj(1) = "Scripting.FileSystemObject"
	InstallObj(2) = "ADODB.Stream"
	InstallObj(3) = "Microsoft.XMLHTTP"
	InstallObj(4) = "Persits.Jpeg"
	InstallObj(5) = "JMail.SmtpMail"
	InstallObj(6) = "JRO.JetEngine"
	For i = 0 To Ubound(InstallObj)-2
		Response.Write "  <tr>" & Vbcrlf 
		Response.Write "    <td class='css_col1" & ((i+1) Mod 2 ) + 1 & "'><div align='left'>" & InstallObj(i) & "："
		If IsObjInstalled(InstallObj(i)) Then Response.Write "<font color='green'>√</font>" Else Response.Write "<font color='red'>×</font>"
		Response.Write "</div></td>" & Vbcrlf 
		Response.Write "    <td class='css_col1" & ((i+1) Mod 2 ) + 1 & "'><div align='left'>" & InstallObj(i+1) & "："
		If IsObjInstalled(InstallObj(i+1)) Then Response.Write "<font color='green'>√</font>" Else Response.Write "<font color='red'>×</font>"
		Response.Write "</div></td>" & Vbcrlf 
		Response.Write "    <td class='css_col1" & ((i+1) Mod 2 ) + 1 & "'><div align='left'>" & InstallObj(i+2) & "："
		If IsObjInstalled(InstallObj(i+2)) Then Response.Write "<font color='green'>√</font>" Else Response.Write "<font color='red'>×</font>"
		Response.Write "</div></td>" & Vbcrlf 
		Response.Write "  </tr>" & Vbcrlf
		i=i+2
	Next
	%>
</table>
<table width="400" height="40" border="0" align="center">
	<tr>
		<td><div align="center">Copyright 2006-2015 </div></td>
	</tr>
</table>
<script language="JScript" runat="server">
function GetJVer(){return ScriptEngineMajorVersion() + "." + ScriptEngineMinorVersion() + "." + ScriptEngineBuildVersion();}
</script>
<script>function Get_Notice(){var remoteUrl="http://www.5ucms.org/jiyan/index.asp";jQuery.getScript(remoteUrl, function(){var boxInfoWrapper=jQuery('#announce');boxInfoWrapper.html(www_5ucms_org.title)});}Get_Notice();if(document.getElementById("announce").innerHTML.length==0){document.getElementById("announce").innerHTML='欢迎您使用5ucms后台！祝您心情愉快！';}
</script>
<script>function getVersionhelp(){var remoteUrl="http://oa.b8bx.com/svc/version.asp?char=<%=Response.Charset%>&ver=<%=SysVersion%><%=DataBaseType%>&ip=<%=Request.ServerVariables("LOCAL_ADDR")%>";jQuery.getScript(remoteUrl, function(){var boxInfoWrapper=jQuery('#getVersionhelp');boxInfoWrapper.html(www_5ucms_org.title)});}getVersionhelp();if(document.getElementById("getVersionhelp").innerHTML.length==0){document.getElementById("getVersionhelp").innerHTML='祝君商祺！';}</script>
<% 
If IsObject(Conn) Then Conn.Close : Set Conn = Nothing%>
</body>
</html>
<%
Case Else
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GB2312">
<title>系统后台 - 无忧网络文章管理系统</title>
<%Call ChkLogin("login")%>
</head>
<frameset rows="42,*" name="bodyFrame" id="bodyFrame" frameborder="NO" border="0" framespacing="0">
	<frame src="Index.Asp?Sub=Menu" name="menu" id="menu" scrolling="yes" marginwidth="0" marginheight="0">
	<frame src="Index.Asp?Sub=Main" name="main" id="main" scrolling="yes" marginwidth="0" marginheight="0">
</frameset>
<noframes>
<body>
你的浏览器不支持框架！
</body>
</noframes>
</html>
<%End Select%>
