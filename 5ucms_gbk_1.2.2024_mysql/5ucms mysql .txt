环境支持 WIN2003 32位系统 其他未经测试 不保证可用

1 安装Microsoft Visual C++ 2010
2 安装my odbc 5.3.4

配置它
====================================== 
以下为修改方法，如果你以前的5U想改成MYSQL 可参考之
======================================
后台 各 admin_xxx.asp 文件均打开  凡是含有Set Rs = DB(Sql,2) 都改为 Set Rs = DB(Sql,3)
admin_admin.asp 将 If AdminCount > 1 Then 改为 
If int(Trim(AdminCount)) > 1 
========================================
ind/function.asp

15行改为

connstr = "Driver={MySQL OdbC 5.3 Unicode Driver};Server=" & databaseserver & ";database=" & databasename & ";User=" & databaseuser & ";Password=" & databasepass & ";option=3;"

50行下方 mysql 的 case 部分改为

sqlstr = replace(sqlstr, "{date}", "CURRENT_DATE()")
sqlstr = replace(replace(sqlstr, "[", "`"), "]", "`")
if instr(lcase(sqlstr), "select top") > 0 then 
dim reg : set reg = new regexp : reg.ignorecase = true : reg.global = true '正则搜索 不区分大小写 
reg.pattern = "(select top )([0-9]+)" 
set toplimit = reg.execute(sqlstr)  '执行搜索
if toplimit.count > 0 then '如果存在select top值 则 
sqlstr = replace(sqlstr, toplimit(0), "select ") '去掉top 语句 因为mysql不支持select top 但支持 

select 
                maxlimit = replace(toplimit(0),"Top","top") '将Top 换为 top
limitstr = split(maxlimit,"top") '获取top 右侧的数字
maxlimit = trim(limitstr(1)) '去掉数字边的空格
sqlstr = sqlstr & " limit 0,"&maxlimit '设置mysql的limit语句，即 搜索的ID范围 组合为正确的mysql语句
end if 
end if  
sqlstr = sqlstr & ";"

965行改为

Public function saveconfig()
dim rs, vLanguage, vupdatepath, config,qss2
vLanguage = "zh-cn"
vupdatepath = false
config = "<" & "%@LANGUAGE=""VBSCRIPT"" CODEPAGE=""936""%" & ">"
config = config & vbcrLf & "<" & "%"
config = config & vbcrLf & "option explicit"
config = config & vbcrLf & "response.charset = ""GB2312"""
config = config & vbcrLf & "session.codepage = 936"
config = config & vbcrLf & "session.timeout = 1440"
config = config & vbcrLf & "server.scripttimeout = 9999"
config = config & vbcrLf & vbcrLf & "dim scriptstart"
config = config & vbcrLf & "    scriptStart = timer()"
config = config & vbcrLf & vbcrLf & "dim dataquery"
config = config & vbcrLf & "    dataquery = 0"
set rs = db("select [Title],[Name],[value],[Data] From [{pre}config] Order By [Order] Asc", 1)
Do while not rs.eof
config = config & vbcrLf & vbcrLf & "dim " & lcase(rs(1))
qss2 = rs(2)
select case lcase(rs(3))
case "text"
config = config & vbcrLf & "    " & lcase(rs(1)) & " = """ & replace(replace(qss2, """", """"""), vbcrLf, "") & """"
case "int"
config = config & vbcrLf & "    " & lcase(rs(1)) & " = " & qss2
end select
 

if lcase(rs(1)) = "language" then vLanguage = replace(replace(qss2, """", """"""), vbcrLf, "")
if lcase(rs(1)) = "createhtml" then if cstr(qss2) <> cstr(Createhtml) then vupdatepath = true ' 清空VIEWPATH
if lcase(rs(1)) = "seodir" then if cstr(qss2) <> cstr(seodir) then vupdatepath = true ' 清空VIEWPATH
if lcase(rs(1)) = "httpurl" then if lcase(qss2) <> httpurl then vupdatepath = true ' Clear viewpath
if lcase(rs(1)) = "installdir" then installdir = qss2 ' 更新安装DIR
rs.movenext
Loop
rs.close: set rs = nothing
config = config & vbcrLf & vbcrLf & "dim aspjpegobj"
config = config & vbcrLf & "	aspjpegobj = " & IsobjInstalled("Persits.Jpeg")
config = config & vbcrLf & "%" & ">"
config = config & vbcrLf & "<" & "!--#include file=""language/" & vLanguage & ".asp""--" & ">"
call db("update [{pre}content] set [Filepath]='',[Viewpath]=''", 0)
call createfile(config, installdir & "inc/config.asp")
end function

==================================
inc/class_dataList.asp

58行改为
RecordCount 改为 Int(CStr(RecordCount))
变成 If Int(CStr(RecordCount)) < PageSize Then Data = Rs.GetRows(RecordCount) Else Data = Rs.GetRows(PageSize)

==================================
admin/admin_createhtml.asp
56行改为 增加了Int
Session("Channel-PageCount-" & Rs(0)) = Abs(Int(0-Int(CStr((Ns(0)))/PageSizes))) : Ns.Close : Set Ns = Nothing '# 当前栏目所有分页数量

83行改为增加了CStr
If IsNumeric(Session("NeedCreate")) And CStr(Session("NeedCreate")) > 1 Then Response.Write "<br /><br /><a href=""Admin_Createhtml.Asp?AutoChannel=Yes"" target=""main"">你还有 " & Session("NeedCreate")-1 & " 个栏目需要生成,点击这里生成下一个栏目!</a><meta http-equiv=""refresh"" content=""3;URL=Admin_Createhtml.Asp?AutoChannel=Yes""/>"	 '# 更新单个栏目完成时的

120行改为增加int(CStr(
Response.Write "本次更新任务共需要更新文章 " & C_Count & " 篇; 还有 " & Round(int(CStr(C_Count))/N)+1 - Createi & " 个批次需要处理;<br />"
?