<%
'140123 解决小于0的小数显示时，前边不带0的bug
Function fmtnumber(nums)
 nums = cstr(nums)    '转换为字符
 if instr(nums,".") > 0 then  '如果中间有点号就说明是数字
  if left(nums,1) = "." then '如果截取的第一个符号是点号，就说明点号前面的0被省略了，那下面就是添加一个 例: .1218
   nums = "0" & nums
  else
   nums = nums    '这里验证就是第一个符号不是点号，就直接显示数据 例：19.1218  00.1218
  end if
 else
  nums = nums     '没有点号直接显示数据 例:191218
 end if
 fmtnumber = nums
end function


%>