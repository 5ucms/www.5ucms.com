<%@LANGUAGE="VBSCRIPT" CODEPAGE="936"%>
<%
option explicit
response.charset = "GB2312"
session.codepage = 936
session.timeout = 1440
server.scripttimeout = 9999

dim scriptstart
    scriptStart = timer()

dim dataquery
    dataquery = 0

dim language
    language = "zh-cn"

dim webname
    webname = "5ucms"

dim installdir
    installdir = "/"

dim templatedir
    templatedir = "template"

dim indexname
    indexname = "首页"

dim indexview
    indexview = "/"

dim indextemplate
    indextemplate = "index_qss.htm"

dim indexpath
    indexpath = "/"

dim httpurl
    httpurl = ""

dim defaultext
    defaultext = "html"

dim sitepathsplit
    sitepathsplit = " > "

dim picwatermarkimg
    picwatermarkimg = "/inc/images/watermark.gif"

dim picwatermarkalpha
    picwatermarkalpha = 0.5

dim picwatermarktype
    picwatermarktype = 0

dim createhtml
    createhtml = 0

dim maxpagenum
    maxpagenum = 0

dim autopinyin
    autopinyin = 0

dim getenglishstate
    getenglishstate = 0

dim remotepic
    remotepic = 1

dim indexpicmode
    indexpicmode = 1

dim prenextmode
    prenextmode = 1

dim changdiyname
    changdiyname = 1

dim descriptionupdate
    descriptionupdate = 1

dim seodir
    seodir = 0

dim templatecache
    templatecache = 1

dim cacheflag
    cacheflag = "96B723"

dim cachetime
    cachetime = 60

dim rewriteext
    rewriteext = ".html"

dim rewritechannel
    rewritechannel = "channel"

dim rewritecontent
    rewritecontent = ""

dim qsscharset
    qsscharset = 0

dim jumptime
    jumptime = -1

dim indexkeywords
    indexkeywords = "网站名称"

dim indexdescription
    indexdescription = "网站名称"

dim author
    author = "作者"

dim source
    source = "来源"

dim aspjpegobj
	aspjpegobj = True
%>
<!--#include file="language/zh-cn.asp"-->