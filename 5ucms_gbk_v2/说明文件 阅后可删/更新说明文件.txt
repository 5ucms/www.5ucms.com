本版本由v1.2.2024改进而来，暂时不支持从旧版本升级 
如需全新安装，请删除根目录下/inc/lock/目录
如果uploadfile目录下存在.mdb文件 也需删除
然后执行 如 http://www.5ucms.com/install.asp
===============================================
如需付费升级您的其他任意版本5U至本版本 请联系Q3876307
===============================================
151125 v2.15.1125发布
一、增加内容页模板调用 栏目关键词和描述的标签
用法：{field:cdescription}
{field:ckeywords}
覆盖：/inc/class_content.asp
-----------------------
151014 v2.15.1029 发布
-----------------------
一、增加带0小数控制 变成2位小数，若小于0，则左方显示0
用法：{f:xxx  $function=formatnumber}
覆盖：/inc/class_template.asp
修改了第568行，如需其他长度小数，则自行改这里
-----------------------
151014 v2.15.1027 发布
-----------------------
一、配置中增加一项设定文章跳转时间的功能 同时简化了预览页功能
旧站请在数据库 config表内手工依次加入一行作为新数据
----------
内容跳转时间 jumptime 2 int input 内容页跳转时间，填0则立即跳转，填-1则不跳转。 75
----------
覆盖 inc/class_content.asp
覆盖 /r.asp
覆盖 管理目录/admin_content.asp
-----------------------
151014 v2.15.1020 发布
-----------------------
一、增加伪静态模式下允许生成静态首页的功能
覆盖 管理目录/index.asp
覆盖 管理目录/admin_createhtml.asp
覆盖 inc/function.asp
-----------------------
151014 v2.15.1014 发布
-----------------------
一、增加文章关键词提取到搜索结果中的功能
覆盖plus/search/ 在后台搜索插件管理中查看随机调用方法
二、增加调用文章关键词 数字链接的功能
覆盖 inc/class-content.asp
覆盖 管理目录/inc/class-content.asp
使用 {field:tags} 限购买过收录暴增插件的同学使用 否则无用处
-----------------------
150701 v2.15.0922 发布
-----------------------
一、修复一个安装bug
覆盖inc/install即可
二、改进一个批量加栏目的效率
覆盖 管理目录/inc/class_channel.asp 即可
-----------------------
150701 v2.15.0813 发布
-----------------------
一、修复自定义页 使用模板时 建立asp文件 插入<!--#include file="inc/const.asp"-->出错的问题
覆盖 管理目录/inc/class_diypage.asp
-----------------------
150701 v2.15.0727 发布
-----------------------
一、新增栏目跳转功能
覆盖 /template/tiao.html
覆盖 管理目录/admin_channel.asp
使用方法见栏目 修改 下方 关键词处
-----------------------
150701 v2.15.0701 发布
-----------------------
一、新增标签导入导出功能 方便更换模板
覆盖以下文件 管理目录/admin_lable.asp 
具体说明见后台 标签管理页
-----------------------
150629 v2.15.0629 发布
-----------------------
一、利用伪静态支持模板防盗
新增文件 根目录下 404.asp 里边模板防盗提示可改
httpd.ini 这个用记事本打开 可修改默认模板目录
默认模板为template目录时 不用改 否则改下
空间支持伪静态时可用 目前只支持ini格式伪静态
其他格式欢迎补充
-----------------------
150627 v2.15.0627 发布
-----------------------
一、站内搜索插件增加了批量删除和批量添加关键词功能
覆盖以下文件 /plus/search/_manage.asp
/plus/search/inc/_manage.js
页面上增加调用例子，方便大家做tags页
比如新建立一个自定义文件
/tags.html 里边用所给的例子
-----------------------
150616 v2.15.0616 发布
-----------------------
一、增加laydate JS日期控件
覆盖以下文件    /inc/js/lyadate 目录
		管理目录/admin_content.asp 
已在文章日期处有调用
----------------------
150615 v2.15.0615 发布 
----------------------
1、修复全新安装时使用mssql提示出错的问题
更新方法：覆盖根目录下的 inc/function.asp
更新必要：一般（覆盖即可）
----------------------
150603 v2.15.0610 发布 
----------------------
1、支持 [list:j] 标签 值同等于 i-1 即会从0开始计数
2、支持 {f:fid} 标签 获取当前栏目或内容页的父栏目ID
更新方法：覆盖根目录下的 inc/class_template.asp
更新必要：一般（覆盖即可）
----------------------
150603 v2.15.0603 发布 
----------------------
1、支持 [list:i+1] 或 [list:i-5] 数字任意
更新方法：覆盖根目录下的 inc/class_template.asp
更新必要：一般（覆盖即可）
----------------------
150531 v2.15.0531 发布 
----------------------
1、解决utf-8编码时 动态模式下可能乱码的问题
更新方法：覆盖根目录下的index.asp channel.asp content.asp即可
更新必要：建议（覆盖即可）
----------------------
150528 v2.15.0528 发布 
----------------------
1、解决utf-8编码时 无法生成静态的问题
更新方法：覆盖 inc/class_template.asp
更新必要：建议（覆盖即可）
----------------------
150518 v2.15.0518 发布 
----------------------
1、支持选择编码功能，当模板编码是utf8时，在后台配置中选择utf-8，前台不会乱码
2、不要乱选哦，不然会乱码。
3、使用模板管理插件时，发现中文乱码时 请不要保存它。先在配置里修改后，再保存。
更新方法：覆盖 inc/function.asp inc/class_template.asp
更新必要：一般（做新网站时用）
----------------------
150510 v2.15.0510 发布 
----------------------
1、修复后台清理上传文件出错的bug

更新方法：覆盖 管理后台目录/admin_upload.asp
更新必要：建议（不更新影响文件清理功能）
----------------------
150506 v2.15.0506 发布 
----------------------
1、优化后台内容管理下方 按栏目选择文章 功能 看起来更方便
2、优化上传文件功能，如果文章没有标题，上传文件时，会将图片标题作为文章标题。

更新方法：覆盖 管理后台目录/admin_content.asp 管理后台目录/upload.asp
更新必要：一般（可以不更新，没什么影响）
----------------------
150421 v2.15.0421 发布 
----------------------
1、增加自定义页面的 当前地址标签 可以用{field:dir} 或 {field:aurl} 其中 field 可 简写为 f
2、增加了安装功能
 
更新方法：覆盖 管理后台目录/inc/class_diypage.asp 
更新必要：一般（可以不更新，没什么影响）
----------------------
150329 v2.15.0408 发布 
----------------------
1、修改 设定栏目模板时，大类只显示大类模板，小类只显示小类和内容模板的设置项
2、修改 栏目设置模板时 可以自动读取相应的栏目以供选择
3、修复 自定义页面 下拉选择common模板时 会有一个空白项的问题
4、支持此类标签 {curl:2}  {cname:23} {title:53} {aurl:123} 直接调用数字ID相关的栏目名 地址 文章名 地址
5、将更新缓存的按钮放在右上角了 更方便 更新结果会显示在左下角

更新方法：覆盖 管理后台目录/channel.asp 管理后台目录/inc/class_channel.asp 管理后台目录/index.asp 管理后台目录/admin_main.asp 管理后台目录/admin_refresh.asp
          根目录/inc/function.asp文件
          根目录/inc/template.asp文件
更新必要：一般（可以不更新，没什么影响）
----------------------
150329 v2.15.0329 发布 
----------------------
1、修改 批量增加模板时，按从左到右的顺序进行从大到小的排序 之前是 相反
2、后台的首页 进行登陆验证跳转 忘了加验证了 不过不登陆也不能操作啥 不显示菜单的

更新方法：覆盖 管理后台目录/inc/class_channel.asp文件
更新必要：一般（可以不更新，没什么影响）
----------------------
150323 v2.15.0326 发布 
----------------------
1、修复了自定义字段不能使用的bug
2、修复了插件配置更新后 页面跳转错误的bug
3、增加了插件开发时 可以使用自定义common文件的功能
格式 plus.newtpl("modpw.html$kuan")  则可嵌套入 common_kuan.html内 如不加$kuan 则默认用common.html
4、分类中会根据情况显示需要修改的模板框 隐藏不需要修改的模板框

更新方法：覆盖inc和 管理后台目录 目录
更新必要：一般（可以不更新，没什么影响）
----------------------
150323 v2.15.0323 发布 
----------------------
特色说明：
后台借鉴zblog样式
后台操作记录功能
编辑器用KINDEDITOR 兼容主流浏览器
内容管理支持15 30 50 100条信息切换分页
栏目支持批量添加