<!--#Include File="../inc/Const.Asp"-->
<!--#Include File="Inc/Class_Content.Asp"-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<title>内容管理</title>
<script language="JavaScript" type="text/javascript" src="Images/ajax.js"></script>
<script src="../inc/js/laydate/laydate.js"></script>
<link href="Images/Style.Css" rel="stylesheet" type="text/css" />
</head>
<body>
<%

If Len(Request.Form("oContent")) > 0 And Request("Act") = "UpDate" Then Session("Content_IN_Cache") = Request.Form("oContent")

Dim U
Set U = New Cls_Content

'qpage获取分页数
dim qpage
	qpage = Request("qpage")
	if qpage = "" or Not IsNumeric(qpage) then
		if len(Session("qpage"))>0 and IsNumeric(Session("qpage")) then
			qpage = Session("qpage")
		else
			qpage = 15 '设置默认分页数量
			Session("qpage") = 15
		end if
	else
		Session("qpage") = qpage
	end if
'qpage结束

Dim Rs,Ns,Sql,Style,i
Dim ID
    ID = Request("ID")
Dim Key
    Key = Request("Key")
Dim Page
    Page = Request("Page")
Dim Orders
    Orders = Request("Orders")
Dim DFieldKey
    DFieldKey = Request("DFieldKey")
Dim DFieldOrders
    DFieldOrders = Request("DFieldOrders")
If len(ID) > 0 And Not IsNumeric(ID) Then : Response.Write "编号只能是数字!" : Conn.Close : Set Conn = Nothing : Set U = Nothing : Response.End '// 检测ID
If Len(Page) = 0 Or Not IsNumeric(Page) Then Page = 1
If Page < 1 Then Page = 1
If LCase(Orders) <> "asc" Then Orders = "Desc"
If Len(DFieldKey) = 0 Then DFieldKey = "Title"
If Len(DFieldOrders) = 0 Then DFieldOrders = "ID"
If Instr("[id],[cid],[title],[commend],[views],[comments],[order],[filepath],[display]" , "[" & Lcase(DFieldKey) & "]") = 0 Then Key = ""
If Instr("[id],[cid],[title],[commend],[views],[comments],[order],[filepath],[display]" , "[" & Lcase(DFieldOrders) & "]") = 0 Then DFieldOrders = "ID"
Page = Int(Page) : Key = Replace(Key,"'","")
Dim JumpUrl
    JumpUrl = "&Key=" & Server.UrlENCode(Key) & "&DFieldKey=" & DFieldKey & "&Orders=" & Orders & "&DFieldOrders=" & DFieldOrders

Select Case Request("Act")
Case "Guide"
	If Len(ID) = 0 Then
		Call U.Initialize()
		Call Main_Guide(0)
	Else
		U.vID = ID
		Call U.SetValue()
		Call Main_Guide(1)
	End If
Case "UpDate"
	If U.GetValue() Then
		If Len(ID) = 0 Then
			If Not U.Create() Then
				Alert U.LastError,"Admin_Content.Asp?Page=" & Page & JumpUrl
			Else
				Alert "","Admin_Content.Asp?Page=" & Page & JumpUrl
			End If
		Else
			U.vID = ID
			If Not U.Modify() Then
				Alert U.LastError,"Admin_Content.Asp?Page=" & Page & JumpUrl
			Else
				Alert "","Admin_Content.Asp?Page=" & Page & JumpUrl
			End If
		End If
	Else
		Alert U.LastError,"Admin_Content.Asp?Page=" & Page & JumpUrl
	End If
Case "Delete"
	If Len(ID) > 0 Then
		U.vID = ID
		If Not U.Delete() Then
			Alert U.LastError,"Admin_Content.Asp?Page=" & Page & JumpUrl
		Else
			Alert "","Admin_Content.Asp?Page=" & Page & JumpUrl
		End If
	Else
		Call Main()
	End If
Case "Change"
	If Len(ID) > 0 Then
		U.vID = ID
		If Not U.Change() Then Alert U.LastError,""
	End If
	Call Main()
Case "DOCreate"
	ID = Split(Replace(Replace(Request("ids"),"'","")," ",""),",")
	For i = 0 To Ubound(ID)
		Call CreateContent(ID(i),0)
	Next
	Call Main()
Case "DODelete"
	ID = Replace(Replace(Request("ids"),"'","")," ","")
	Call U.DeleteAll(ID)
	Call Main()
Case "DOMove"
	ID = Replace(Replace(Request("ids"),"'","")," ","")
	Call U.EchoMoveHtml(ID)
Case "DOMoveit"
	Dim Tmp
	Dim Tocid : Tocid = Request("Tocid")
	Call U.MoveContent(Tocid,ID)
	Call Main()
Case Else
	Call Main()
End Select

Set U = Nothing
If IsObject(Conn) Then Conn.Close : Set Conn = Nothing

Sub Main()
If Len(Key) > 0 Then
	If Instr(",ID,Cid,Commend,Views,Comments,Orders,Display,","," & DFieldKey & ",") > 0 Then
		If Len(Key) > 0 And IsNumeric(Key) Then Sql = "[" & DFieldKey & "]=" & Key & ""
	Else
		'Sql = "[" & DFieldKey & "] Like '%" & Key & "%'"
		Sql = "InStr(1,LCase([" & DFieldKey & "]),LCase('"&Key&"'),0)<>0 " '此方法替代上方方法后 支持搜索日文 否则会提示 内存溢出
	End If
End If

Dim SQLCID
If Len(getLogin("admin","managechannel")) = 0 Then
	SQLCID = "CID=0"
Else
	If Cstr(getLogin("admin","managechannel")) <> Cstr("0") Then
		SQLCID = "CID IN (" & getLogin("admin","managechannel") & ")"
	End If
End If

' 栏目权限
If Len(SQL) > 0 Then
	If LEN(SQLCID) > 0 Then SQL = SQL & " and " & SQLCID
Else
	If LEN(SQLCID) > 0 Then SQL = SQLCID
ENd If

Set Rs = New DataList
Rs.Field = "[ID],[Cid],[Title],[Commend],[Views],[Order],[Filepath],[Display],[Indexpic],[sid]"
Rs.Table = "[{pre}Content]"
Rs.Where = SQL
Rs.Order = "[" & DFieldOrders & "] " & Orders & ""
Rs.AbsolutePage = Page
Rs.List(qpage)
%>
<table width=100% border=0 cellpadding=3 cellspacing=1 class=css_table bgcolor='#E1E1E1' id="www_5ucms_org">
	<tr>
		<td colspan=8 class=css_menu><table width=100% border=0 cellpadding=4 cellspacing=0 class=css_main_table>
				
					<tr>
						<td class=css_main width="160" height="20"><a href=Admin_Content.Asp>内容管理</a> <a href=Admin_Content.Asp?Act=Guide&Page=<%=Page%><%=JumpUrl%>>添加内容</a></td> <form name=frmSearch method=post action=Admin_Content.Asp>
						<td class=css_search>

 
    选择每页内容数量为：<select name="qpage" onchange="submit();"> 
    <option value="15" >15</option>
    <option value="30" <%If qpage = 30 Then Response.Write "selected='selecte'" %>>30</option>
    <option value="50" <%If qpage = 50 Then Response.Write "selected='selecte'" %>>50</option>
    <option value="100" <%If qpage = 100 Then Response.Write "selected='selecte'" %>>100</option>
    </select>条 
              
                        
</td></form>     <form name=frmSearch method=post action=Admin_Content.Asp>   
						<td class=css_search width="200">                     
                        <select name=DFieldKey id=DFieldKey>
								<option value="ID" <%If Lcase(DFieldKey) = "id" Then Response.Write "selected='selecte'" %>>编号</option>
								<option value="Cid" <%If Lcase(DFieldKey) = "cid" Then Response.Write "selected='selected'" %>>栏目</option>
								<option value="Title" <%If Lcase(DFieldKey) = "title" Then Response.Write "selected='selected'" %>>标题</option>
								<option value="Display" <%If Lcase(DFieldKey) = "display" Then Response.Write "selected='selected'" %>>前台显示</option>
							</select>
							<input name=Key type=text id=Key size=10 value="<%=Key%>">
							<input type=submit name=Submit value=搜></td></form>
					</tr>
				
			</table></td>
	</tr>
	<form name=frm method=post action=Admin_Content.Asp>
		<tr>
			<td width="30" class='css_top'><input type="checkbox" name="chkall" id="chkall" onclick="CheckAll()" class="checkbox"></td>
			<td width="38" class='css_top'>编号</td>
			<td class='css_top'>标题</td>
			<td width="38" class='css_top'><a href='Admin_Content.Asp?Page=<%=Page%>&DFieldOrders=Views&Orders=<%If Lcase(DFieldOrders) = Lcase("Views") And Orders = "Desc" Then Response.Write "Asc" Else Response.Write "Desc" %>'>浏览</a></td>
			<td width="38" class='css_top'><a href='Admin_Content.Asp?Page=<%=Page%>&DFieldOrders=Orders&Orders=<%If Lcase(DFieldOrders) = Lcase("Orders") And Orders = "Desc" Then Response.Write "Asc" Else Response.Write "Desc" %>'>权重</a></td>
			<td width="38" class='css_top'><a href='Admin_Content.Asp?Page=<%=Page%>&DFieldOrders=Commend&Orders=<%If Lcase(DFieldOrders) = Lcase("Commend") And Orders = "Desc" Then Response.Write "Asc" Else Response.Write "Desc" %>'>推荐</a></td>
			<td width="38" class='css_top'><a href='Admin_Content.Asp?Page=<%=Page%>&DFieldOrders=Display&Orders=<%If Lcase(DFieldOrders) = Lcase("Display") And Orders = "Desc" Then Response.Write "Asc" Else Response.Write "Desc" %>'>状态</a></td>
			<td width="90" class='css_top'>管理</td>
		</tr>
        <tbody id="QQ3876307">
		<%
		If Rs.Eof Then
		%>
		<tr align="center" class=css_norecord>
			<td colspan=8>没有任何记录！</td>
		</tr>
		<%
		Else
		Dim Hivepic
		For i = 0 To UBound(Rs.Data,2)
		If Len(Rs.Data(8,i)) > 0 Then Hivepic = " <font color=#FFB9B9>图</font>" Else Hivepic = ""
		%>
		<tr>
			<td width="30" class=css_list>
            <% 
			if Rs.Data(9,i)<1 then			
			%>
            <input type="checkbox" name="ids" value="<%=Rs.Data(0,i)%>" class="checkbox">
            <%else%>
            锁定
			<%end if%>
            
            </td>
			<td width="38" class=css_list><%=Rs.Data(0,i)%></td>
			<td class=css_list><div align="left"> [<a href=Admin_Content.Asp?Key=<%=Rs.Data(1,i)%>&DFieldKey=Cid><%=GetChannel(Rs.Data(1,i),"Name")%></a>] &nbsp;<a href="Admin_Content.Asp?Act=Guide&ID=<%=Rs.Data(0,i)%>&Page=<%=Page%><%=JumpUrl%>"><%=Rs.Data(2,i)%></a> <a href="<%="../r.asp?" & Rs.Data(0,i)%>" target="_blank"><font color=#999999>预</font></a><%=Hivepic%> </div></td>
			<td width="38" class=css_list><%=Rs.Data(4,i)%></td>
			<td width="38" class=css_list><%=Rs.Data(5,i)%></td>
			<td width="38" class=css_list><span id="ContentCommend<%=Rs.Data(0,i)%>" style="cursor:hand;" onclick="ContentCommend(<%=Rs.Data(0,i)%>);"><%
			Select Case Cstr(LCase(Rs.Data(3,i)))
			Case "0" Response.Write "普通"
			Case "1" Response.Write "<font color=red>推荐</font>"
			Case Else Response.Write Rs.Data(2,i)
			End Select
			%></span></td>
			<td width="38" class=css_list><span id="ContentState<%=Rs.Data(0,i)%>" style="cursor:hand;" onclick="ContentState(<%=Rs.Data(0,i)%>);"><%
			Select Case Cstr(LCase(Rs.Data(7,i)))
			Case "0" Response.Write "<font color=blue>隐藏</font>"
			Case "1" Response.Write "显示"
			Case Else Response.Write Rs.Data(7,i)
			End Select
			%></span></td>
			<td width="90" class=css_list><input name=modify type=button onclick="location.href='Admin_Content.Asp?Act=Guide&ID=<%=Rs.Data(0,i)%>&Page=<%=Page%><%=JumpUrl%>';" value=修改>
            
            <% 
			if Rs.Data(9,i)<1 then			
			%>
            <input name=delete type=button onclick="if(confirm('您确定要删除这条记录吗?')){location.href='Admin_Content.Asp?Act=Delete&ID=<%=Rs.Data(0,i)%>&Page=<%=Page%><%=JumpUrl%>';}" value=删除>
            <%else%>
            <input name=delete2 type=button onclick="alert('此为关键文章，删除将导致某页面打不开！如果您一定要删除它，请先联系您的建站技术人员！');" value=禁删>
			<%end if%>
		  </td>
		</tr>
		<%
		Next
		End If
		%>
        </tbody>
		<tr class=css_page_list>
			<td colspan=8><%If Createhtml=1 Then%>
				<input type="button" class="inputs" onclick="ContentDo('DOCreate');" value="生成" />
				<%End If%>
				<input name="" type="button" class="inputs" onclick="ContentDo('DODelete');" value="删除" />
				<input name="" type="button" class="inputs" onclick="ContentDo('DOMove');" value="移动" />
				&nbsp;
 				
                <%dim key2:if DFieldKey="Cid" then key2 = Int(key) else key2=0%>  
                <%=SelectChannel(Key2," name='Jcid' onchange=""location.href='Admin_Content.Asp?Key='+this.options[this.selectedIndex].value+'&DFieldKey=Cid&Page=1';""  style='font-size:12px;' ",0)%> 
                
				<%=PageList(Rs.PageCount,page,Rs.RecordCount,qPage,"Admin_Content.Asp?Page={p}&Key=" & Server.UrlENCode(Key) & "&DFieldKey=" & DFieldKey & "&Orders=" & Orders & "&DFieldOrders=" & DFieldOrders)%></td>
		</tr>
	</form>
</table>

<script language="JavaScript" type="text/javascript" src="images/q3876307.js"></script>
<%
Set Rs = Nothing
End Sub

Sub Main_Guide(T)
	%>
<%If IsNull(U.vCommend) Then U.vCommend=""%> <%If IsNull(U.vIsComment) Then U.vIsComment=""%> <%If IsNull(U.vDisplay) Then U.vDisplay=""%> <%If IsNull(U.vStyle) Then U.vStyle=""%> <%If IsNull(U.vColor) Then U.vColor=""%>
	<form name='frm' method='post' action='Admin_Content.Asp?Act=UpDate&ID=<%=ID%>&Page=<%=Page%><%=JumpUrl%>'>
<table width=100% border=0 cellpadding=3 cellspacing=1 class=css_table bgcolor='#E1E1E1'>
	<tr class=css_menu>
		<td colspan=7><table width=100% border=0 cellpadding=4 cellspacing=0 class=css_main_table>
				<tr>
					<td class=css_main><a href=Admin_Content.Asp?Page=<%=Page%><%=JumpUrl%>>内容管理</a></td>
				</tr>
			</table></td>
	</tr>

		<tr>
			<td width="94" class="css_col12_content"><div align="right"> <font color=red>栏目：</font> </div></td>
			<td colspan="6" class="css_col22"><%If IsNull(U.vCid) Then U.vCid=""%> <%If Not IsNumeric(U.vCid) Then U.vCid="" Else U.vCid = Int(U.vCid)%> <%=SelectChannel(U.vCid," class='css_select' name=""oCid"" type=""text"" id=""oCid"" ",getLogin("admin","managechannel"))%> <%If IsNull(U.vSid) Then U.vSid=""%> <%If Not IsNumeric(U.vSid) Then U.vSid="" Else U.vSid = Int(U.vSid)%> <%'SelectChannel(U.vSid," class='css_select' name=""oSid"" type=""text"" id=""oSid"" ",getLogin("admin","managechannel"))%><script>$$('oCid').onchange=function(){modeext(this.value,<%=U.vID%>);};</script> 每篇内容 您都需要先选择一个栏目哦！如果没有 请到 栏目 菜单中 先添加栏目分类！<a href="http://www.5ucms.org/5ucms-jiaocheng/1152.html" target="_blank">为什么有的栏目是灰/黑色背景 不能发布文章？</a> </td>
		</tr>
		<tr>
			<td class=css_col11_content><div align="right"> <font color=red>标题：</font> </div></td>
			<td colspan="6" class=css_col21><input name="oTitle" type="text" class="css_input" id="oTitle" <%if len(u.vtitle) = 0 then%>onblur="ChkTitle();"<%End If%> value="<%=U.vTitle%>" size="70" />
				<input name="oCommend" type="checkbox" value="1" <%If U.vCommend = 1 Then Response.Write "checked='checked'"%> /> 推荐
				<input name="oIsComment" type="checkbox" value="1" <%If U.vIsComment = 1 Then Response.Write "checked='checked'"%> /> 评论
				<input name="oDisplay" type="checkbox" value="1" <%If U.vDisplay = 1 Then Response.Write "checked='checked'"%> /> 显示(不勾选则为隐藏)			</td>
		</tr>
		<tr>
			<td class=css_col12_content><div align="right"> 属性： </div></td>
			<td colspan="6" class=css_col22><table border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td><input name="oAuthor" type="text" class='css_input' id="oAuthor" value="<%=U.vAuthor%>" size="6">
							<select name="select" onchange="frm.oAuthor.value=this.value;">
								<option>清空</option>
								<%
								Author = Split(Author,",")
								For i = 0 To Ubound(Author)
								Response.Write "<option value='" & Author(i) & "' "
								If LCase(Author(i)) = LCase(U.vAuthor) Then Response.Write "selected"
								Response.Write " >" & Author(i) & "</option>"
								Next
								%>
							</select>						</td>
						<td width="10">&nbsp;</td>
						<td>来源：</td>
						<td><input name="oSource" type="text" class='css_input' id="oSource" value="<%=U.vSource%>" size="6" />
							<select name="select2" onchange="frm.oSource.value=this.value;">
								<option>清空</option>
								<%
								Source = Split(Source,",")
								For i = 0 To Ubound(Source)
								Response.Write "<option value='" & Source(i) & "' "
								If LCase(Source(i)) = LCase(U.vSource) Then Response.Write "selected"
								Response.Write " >" & Source(i) & "</option>"
								Next
								%>
							</select>						</td>
						<td width="10"></td>
						<td>浏览：</td>
						<td><input class='css_input' name="oViews" type="text" id="oViews" value="<%=U.vViews%>" size=2 />						</td>
						<td width="10">&nbsp;</td>
						<td>权重：</td>
						<td><input class='css_input' name="oOrder" type="text" id="oOrder" value="<%=U.vOrder%>" size=2 />						</td>
						<td width="10">&nbsp;</td><td><select class='css_select' name="oStyle" type="text" id="oStyle">
					<option value="" <%If U.vStyle = "" Then Response.Write "selected"%>>样式</option>
					<option value="strong" <%If U.vStyle = "strong" Then Response.Write "selected"%>>粗体</option>
					<option value="em" <%If U.vStyle = "em" Then Response.Write "selected"%>>斜体</option>
				</select></td><td width="10">&nbsp;</td>
						<td><select class='css_select' name="oColor" type="text" id="oColor">
					<option value="" <%If U.vColor = "" Then Response.Write "selected"%>>颜色</option>
					<option value="#FF0000" style="background-color:#FF0000;" <%If U.vColor = "#FF0000" Then Response.Write "selected"%>></option>
					<option value="#0000FF" style="background-color:#0000FF;" <%If U.vColor = "#0000FF" Then Response.Write "selected"%>></option>
					<option value="#00FFFF" style="background-color:#00FFFF;" <%If U.vColor = "#00FFFF" Then Response.Write "selected"%>></option>
					<option value="#FF9900" style="background-color:#FF9900;" <%If U.vColor = "#FF9900" Then Response.Write "selected"%>></option>
					<option value="#339966" style="background-color:#339966;" <%If U.vColor = "#339966" Then Response.Write "selected"%>></option>
				</select></td>
					</tr>
				</table></td>
		</tr>
		<%
		If T = 0 Then 
			Style=""
		Else
			If ChangDiyname = 1 Then Style="" Else Style=" style='display:none;'"
		End If
		%>
		<tr<%=Style%>>
			<td class=css_col11_content><div align="right"> 文件名： </div></td>
			<td colspan="6" class=css_col21><input name="oDiyname" type="text" class="css_input" id="odiyname" onblur="ChkDiyname('<%=U.vID%>');" value="<%=U.vDiyname%>" size="32" /> <a href="javascript:pinyin();" style="color:green" title="关键字设置了才能分析">自动分析</a>
				<input name="oAutopinyin" type="checkbox" id="oAutopinyin" value="Yes" <%If Autopinyin = 1 And Len(U.vTitle)=0 Then Response.Write "checked='checked'"%> />
				将标题转换成拼音 (如果填写了文件名则不转换成拼音) </td>
		</tr>
		<tr>
		  <td class=css_col12_content><div align="right"> 发布时间： </div></td>
		  <td colspan="6" class=css_col22><input name="oCreatetime" type="text" class='css_input' id="oCreatetime" value="<%=U.vCreatetime%>" size="32" onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})"/> 如修改，请注意时间格式一定要和已有格式一致</td>
		</tr>        
		<tr>
			<td class=css_col12_content><div align="right"> 关键字： </div></td>
			<td colspan="6" class=css_col22><input name="oKeywords" type="text" class='css_input' id="oKeywords" value="<%=Server.HtmlEnCode(U.vKeywords)%>" size="32" /> <a href="javascript:keywords();" style="color:green" title="对标题进行分析">自动分析</a>
				 
			 <input name="checkbox4" type="checkbox" onclick="if (this.checked) {ShowObj('trdescription');} else {HideObj('trdescription')}" value="checkbox" />
				<font color=red><%=IIf(DescriptionUpdate = 0 ,"手动","自动")%></font>编辑描述 (多个关键字请用英文状态下的逗号分隔)  </td>
		</tr>
		<tr id='trjumpurl'>
			<td class=css_col11_content><div align="right"> 跳转地址： </div></td>
			<td colspan="5" class=css_col21><input name="oJumpurl" type="text" class="css_input" id="oJumpurl" value="<%=U.vJumpurl%>" size="54" > 如需调用此字段且不想跳转，请在配置中把跳转时间设为-1</td>
			<td width="160" rowspan="3" class=css_col21><div id="indexpicimg" align="center"> <%If Len(U.vIndexpic) > 0 Then%> <img src="<%=U.vIndexpic%>" height=95 width=150 /> <%Else%> <img src="Images/nopic.gif" height=95 width=150 /> <%End If%> </div></td>
		</tr>
		<tr>
			<td class=css_col11_content><div align="right"> 形象图： </div></td>
			<td colspan="5" class=css_col21><input class='css_input' name="oIndexpic" type="text" id="oIndexpic" value="<%=U.vIndexpic%>" size=32 >
				<select name="indexpiclist" id="indexpiclist" onchange="frm.oIndexpic.value=this.value; if(this.value.length>0){indexpicimg.innerHTML='<img src=' + this.value + ' height=95 width=150 />';}else{indexpicimg.innerHTML='<img src=Images/nopic.gif height=95 width=150 />';}">
					<option value="">不指定文章形象图</option>
					<%
					If T = 1 Then
						Dim Pic
						Set Pic = DB("Select [Dir],[Ext] From [{pre}Upload] Where [Aid]=" & U.vID & " Order By [Time] Desc",1)
						Do While Not Pic.EOf
							If Instr("gif,jpg,jpeg,bmp,png",LCase(Pic(1))) Then
								Response.Write "<option value='" & Pic(0) & "' "
								If LCase(Pic(0)) = LCase(U.vIndexpic) Then Response.Write "selected"
								Response.Write " >" & Replace(LCase(Pic(0)),"/uploadfile","") & "</option>"
							End If
							Pic.MoveNext
						Loop
					End If
					%>
				</select>	<a href="javascript:Insert(frm.oIndexpic.value);" style="color:red">插入所选图片</a>			</td>
		</tr>
		<tr>
			<td class=css_col11_content><div align="right"> 上传文件： </div></td>
			<td class=css_col21><iframe name='uploadframe' src="Upload.Asp" width="372" height="24" frameborder="0" scrolling="no" border="0"></iframe> </td>
			<td class=css_col21 width=100><a href="http://www.5ucms.org/help/?1000" target="_blank"><--如何上传？</a></td>
			<td class=css_col21 width=70><center><a href="javascript:multiup();" style="color:blue">批量上传</a></center></td>
			<td width="88" class=css_col21><input <%If U.vSid = 1 Then %>onclick="alert('此功能用于锁定此关键性文章不被误删,但本文允许修改！如欲取消,请务必咨询建站专员再操作！否则由此带来的不良后果,您可能需要额外付费来恢复!（若误点击请再点一次,保持此处为打钩状态)')"<%else%>onclick="alert('此功能用于锁定此文章为关键性文章以防被误删,但本文允许修改！（需要锁定,请保持打钩状态.误操作请保持无钩状态.若不确定,请咨询客服)')"<%end if%> name="oSid" type="checkbox" value="1" <%If U.vSid = 1 Then Response.Write "checked='checked'"%> />
					禁止删除</td>
			<td width="88" class=css_col21><div align="center">
					<input name="oRemotepic" type="checkbox" id="oRemotepic" value="Save" <% If Remotepic = 1 Then Response.Write "checked='checked'"%> />
					远程抓图 </div></td>
		</tr>
   
		<tr>
			<td class=css_col11_content><div align="right"> 描述： </div></td>
			<td colspan="6" class=css_col21><div  style='display:none;' id="trdescription"><textarea name="oDescription" style="width:99.2%;" rows="3" class='css_textarea' id="oDescription" type="text"><%If DescriptionUpdate = 0 Then Response.WRite U.vDescription%></textarea>	</div></td>
		</tr> 
        
		<tr>
			<td class=css_col11_content><div align="right"> 扩展属性：<br /></div></td>
			<td colspan="6" class=css_col21 id="_modeindex" style="padding:0px 0px 5px 5px;"></td><script>modeext($$('oCid').value,<%=U.vID%>);</script> 
		</tr>
        
		<tr >
			<td class=css_col11_content><div align="right"> <font color=red>内容：<br/><br/><a href="javascript:multiup();" style="color:blue">批量上传</a></font><br />
<a href="http://www.5ucms.org/5ucms-jiaocheng/1147.html" title="不会使用批量上传？批量上传不能用？请点我学习" target="_blank">不会用？</a></div></td>
			<td colspan="6" class=css_col21>
<script charset="utf-8" src="../inc/editor/kindeditor/kindeditor.js"></script>
<script charset="utf-8" src="../inc/editor/kindeditor/lang/zh_CN.js"></script>
<script charset="utf-8" src="../inc/editor/kindeditor/plugins/code/prettify.js"></script>
	<script>
var KE;
KindEditor.ready(function(K) {
	K.basePath = '../inc/editor/kindeditor/';
	KE = K.create('#oContent',{
		allowImageUpload: false,
		filterMode : false,
		allowFileManager: false,
		allowFlashUpload: false,
		allowMediaUpload:false,
		allowFileUpload:false,
		afterBlur: function(){this.sync();},
		cssPath: ['../inc/editor/kindeditor/plugins/code/prettify.css'],
		items : ['source', '|', 'undo', 'redo', '|', 'preview', 'print', 'template', 'code', 'cut', 'copy', 'paste','plainpaste', 'wordpaste', '|', 'justifyleft', 'justifycenter', 'justifyright','justifyfull', 'insertorderedlist', 'insertunorderedlist', 'indent', 'outdent', 'subscript', 'superscript', 'clearhtml', 'quickformat', 'selectall', '|', 'fullscreen', '/','formatblock', 'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold','italic', 'underline', 'strikethrough', 'lineheight', 'removeformat', '|', 'image','flash', 'media', 'insertfile', 'table', 'hr', 'emoticons', 'baidumap', 'pagebreak','anchor', 'link', 'unlink', '|', 'about'],
		afterChange : function() {
						K('.word_count1').html(this.count());
						K('.word_count2').html(this.count('text'));
							}});
});



	</script>
<div  id="trcontent">
			<textarea id="oContent" name="oContent" style="width:99%;height:400px;visibility:hidden;display:block;"><%=server.htmlencode(U.vContent & "")%></textarea></div></td>
		</tr>
		<tr class=css_page_list>
			<td colspan=7><input type="submit" name="button" value="提交内容" />	</td>
		</tr>
	
</table>
</form>
<!--批量上传功能开始by Q3876307 14.12.12-->
<script>
function multiup(){
	document.getElementById('multiup').style.top='178px';
	document.getElementById('multiup').style.left='520px';
	document.getElementById('multiup').style.display='inline';
} 
</script>
  <script language="javascript">
function challs_flash_update(){ //Flash 初始化函数
	var a={};
	//定义变量为Object 类型

	a.title = "上传文件"; //设置组件头部名称
	
	a.FormName = "Filedata";
	//设置Form表单的文本域的Name属性
	
	a.url = "update.asp"; 
	//设置服务器接收代码文件
	
	a.parameter = ""; 
	//设置提交参数，以GET形式提交,例："key=value&key=value&..."
	
	a.typefile = ["Images (*.gif,*.png,*.jpg,*jpeg)","*.gif;*.png;*.jpg;*.jpeg;",
				"GIF (*.gif)","*.gif;",
				"PNG (*.png)","*.png;",
				"JPEG (*.jpg,*.jpeg)","*.jpg;*.jpeg;"];
	//设置可以上传文件 数组类型
	//"Images (*.gif,*.png,*.jpg)"为用户选择要上载的文件时可以看到的描述字符串,
	//"*.gif;*.png;*.jpg"为文件扩展名列表，其中列出用户选择要上载的文件时可以看到的 Windows 文件格式，以分号相隔
	//2个为一组，可以设置多组文件类型
	
	a.newTypeFile = ["Images (*.gif,*.png,*.jpg,*jpeg)","*.gif;*.png;*.jpg;*.jpeg;","JPE;JPEG;JPG;GIF;PNG",
				"GIF (*.gif)","*.gif;","GIF",
				"PNG (*.png)","*.png;","PNG",
				"JPEG (*.jpg,*.jpeg)","*.jpg;*.jpeg;","JPE;JPEG;JPG"];
	//设置可以上传文件，多了一个苹果电脑文件类型过滤 数组类型, 设置了此项，typefile将无效
	//"Images (*.gif,*.png,*.jpg)"为用户选择要上载的文件时可以看到的描述字符串,
	//"*.gif;*.png;*.jpg"为文件扩展名列表，其中列出用户选择要上载的文件时可以看到的 Windows 文件格式，以分号相隔
	//"JPE;JPEG;JPG;GIF;PNG" 分号分隔的 Macintosh 文件类型列表，如下面的字符串所示："JPEG;jp2_;GI
	
	a.UpSize = 0;
	//可限制传输文件总容量，0或负数为不限制，单位MB
	
	a.fileNum = 0;
	//可限制待传文件的数量，0或负数为不限制
	
	a.size = 1;
	//上传单个文件限制大小，单位MB，可以填写小数类型
	
	a.FormID = ['select','select2'];
	//设置每次上传时将注册了ID的表单数据以POST形式发送到服务器
	//需要设置的FORM表单中checkbox,text,textarea,radio,select项目的ID值,radio组只需要一个设置ID即可
	//参数为数组类型，注意使用此参数必须有 challs_flash_FormData() 函数支持
	
	a.autoClose = 1;
	//上传完成条目，将自动删除已完成的条目，值为延迟时间，以秒为单位，当值为 -1 时不会自动关闭，注意：当参数CompleteClose为false时无效
	
	a.CompleteClose = true;
	//设置为true时，上传完成的条目，将也可以取消删除条目，这样参数 UpSize 将失效, 默认为false
	
	a.repeatFile = true;
	//设置为true时，可以过滤用户已经选择的重复文件，否则可以让用户多次选择上传同一个文件，默认为false
	
	a.returnServer = true;
	//设置为true时，组件必须等到服务器有反馈值了才会进行下一个步骤，否则不会等待服务器返回值，直接进行下一步骤，默认为false
	
	a.MD5File = 1;
	//设置MD5文件签名模式，参数如下 ,注意：FLASH无法计算超过100M的文件,在无特殊需要时，请设置为0
	//0为关闭MD5计算签名
	//1为直接计算MD5签名后上传
	//2为计算签名，将签名提交服务器验证，在根据服务器反馈来执行上传或不上传
	//3为先提交文件基本信息，根据服务器反馈，执行MD5签名计算或直接上传，如果是要进行MD5计算，计算后，提交计算结果，在根据服务器反馈，来执行是否上传或不上传
	
	a.loadFileOrder=true;
	//选择的文件加载文件列表顺序，TRUE = 正序加载，FALSE = 倒序加载
	
	a.mixFileNum=0;
	//至少选择的文件数量，设置这个将限制文件列表最少正常数量（包括等待上传和已经上传）为设置的数量，才能点击上传，0为不限制
	
	a.ListShowType = 2;
	//文件列表显示类型：1 = 传统列表显示，2 = 缩略图列表显示（适用于图片专用上传）
	
	a.TitleSwitch = true;
	//是否显示组件头部
	
	a.ForceFileNum = 0;
	//强制条目数量，已上传和待上传条目相加等于为设置的值（不包括上传失败的条目），否则不让上传, 0为不限制，设置限制后mixFileNum,autoClose和fileNum属性将无效！
	
	a.autoUpload = false;
	//设置为true时，用户选择文件后，直接开始上传，无需点击上传，默认为false;
	
	a.adjustOrder = true;
	//设置为true时，用户可以拖动列表，重新排列位置
	
	a.deleteAllShow = true
	//设置是否显示，全部清除按钮
	 
	a.countData = true;
	//是否向服务器端提交组件文件列表统计信息，POST方式提交数据
	//access2008_box_info_max 列表总数量
	//access2008_box_info_upload 剩余数量 （包括当前上传条目）
	//access2008_box_info_over 已经上传完成数量 （不包括当前上传条目)
	
	a.isShowUploadButton = true;
	//是否显示上传按钮，默认为true
	
	a.isRotation = true;
	//是否可旋转图片
	//此项只有在缩略图模式下才有用
	//开启此项会POST一个图片角度到服务器端，由服务器端旋转图片
	//access2008_image_rotation 角度  0 到 -360 
	
	a.isUploadRate = true;
	//是否显示上传速率

	a.requireMpegBitrates = 0;
	//MP3播放模式下有效
	//MP3频率要求，0为不限制
	//频率与值,例子
	//128kbps = 128
	//320kbps = 320
	//低于要求将报错，不予上传

	a.mpegBitratesError = "mpegBitrates Error";
	//MP3播放模式下有效
	//a.requireMpegBitrates 不为0 时
	//报错显示文本设置

	a.isErrorStop = true;
	//遇见错误时，是否停止上传，如果为false时，忽略错误进入下一个上传

	a.isReadCookie = false;
	//是否读取cookie值，POST到服务器端
	//需要增加了challs_flash_cookies() JS函数支持
	//POST到服务器端，值名格式为 access2008_cookie_<cookie值名称>
	//比如 cookie名称为 aaa ,post 服务器 名称为 access2008_cookie_aaa

	return a ;
	//返回Object
}

function challs_flash_onComplete(a){ //每次上传完成调用的函数，并传入一个Object类型变量，包括刚上传文件的大小，名称，上传所用时间,文件类型
	var name=a.fileName; //获取上传文件名
	var size=a.fileSize; //获取上传文件大小，单位字节
	var time=a.updateTime; //获取上传所用时间 单位毫秒
	var type=a.fileType; //获取文件类型，在 Windows 上，此属性是文件扩展名。 在 Macintosh 上，此属性是由四个字符组成的文件类型
	var creationDate = a.fileCreationDate //获取文件创建时间
	var modificationDate = a.fileModificationDate //获取文件最后修改时间
	//document.getElementById('show').innerHTML+=name+' --- '+size+'字节 ----文件类型：'+type+'--- 用时 '+(time/1000)+'秒<br><br>'
}

function challs_flash_onCompleteData(a){ //获取服务器反馈信息事件
 
	
			var ext = a.split(".")[1];
			
			switch (ext) {
	case 'jpg':
	case 'gif':
	case 'bmp':
	case 'png':
	case 'JPG':
	case 'GIF':
	case 'BMP':
	case 'PNG':
		KE.insertHtml('<img src="' + a + '" width=650 />');
			document.frm.indexpiclist.options[document.frm.indexpiclist.length]=new Option(a,a);
			document.frm.indexpiclist.selectedIndex+=1;
			document.getElementById('indexpicimg').innerHTML='<img src=' + a + ' height=95 width=150 />';
			document.frm.oIndexpic.value=a;
		 break;
	case 'swf':
		KE.insertHtml('<embed src="' + t + '" type="application/x-shockwave-flash" width="650" height="500" quality="high" />')
		 break;
	case 'wmv':
	case 'avi':
	case 'mpeg':
	case 'mpg':
		KE.insertHtml('<embed src="' + t + '" type="video/x-ms-asf-plugin" width="650" height="500" autostart="false" loop="true" />')
		 break;
	case 'rmvb':
	case 'rm':
		KE.insertHtml('<embed src="' + t + '" type="audio/x-pn-realaudio-plugin" width="650" height="500" autostart="false" loop="true" />')
		 break;
	default:
		KE.insertHtml('<div class="accessory"><strong>内容附件: </strong><a href="' + t + '">' + t + '</a></div>')
		 break;
	}	
}
function challs_flash_onStart(a){ //开始一个新的文件上传时事件,并传入一个Object类型变量，包括刚上传文件的大小，名称，类型
	var name=a.fileName; //获取上传文件名
	var size=a.fileSize; //获取上传文件大小，单位字节
	var type=a.fileType; //获取文件类型，在 Windows 上，此属性是文件扩展名。 在 Macintosh 上，此属性是由四个字符组成的文件类型
	var creationDate = a.fileCreationDate //获取文件创建时间
	var modificationDate = a.fileModificationDate //获取文件最后修改时间
	//document.getElementById('show').innerHTML+=name+'开始上传！<br />';
	
	return true; //返回 false 时，组件将会停止上传
}

function challs_flash_onStatistics(a){ //当组件文件数量或状态改变时得到数量统计，参数 a 对象类型
	var uploadFile = a.uploadFile; //等待上传数量
	var overFile = a.overFile; //已经上传数量
	var errFile = a.errFile; //上传错误数量
}

function challs_flash_alert(a){ //当提示时，会将提示信息传入函数，参数 a 字符串类型
	//document.getElementById('show').innerHTML+='<font color="#ff0000">组件提示：</font>'+a+'<br />';
}

function challs_flash_onCompleteAll(a){ //上传文件列表全部上传完毕事件,参数 a 数值类型，返回上传失败的数量
	//document.getElementById('show').innerHTML+='<font color="#ff0000">所有文件上传完毕，</font>上传失败'+a+'个！<br />';
	//window.location.href='http://www.access2008.cn/update'; //传输完成后，跳转页面
}

function challs_flash_onSelectFile(a){ //用户选择文件完毕触发事件，参数 a 数值类型，返回等待上传文件数量
	//document.getElementById('show').innerHTML+='<font color="#ff0000">文件选择完成：</font>等待上传文件'+a+'个！<br />';
}

function challs_flash_deleteAllFiles(){ //清空按钮点击时，出发事件

	//返回 true 清空，false 不清空
	return confirm("你确定要清空列表吗?");
}

function challs_flash_onError(a){ //上传文件发生错误事件，并传入一个Object类型变量，包括错误文件的大小，名称，类型
	var err=a.textErr; //错误信息
	var name=a.fileName; //获取上传文件名
	var size=a.fileSize; //获取上传文件大小，单位字节
	var type=a.fileType; //获取文件类型，在 Windows 上，此属性是文件扩展名。 在 Macintosh 上，此属性是由四个字符组成的文件类型
	var creationDate = a.fileCreationDate //获取文件创建时间
	var modificationDate = a.fileModificationDate //获取文件最后修改时间
	//document.getElementById('show').innerHTML+='<font color="#ff0000">'+name+' - '+err+'</font><br />';
}

function challs_flash_FormData(a){ // 使用FormID参数时必要函数
	try{
		var value = '';
		var id=document.getElementById(a);
		if(id.type == 'radio'){
			var name = document.getElementsByName(id.name);
			for(var i = 0;i<name.length;i++){
				if(name[i].checked){
					value = name[i].value;
				}
			}
		}else if(id.type == 'checkbox'){
			var name = document.getElementsByName(id.name);
			for(var i = 0;i<name.length;i++){
				if(name[i].checked){
					if(i>0) value+=",";
					value += name[i].value;
				}
			}
		}else if(id.type == 'select-multiple'){
		    for(var i=0;i<id.length;i++){
		        if(id.options[i].selected){
					if(i>0) value+=",";
			         values += id.options[i].value; 
			    }
		    }
		}else{
			value = id.value;
		}
		return value;
	 }catch(e){
		return '';
	 }
}

function challs_flash_cookies(){ //获取cookie
	var postParams = {};
	var i, cookieArray = document.cookie.split(';'), caLength = cookieArray.length, c, eqIndex, name, value;
	for (i = 0; i < caLength; i++) {
		c = cookieArray[i];
		// Left Trim spaces
		while (c.charAt(0) === " ") {
			c = c.substring(1, c.length);
		}
		eqIndex = c.indexOf("=");
		if (eqIndex > 0) {
			name = c.substring(0, eqIndex);
			value = c.substring(eqIndex + 1);
			postParams[name] = value;
		}
	}
	return postParams;
}

function challs_flash_style(){ //组件颜色样式设置函数
	var a = {};
	
	/*  整体背景颜色样式 */
	a.backgroundColor=['#f6f6f6','#f3f8fd','#dbe5f1'];	//颜色设置，3个颜色之间过度
	a.backgroundLineColor='#5576b8';					//组件外边框线颜色
	a.backgroundFontColor='#066AD1';					//组件最下面的文字颜色
	a.backgroundInsideColor='#FFFFFF';					//组件内框背景颜色
	a.backgroundInsideLineColor=['#e5edf5','#34629e'];	//组件内框线颜色，2个颜色之间过度
	a.upBackgroundColor='#ffffff';						//上翻按钮背景颜色设置
	a.upOutColor='#000000';								//上翻按钮箭头鼠标离开时颜色设置
	a.upOverColor='#FF0000';							//上翻按钮箭头鼠标移动上去颜色设置
	a.downBackgroundColor='#ffffff';					//下翻按钮背景颜色设置
	a.downOutColor='#000000';							//下翻按钮箭头鼠标离开时颜色设置
	a.downOverColor='#FF0000';							//下翻按钮箭头鼠标移动上去时颜色设置
	
	/*  头部颜色样式 */
	a.Top_backgroundColor=['#e0eaf4','#bcd1ea']; 		//颜色设置，数组类型，2个颜色之间过度
	a.Top_fontColor='#245891';							//头部文字颜色
	
	
	/*  按钮颜色样式 */
	a.button_overColor=['#FBDAB5','#f3840d'];			//鼠标移上去时的背景颜色，2个颜色之间过度
	a.button_overLineColor='#e77702';					//鼠标移上去时的边框颜色
	a.button_overFontColor='#ffffff';					//鼠标移上去时的文字颜色
	a.button_outColor=['#ffffff','#dde8fe']; 			//鼠标离开时的背景颜色，2个颜色之间过度
	a.button_outLineColor='#91bdef';					//鼠标离开时的边框颜色
	a.button_outFontColor='#245891';					//鼠标离开时的文字颜色
	
	/* 文件列表样式 */
	a.List_scrollBarColor="#000000"						//列表滚动条颜色
	a.List_backgroundColor='#EAF0F8';					//列表背景色
	a.List_fontColor='#333333';							//列表文字颜色
	a.List_LineColor='#B3CDF1';							//列表分割线颜色
	a.List_cancelOverFontColor='#ff0000';				//列表取消文字移上去时颜色
	a.List_cancelOutFontColor='#D76500';				//列表取消文字离开时颜色
	a.List_progressBarLineColor='#B3CDF1';				//进度条边框线颜色
	a.List_progressBarBackgroundColor='#D8E6F7';		//进度条背景颜色	
	a.List_progressBarColor=['#FFCC00','#FFFF00'];		//进度条进度颜色，2个颜色之间过度
	
	/* 错误提示框样式 */
	a.Err_backgroundColor='#C0D3EB';					//提示框背景色
	a.Err_fontColor='#245891';							//提示框文字颜色
	a.Err_shadowColor='#000000';						//提示框阴影颜色
	
	
	return a;
}

function challs_flash_language(){ //组件文字设置函数
	var a = {
		// $[1]$ $[2]$ $[3]$是替换符号 
		// \n 是换行符号

		//按钮文字
		ButtonTxt_1:'停 止',
		ButtonTxt_2:'选择文件',
		ButtonTxt_3:'上 传',
		ButtonTxt_4:'清空',
		
		//全局文字设置
		Font:'宋体',
		FontSize:12,
		
		//提示文字
		Alert_1:'初始化错误：\n\n没有找到 JAVASCRITP 函数 \n函数名为 challs_flash_update()',
		Alert_2:'初始化错误：\n\n函数 challs_flash_update() 返回类型必须是 "Object" 类型',
		Alert_3:'初始化错误：\n\n没有设置上传路径地址',
		Alert_4:'添加上传文件失败，\n\n不可以在添加更多的上传文件!',
		Alert_5:'添加上传文件失败，\n\n等待上传文件列表只能有$[1]$个，\n请先上传部分文件!',
		Alert_6:'提示信息：\n\n请再选择$[1]$个上传文件！',
		Alert_7:'提示信息：\n\n请至少再选择$[1]$个上传文件！',
		Alert_8:'\n\n请选择上传文件！',
		Alert_9:'上传错误：\n\n$[1]$',

		//界面文字
		Txt_5:'等待上传',
		Txt_6:'等待上传：$[1]$个  已上传：$[2]$个',
		Txt_7:'字节',
		Txt_8:'总量限制（$[1]$MB）,上传失败',
		Txt_9:'文件超过$[1]$MB,上传失败',
		Txt_10:'秒',
		Txt_11:'保存数据中...',
		Txt_12:'上传完毕',
		Txt_13:'文件加载错误',
		Txt_14:'扫描文件...',
		Txt_15:'验证文件...',
		Txt_16:'取消',
		Txt_17:'无图',
		Txt_18:'加载中',

		Txt_20:'关闭',
		Txt_21:'确定',
		Txt_22:'上传文件',
		
		//错误提示
		Err_1:'上传地址URL无效',
		Err_2:'服务器报错：$[1]$',
		Err_3:'上传失败,$[1]$',
		Err_4:'服务器提交效验错误',
		Err_5:'效验数据无效错误'
	};
	return a;

}

var isMSIE = (navigator.appName == "Microsoft Internet Explorer");   
function thisMovie(movieName){   
  if(isMSIE){   
  	return window[movieName];   
  }else{
  	return document[movieName];   
  }   
}
</script>
<div id="multiup" style="display:none;position:absolute;z-index:2;"> 
<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=11,0,0,0" width="442" height="330" id="update" align="middle">
<param name="allowFullScreen" value="false" />
    <param name="allowScriptAccess" value="always" />
	<param name="movie" value="update.swf" />
    <param name="quality" value="high" />
    <param name="bgcolor" value="#ffffff" />
    <embed src="update.swf" quality="high" bgcolor="#ffffff" width="442" height="320" name="update" align="middle" allowScriptAccess="always" allowFullScreen="false" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" />
	</object>
<div style="margin: 10px -3px;"><a href="javascript:;" onclick="document.getElementById('multiup').style.display='none';"><img src="images/close.gif" border="0" /></a><br>(建议上传图片小于100K，过大将导致上传和网页打开都慢，压缩工具请联系客服索取)</div>
</div>
<!--批量上传功能结束 by Q3876307 14.12.12 -->
 
<script language="JavaScript">
<!--
function Insert(s){
	KE.insertHtml('<img src = "'+s+'"/>');
}
//-->
</script>
<%
End Sub
%>
</body>
</html>
