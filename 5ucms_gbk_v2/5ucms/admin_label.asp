<!--#Include File="../Inc/Const.Asp"-->
<!--#Include File="Inc/Class_Label.Asp"-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<title>自定义标签管理</title>
<link href="Images/Style.Css" rel="stylesheet" type="text/css" />
</head>
<body>
<%

Call ChkLogin("label")

Dim U
Set U = New Cls_Label

Dim Rs,Sql,Style
Dim ID
    ID = Request("ID")
Dim Key
    Key = Request("Key")
Dim Page
    Page = Request("Page")
Dim Orders
    Orders = Request("Orders")
Dim DFieldKey
    DFieldKey = Request("DFieldKey")
Dim DFieldOrders
    DFieldOrders = Request("DFieldOrders")
If len(ID) > 0 And Not IsNumeric(ID) Then : Response.Write "编号只能是数字!" : Conn.Close : Set Conn = Nothing : Set U = Nothing : Response.End '// 检测ID
If Len(Page) = 0 Or Not IsNumeric(Page) Then Page = 1
If Page < 1 Then Page = 1
If LCase(Orders) <> "asc" Then Orders = "Desc"
If Len(DFieldKey) = 0 Then DFieldKey = "Name"
If Len(DFieldOrders) = 0 Then DFieldOrders = "ID"
If Instr("[id],[name],[info]" , "[" & Lcase(DFieldKey) & "]") = 0 Then Key = ""
If Instr("[id],[name],[info]" , "[" & Lcase(DFieldOrders) & "]") = 0 Then DFieldOrders = "ID"
Page = Int(Page) : Key = Replace(Key,"'","")
Dim JumpUrl
    JumpUrl = "&Key=" & Server.UrlENCode(Key) & "&DFieldKey=" & DFieldKey & "&Orders=" & Orders & "&DFieldOrders=" & DFieldOrders

Select Case Request("Act")
Case "Guide"
	If Len(ID) = 0 Then
		Call U.Initialize()
		Call Main_Guide(0)
	Else
		U.vID = ID
		Call U.SetValue()
		Call Main_Guide(1)
	End If
Case "UpDate"
	If U.GetValue() Then
		If Len(ID) = 0 Then
			If Not U.Create() Then
				Alert U.LastError,"Admin_Label.Asp?Page=" & Page & JumpUrl
			Else
				Alert "","Admin_Label.Asp?Page=" & Page & JumpUrl
			End If
		Else
			U.vID = ID
			If Not U.Modify() Then
				Alert U.LastError,"Admin_Label.Asp?Page=" & Page & JumpUrl
			Else
				Alert "","Admin_Label.Asp?Page=" & Page & JumpUrl
			End If
		End If
	Else
		Alert U.LastError,"Admin_Label.Asp?Page=" & Page & JumpUrl
	End If
Case "Delete"
	If Len(ID) > 0 Then
		U.vID = ID
		If Not U.Delete() Then
			Alert U.LastError,"Admin_Label.Asp?Page=" & Page & JumpUrl
		Else
			Alert "","Admin_Label.Asp?Page=" & Page & JumpUrl
		End If
	Else
		Call Main()
	End If
Case "Change"
	If Len(ID) > 0 Then
		U.vID = ID
		If Not U.Change() Then
			Alert U.LastError,""
		End If
	End If
	Call Main()
Case "listlable" '导出标签
	call listlable()
Case "addlable" '导入标签
	call addlable()
Case "addlablesave"	'储存导入标签
		dim qsspost,qsskey,baidu,qss,zll,q,z,q1,q2,q3,gg
		qsspost=Request("qsspost")
		qsskey=Request("qsskey") 
		zll = split(qsskey,"$5ucms.com$") '区分组数据
		for z = 0 to ubound(zll) 
			qss = split(zll(z),"$qss$")
			q1 = qss(0)
				q1 = replace(q1,chr(10),"")
				q1 = replace(q1,chr(13),"")
				q1 = replace(q1," ","")
			q2 = qss(1)
			'if len(q2)<3 or isnull(q2) or q2="" or q2=" " then q2 = "预留标签"&q1
			q3 = qss(2)
			'if len(q3)<3 or isnull(q3) or q3="" or q3=" " then q3 = "预留标签"&q1
			baidu = baidu & "Name:"&q1&" Info:"&q2&" Code:"&q3&"<hr>"
			call addlabel(q1,q2,q3)
		next   
		call clscache
		call clscache   
		response.Write(gg&"<br><b>恭喜您！导入完成！ <a href=Admin_Label.Asp>点这里返回自定义标签管理</a></b>")  
Case Else
	Call Main()
End Select 

'导入标签过程
function addlabel(q1,q2,q3)
		Set Rs = DB("Select [ID] From [{pre}Label] Where [Name]='" & q1 & "'",1)
		If Not Rs.Eof Then 
			Set Rs = DB("Select * From [{pre}Label] Where [Name]='" & q1 & "'",3)		
				'Rs("Name") = q1
				Rs("Info") = q2
				Rs("Code") = q3
				Rs.Update
				gg = gg & "已更新"&q1&"成功<br>" 
		else	
			Set Rs = DB("Select * From [{pre}Label]",3)
				Rs.AddNew
					Rs("Name") = q1
					Rs("Info") = q2
					Rs("Code") = q3
				Rs.Update
				gg = gg & "已新增"&q1&"成功<br>" 
		end if
		Rs.Close
end function

Set U = Nothing
If IsObject(Conn) Then Conn.Close : Set Conn = Nothing

Sub Main()
	Sql = "Select [ID],[Name],[Info] From [{pre}Label]"
	If Len(Key) > 0 Then
		If Instr(",ID,","," & DFieldKey & ",") > 0 Then
			If Len(Key) > 0 And IsNumeric(Key) Then
				Sql = Sql & " Where [" & DFieldKey & "]=" & Key & ""
			End If
		Else
			Sql = Sql & " Where [" & DFieldKey & "] Like '%" & Key & "%'"
		End If
	End If
	Sql = Sql & " Order By [" & DFieldOrders & "] " & Orders & ""
	Set Rs = DB(Sql,2)
	%>
	<table width=100% border=0 cellpadding=3 cellspacing=1 class=css_table bgcolor='#E1E1E1' id="www_5ucms_org">
	  <tr class=css_menu>
	    <td colspan=4 class=css_main><table width=100% border=0 cellpadding=4 cellspacing=0 class=css_main_table>
	        <form name=frmSearch method=post action=Admin_Label.Asp>
	          <tr>
	            <td class=css_main><a href=Admin_Label.Asp?Page=<%=Page%><%=JumpUrl%>>自定义标签管理</a> <a href=Admin_Label.Asp?Act=Guide&Page=<%=Page%><%=JumpUrl%>>添加自定义标签</a> <a href=Admin_Label.Asp?Act=addlable>导入标签信息</a> <a href=Admin_Label.Asp?Act=listlable>导出标签信息</a> </td>
	            <td class=css_search><select name=DFieldKey id=DFieldKey>
	              <option value="ID" <%If Lcase(DFieldKey) = "id" Then Response.Write "selected='selecte'" %>>编号</option><option value="Name" <%If Lcase(DFieldKey) = "name" Then Response.Write "selected='selected'" %>>标签代码</option><option value="Info" <%If Lcase(DFieldKey) = "info" Then Response.Write "selected='selected'" %>>标签说明</option>
	              </select>
	              <input name=Key type=text id=Key size=10 value="<%=Key%>">
	              <input type=submit name=Submit value=搜>
	            </td>
	          </tr>
	        </form>
	      </table></td>
	  </tr>
	  <tr>
	    <td class='css_top'>编号</td>
	    <td class='css_top'>标签代码</td>
	    <td class='css_top'>标签说明</td>
	    <td class='css_top'>管理</td>
	  </tr>
            <tbody id="QQ3876307">
	  <%
	  If Rs.Eof Then
	  %>
	  <tr class=css_norecord>
	    <td colspan=4>没有任何记录！</td>
	  </tr>

	  <%
	  Else
	  Dim i
	  Rs.PageSize = 15
	  Rs.AbsolutePage = Page
	  For i = 1 To Rs.PageSize
	  If Rs.Eof Then Exit For
	  %>
	  <tr>
	    <td class=css_list><%=Rs("ID")%></td>
	    <td class=css_list>{My:<%=Rs("Name")%>}</td>
	    <td class=css_list><%=Rs("Info")%></td>
	    <td class=css_list>
	    <input name=modify type=button onclick="location.href='Admin_Label.Asp?Act=Guide&ID=<%=Rs("ID")%>&Page=<%=Page%><%=JumpUrl%>';" value=修改>
	    <input name=delete type=button onclick="if(confirm('您确定要删除这条记录吗?')){location.href='Admin_Label.Asp?Act=Delete&ID=<%=Rs("ID")%>&Page=<%=Page%><%=JumpUrl%>';}" value=删除>
        </td>
	  </tr>
	  <%
	  Rs.MoveNext
	  Next
	  End If
	  %>
      </tbody>
	  <tr class=css_page_list>
	    <td colspan=4><%=PageList(Rs.PageCount,Page,Rs.RecordCount,15,"Admin_Label.Asp?Page={p}&Key=" & Server.UrlENCode(Key) & "&DFieldKey=" & DFieldKey & "&Orders=" & Orders & "&DFieldOrders=" & DFieldOrders)%></td>
	  </tr>
</table> 
<script language="JavaScript" type="text/javascript" src="images/q3876307.js"></script>
<%
	Rs.Close
End Sub

'150710 导出标签
Sub listlable() 
dim ls,qq,q1,q2,q3,k,ll
k = 0
		Set ls = DB("Select [ID],[Name],[Info],[Code] From [{pre}Label]",1)
		do while Not ls.Eof 
			 k = k + 1
				 q1 = ls(1)
				 q2 = ls(2) 
				 q3 = ls(3)
			 if k > 1 then
				ll = "$5ucms.com$" & q1 & "$qss$" & q2 & "$qss$" & q3
			 else
				ll =  q1 & "$qss$" & q2 & "$qss$" & q3 	
			 end if 
'			 if q1 = "friendlink" or q1 = "copyright" or q1 = "indextitle" then
'			 	ll = ""
'		     end if	 
			 ll = replace(ll,"5u.hk","5ucms.org") 
			 qq = qq & ll 
		ls.moveNext
		loop
		ls.close
%> 
<b><a href=Admin_Label.Asp>返回自定义标签管理</a></b><br /> <br /> 
<TEXTAREA cols="100" rows="20"><%=qq%></TEXTAREA> <br />
<br />
说明：复制以上信息到其他5UCMS标签管理页，进入 <strong><a href=Admin_Label.Asp?Act=addlable>导入标签信息</a></strong> 即可。<br />
您也可以将此处信息存到一个txt文档里 如有问题请联系技术人员获取帮助。<br />
格式说明：$5ucms.com$ 用来分割不同的标签。$qss$ 用来分割标签的 名称 说明 HTML代码。因此您可以细化再修改，然后导入。

<%
End Sub

'150710 导入标签
Sub addlable() 
		  

%>
<style type="text/css">
.red {color: red}
.ql b {float:left}
.ql li { clear:both; line-height:30px; }
.black {color:black}
</style>

<b><a href=Admin_Label.Asp>返回自定义标签管理</a></b><br /> <br /> 
 
<form action="Admin_Label.Asp?Act=addlablesave" method="post" name="frmsearch" id="frmsearch">
    5UCMS LABEL标签导入 请输入指定格式内容：<br>
<TEXTAREA  name="qsskey"  cols="100" rows="10"></TEXTAREA><br>    <input name="button" type="submit" class="btn" id="button" value="开始导入咯！" /><br /><br />

说明：导入不可还原，建议先行 <a href=Admin_Label.Asp?Act=listlable><strong>导出标签信息</strong></a> 备份。<br />
导入规则：相同标签值会进行替换，不同则新增<br>格式说明：$5ucms.com$ 用来分割不同的标签。$qss$ 用来分割标签的 名称 说明 HTML代码。因此您可以细化再修改，然后导入。


</form>
<% 
End Sub

Sub Main_Guide(T)
	%>
    <form name='frm' method='post' action='Admin_Label.Asp?Act=UpDate&ID=<%=ID%>&Page=<%=Page%><%=JumpUrl%>'>
	<table width=100% border=0 cellpadding=3 cellspacing=1 class=css_table bgcolor='#E1E1E1'>
	  <tr class=css_menu>
	    <td colspan=2><table width=100% border=0 cellpadding=4 cellspacing=0 class=css_main_table>
	        <tr>
	          <td class=css_main><a href=Admin_Label.Asp?Page=<%=Page%><%=JumpUrl%>>自定义标签管理</a> <a href="http://b8bx.com/zishutongji.html" target="_blank">字数统计</a></td>
	        </tr>
	      </table></td>
	  </tr>
	  
	    <%If T = 0 Then Style="" Else Style=""%>
	    <tr<%=Style%>>
	      <td class=css_col11>标签代码：<div id='hName' style="color:#ccc;letter-spacing: 0px;font-size:13px;">只能是英文,不可重复&nbsp;</div></td>
	      <td class=css_col21>
	      <input class='css_input' name="oName" type="text" id="oName" onfocus="hName.style.color='red';" onblur="hName.style.color='#ccc';" value="<%=U.vName%>" size=40 >
	      若修改,请注意前台模板对应新名称 </td>
	    </tr>
	    <%If T = 0 Then Style="" Else Style=""%>
	    <tr<%=Style%>>
	      <td class=css_col12>标签说明：<div id='hInfo' style="color:#ccc;letter-spacing: 0px;font-size:13px;">简要说是该标签的使用和用途&nbsp;</div></td>
	      <td class=css_col22>
	      <input class='css_input' name="oInfo" type="text" id="oInfo" onfocus="hInfo.style.color='red';" onblur="hInfo.style.color='#ccc';" value="<%=U.vInfo%>" size=40 >
	      <br />        <style>
			form {
				margin: 0;
			}
			textarea {
				display: block;
			}
		</style>
            
        <script charset="utf-8" src="http://code.jquery.com/jquery-1.6.4.min.js"></script>
		<script>
			$(function() {
				var editor;
				$('input[name=load]').click(function() {
					$.getScript('../inc/editor/kindeditor/kindeditor-min.js', function() {
						KindEditor.basePath = '../inc/editor/kindeditor/';
						editor = KindEditor.create('textarea[name="oCode"]',{allowImageUpload: false,allowFileManager: false,allowFlashUpload: false,allowMediaUpload:false,allowFileUpload:false,items : ['source', '|', 'undo', 'redo', '|', 'preview', 'print', 'template', 'code', 'cut', 'copy', 'paste','plainpaste', 'wordpaste', '|', 'justifyleft', 'justifycenter', 'justifyright','justifyfull', 'insertorderedlist', 'insertunorderedlist', 'indent', 'outdent', 'subscript', 'superscript', 'clearhtml', 'quickformat', 'selectall', '|', 'fullscreen', '/','formatblock', 'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold','italic', 'underline', 'strikethrough', 'lineheight', 'removeformat', '|', 'image','flash', 'media', 'insertfile', 'table', 'hr', 'emoticons', 'baidumap', 'pagebreak','anchor', 'link', 'unlink', '|', 'about']});
					});
				});
				$('input[name=remove]').click(function() {
					if (editor) {
						editor.remove();
						editor = null;
					}
				});
			});
		</script>
		<input type="button" name="load" value="加载编辑器" />
		<input type="button" name="remove" value="删除编辑器" /></td>
	    </tr>
	    <%If T = 0 Then Style="" Else Style=""%>
	    <tr<%=Style%>>
	      <td class=css_col11>HTML代码：<div id='hCode' style="color:#ccc;letter-spacing: 0px;font-size:13px;">HTML值,自定义的标签优先权最高,所以你可能调用系统标签&nbsp;<br />你可以使用＄＄＄区分多个标签值&nbsp;</div></td>
	      <td class=css_col21>
	      <textarea name="oCode" id="oCode" style="width:98%;height:400px;" onfocus="hCode.style.color='red';" onblur="hCode.style.color='#ccc';"><%=Server.HtmlEncode(U.vCode)%></textarea>
	      </td>
	    </tr>
	    <tr class=css_page_list>
	      <td colspan=2><input type='submit' name='Submit' value='保存'></td>
	    </tr>
	  
	</table>
    </form>
	<%
End Sub
%>
</body>
</html>