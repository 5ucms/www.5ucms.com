<!--#include file="../../Inc/Const.asp"-->
<!--#include file="Inc/_Manage.Class.asp"-->
<%
Dim Plus
Set Plus = New Cls_Plus
Plus.Open("guestbook") ' 打开配置文件
Plus.CheckUser ' 权限检查
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GB2312" />
<title>管理</title>
<link href="../../inc/Images/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../../Images/ajax.js"></script>
<script type="text/javascript" src="Inc/_Manage.js"></script>
<script type="text/javascript" src="../../inc/Images/func.js"></script>
</head>
<body>

<%

Dim U
Set U = New Cls_guestbook

Dim Rs,Sql,Style
Dim ID
    ID = Request("ID")
Dim URL
	URL = "_Manage.Asp"
Dim Key
    Key = Request("Key")
Dim Page
    Page = Request("Page")
Dim Orders
    Orders = Request("Orders")
Dim DFieldKey
    DFieldKey = Request("DFieldKey")
Dim DFieldOrders
    DFieldOrders = Request("DFieldOrders")
If len(ID) > 0 And Not IsNumeric(ID) Then : Response.Write "编号只能是数字!" : Conn.Close : Set Conn = Nothing : Set U = Nothing : Response.End '// 检测ID
If Len(Page) = 0 Or Not IsNumeric(Page) Then Page = 1
If Page < 1 Then Page = 1
If LCase(Orders) <> "asc" Then Orders = "Desc"
If Len(DFieldKey) = 0 Then DFieldKey = "content"
If Len(DFieldOrders) = 0 Then DFieldOrders = "ID"
If Instr("[id],[user],[qss1],[qss2],[qss3],[qss4],[qss5],[qss6],[qss7],[qss8],[qss9],[qss10],[ip],[time],[state],[content]" , "[" & Lcase(DFieldKey) & "]") = 0 Then Key = ""
If Instr("[id],[user],[qss1],[qss2],[qss3],[qss4],[qss5],[qss6],[qss7],[qss8],[qss9],[qss10],[ip],[time],[state]" , "[" & Lcase(DFieldOrders) & "]") = 0 Then DFieldOrders = "ID"
Dim DFieldKey1 :  DFieldKey1 = "{pre}guestbook].[" & DFieldKey
Page = Int(Page) : Key = Replace(Key,"'","")
Dim JumpUrl
    JumpUrl = "&Key=" & Server.UrlENCode(Key) & "&DFieldKey=" & DFieldKey & "&Orders=" & Orders & "&DFieldOrders=" & DFieldOrders
	
Select Case Request("Act")
Case "Guide"
	If Len(ID) = 0 Then
		Call U.Initialize()
		Call Main_Guide(0)
	Else
		U.vID = ID
		Call U.SetValue()
		Call Main_Guide(1)
	End If
Case "UpDate"
	If U.GetValue() Then
		If Len(ID) = 0 Then
			Alert "参数出错！",URL & "?Page=" & Page & JumpUrl
		Else
			U.vID = ID
			If Not U.Modify() Then
				Alert U.LastError,URL & "?Page=" & Page & JumpUrl
			Else
				Alert "",URL & "?Page=" & Page & JumpUrl
			End If
		End If
	Else
		Alert U.LastError,URL & "?Page=" & Page & JumpUrl
	End If
Case "Delete"
	If Len(ID) > 0 Then
		U.vID = ID
		If Not U.Delete() Then
			Alert U.LastError,URL & "?Page=" & Page & JumpUrl
		Else
			Alert "",URL & "?Page=" & Page & JumpUrl
		End If
	Else
		Call Main()
	End If
	
	'新增批量删除按钮 by cssHhaier
Case "DODelete"
	ID = Replace(Replace(Request("ids"),"'","")," ","")
	Call U.DeleteAll(ID)
	Call Main()
	
	
Case "Change"
	If Len(ID) > 0 Then
		U.vID = ID
		If Not U.Change() Then
			Alert U.LastError,""
		End If
	End If
	Call Main()
Case Else
	Call Main()
End Select
Set U = Nothing
If IsObject(Conn) Then Conn.Close : Set Conn = Nothing

Sub Main()
	Sql = "Select * From [{pre}guestbook]"
	If Len(Key) > 0 Then
		If Instr(",ID,State,","," & DFieldKey & ",") > 0 Then
			If Len(Key) > 0 And IsNumeric(Key) Then
				Sql = Sql & " Where [" & DFieldKey1 & "]=" & Key & ""
			End If
		Else
			Sql = Sql & " Where [" & DFieldKey1 & "] Like '%" & Key & "%'"
		End If
	End If
	If Instr(LCase(Sql)," where [") > 0 Then
		Sql = Sql & "  Order By [{pre}guestbook].[" & DFieldOrders & "] " & Orders & ""
	Else
		Sql = Sql & "  Order By [{pre}guestbook].[" & DFieldOrders & "] " & Orders & ""
	End If
	Set Rs = DB(Sql,2)
	%>
<table width=100% border=0 cellpadding=3 cellspacing=1 class=css_table bgcolor='#E1E1E1'>
	<tr class=css_menu>
		<td colspan=7><table width=100% border=0 cellpadding=4 cellspacing=0 class=css_main_table>
				<form name=frmSearch method=post action=<%=Url%>>
					<tr>
						<td class=css_main><a href=<%=Url%>>留言管理</a></td>
						<td class=css_search><select name=DFieldKey id=DFieldKey>
								<option value="ID" <%If Lcase(DFieldKey) = "id" Then Response.Write "selected='selecte'" %>>编号</option>								
								<option value="User" <%If Lcase(DFieldKey) = "user" Then Response.Write "selected='selected'" %>>留言者</option>
								<option value="Content" <%If Lcase(DFieldKey) = "content" Then Response.Write "selected='selected'" %>>留言内容</option>
								<option value="Ip" <%If Lcase(DFieldKey) = "ip" Then Response.Write "selected='selected'" %>>留言IP</option>
								<option value="Time" <%If Lcase(DFieldKey) = "time" Then Response.Write "selected='selected'" %>>留言时间</option>
								<option value="State" <%If Lcase(DFieldKey) = "state" Then Response.Write "selected='selected'" %>>审核状态</option>
							</select>
							<input name=Key type=text id=Key size=50 value="<%=Key%>"> <input type=submit name=Submit value=搜> </td>
					</tr>
				</form>
			</table></td>
	</tr>
	<form name=frm method=post action=_manage.asp>
	<tr>
	<!--新增批量删除-->
		<td width="30" class='css_top'><input type="checkbox" name="chkall" id="chkall" onclick="CheckAll()" class="checkbox"></td>
  <!--新增批量删除-->
		<td class='css_top'>编号</td>		
		<td class='css_top'>留言者</td>
		<td class='css_top'>留言IP</td>
		<td class='css_top'>时间</td>
		<td class='css_top'><a href='<%=Url%>?Page=<%=Page%>&DFieldOrders=State&Orders=<%If Lcase(DFieldOrders) = Lcase("State") And Orders = "Desc" Then Response.Write "Asc" Else Response.Write "Desc" %>'>状态</a></td>
		<td class='css_top'>管理</td>
	</tr>
	<%
	  If Rs.Eof Then
	  %>
	<tr class=css_norecord>
		<td colspan=30>没有任何记录！</td>
	</tr>
	<%
	  Else
	  Dim i
	  Rs.PageSize = 9
	  Rs.AbsolutePage = Page
	  For i = 1 To Rs.PageSize
	  If Rs.Eof Then Exit For
	  %>
		<tr>
	  <!--新增批量删除--><td width="30" class='css_list'><input type="checkbox" name="ids" value="<%=Rs("ID")%>" class="checkbox"></td><!--新增批量删除-->
      		<td class='css_list'><%=Rs("ID")%></td>		
		<td class='css_list'><%=Rs("User")%></td>
		<td class='css_list'><%=Rs("Ip")%></td>
		<td class='css_list'><%=Rs("Time")%></td>
		<td class='css_list'><span id="CommentState<%=Rs("ID")%>" style="cursor:hand;" onclick="CommentState(<%=Rs("ID")%>);"> <%
	    Select Case Cstr(LCase(Rs("State")))
	    Case "0" Response.Write "<font color=blue>待审</font>"
	    Case "1" Response.Write "已审"
	    Case Else Response.Write Rs("State")
	    End Select
	    %></span></td>
		<td class='css_list'><input name=modify type=button onclick="location.href='<%=Url%>?Act=Guide&ID=<%=Rs("ID")%>&Page=<%=Page%><%=JumpUrl%>';" value=修改> <input name=delete type=button onclick="if(confirm('您确定要删除这条记录吗?')){location.href='<%=Url%>?Act=Delete&ID=<%=Rs("ID")%>&Page=<%=Page%><%=JumpUrl%>';}" value=删除></td>
	</tr>
	<tr>
		<td colspan="7" class='css_comment_list'><span title="回复留言" style="cursor:hand;" onclick="ReComment(<%=Rs("ID")%>);"  id="ReComment(<%=Rs("ID")%>)" name="ReComment(<%=Rs("ID")%>)"><%=Rs("Content")%></span><span id="ReComment<%=Rs("ID")%>"><%If Len(Rs("Recomment"))>0 Then Response.Write "<font color=red>回复: " & Rs("Recomment") & "</font>"%></span><span id="ReCommentState<%=Rs("ID")%>" style='display:none;'></span></td>
	</tr>

	<%
	  Rs.MoveNext
	  Next
	  End If
	  %>
	<tr class=css_page_list>
		<td colspan=8><input name="" type="button" class="inputs" onclick="GuestbookDo('DODelete');" value="批量删除" />
	  <%=PageList(Rs.PageCount,Page,Rs.RecordCount,15,URL & "?Page={p}&Key=" & Server.UrlENCode(Key) & "&DFieldKey=" & DFieldKey & "&Orders=" & Orders & "&DFieldOrders=" & DFieldOrders)%></td>
	</tr>
  </form>
</table>
<%
	Rs.Close
End Sub

Sub Main_Guide(T)
	%>
<table width=100% border=0 cellpadding=3 cellspacing=1 class=css_table bgcolor='#E1E1E1'>
	<tr class=css_menu>
		<td colspan=2><table width=100% border=0 cellpadding=4 cellspacing=0 class=css_main_table>
				<tr>
					<td class=css_main><FORM METHOD=POST ACTION="http://www.ip138.com/ips8.asp" name="ipform" target="_blank"><a href=<%=Url%>>留言管理</a>  
留言者IP地址：<input type="text" name="ip" size="16" class='css_input' value="<%=U.vIP%>"> 
<input type="submit" value="查询"><INPUT TYPE="hidden" name="action" value="2"> 可查询留言者所在地</FORM></td>
				</tr>
			</table></td>	</tr>

	<form name='frm' method='post' action='<%=Url%>?Act=UpDate&ID=<%=ID%>&Page=<%=Page%><%=JumpUrl%>'>
		<%If T = 0 Then Style=" style='display:none;'" Else Style=""%>
		<tr<%=Style%>>
			<td class=css_col11>姓名：<span id='hUser' style="color:#ccc;letter-spacing: 0px;font-size:13px;"></span></td>			<td class=css_col21><input class='css_input' name="oUser" type="text" id="oUser" onfocus="hUser.style.color='red';" onblur="hUser.style.color='#ccc';" value="<%=U.vUser%>" size=50 > </td>

		</tr>
        
        

       <%If T = 0 Then Style=" style='display:none;'" Else Style=""%>
		<tr<%=Style%>>
			<td class=css_col11>公司名称：<span id='hUser' style="color:#ccc;letter-spacing: 0px;font-size:13px;"></span></td>
			<td class=css_col21><input class='css_input' name="oqss1" type="text" id="oUser" onfocus="hUser.style.color='red';" onblur="hUser.style.color='#ccc';" value="<%=U.vqss1%>" size=50 ></td>
		</tr>
       <%If T = 0 Then Style=" style='display:none;'" Else Style=""%>
		<tr<%=Style%>>
			<td class=css_col11>联系电话：<span id='hUser' style="color:#ccc;letter-spacing: 0px;font-size:13px;"></span></td>
			<td class=css_col21><input class='css_input' name="oqss2" type="text" id="oUser" onfocus="hUser.style.color='red';" onblur="hUser.style.color='#ccc';" value="<%=U.vqss2%>" size=50 > </td>
		</tr>
       <%If T = 0 Then Style=" style='display:none;'" Else Style=""%>
		<tr<%=Style%>>
			<td class=css_col11>联系传真：<span id='hUser' style="color:#ccc;letter-spacing: 0px;font-size:13px;"></span></td>
			<td class=css_col21><input class='css_input' name="oqss3" type="text" id="oUser" onfocus="hUser.style.color='red';" onblur="hUser.style.color='#ccc';" value="<%=U.vqss3%>" size=50 > </td>
		</tr>
       <%If T = 0 Then Style=" style='display:none;'" Else Style=""%>
		<tr<%=Style%>>
			<td class=css_col11>电子邮件：<span id='hUser' style="color:#ccc;letter-spacing: 0px;font-size:13px;"></span></td>
			<td class=css_col21><input class='css_input' name="oqss4" type="text" id="oUser" onfocus="hUser.style.color='red';" onblur="hUser.style.color='#ccc';" value="<%=U.vqss4%>" size=50 > </td>
		</tr>
       <%If T = 0 Then Style=" style='display:none;'" Else Style=""%>
		<tr<%=Style%>>
			<td class=css_col11>留言标题：<span id='hUser' style="color:#ccc;letter-spacing: 0px;font-size:13px;"></span></td>
			<td class=css_col21><input class='css_input' name="oqss5" type="text" id="oUser" onfocus="hUser.style.color='red';" onblur="hUser.style.color='#ccc';" value="<%=U.vqss5%>" size=50 > </td>
		</tr>                                                        		<tr>        
 
			<td class=css_col12>留言内容：<span id='hContent' style="color:#ccc;letter-spacing: 0px;font-size:13px;"></span></td>
			<td class=css_col22><textarea class='css_textarea' name="oContent" type="text" id="oContent" onfocus="hContent.style.color='red';" onblur="hContent.style.color='#ccc';" cols="60" rows="6" ><%=Server.HtmlEnCode(U.vContent&"")%></textarea>
			</td>
		</tr>
 
		<tr<%=Style%>>
			<td class=css_col11>管理回复：<span id='hRecomment' style="color:#ccc;letter-spacing: 0px;font-size:13px;"></span></td>
			<td class=css_col21><textarea class='css_textarea' name="oRecomment" type="text" id="oRecomment" onfocus="hRecomment.style.color='red';" onblur="hRecomment.style.color='#ccc';" cols="60" rows="6" ><%=Server.HtmlEnCode(U.vRecomment&"")%></textarea>
			不填则前台不显示回复内容及回复人
			</td>
		</tr>
        <%If T = 0 Then Style=" style='display:none;'" Else Style=""%>
		<tr<%=Style%>>
			<td class=css_col12>回复人：<span id='hIp' style="color:#ccc;letter-spacing: 0px;font-size:13px;"></span></td>
			<td class=css_col22><input class='css_input' name="oqss10" type="text" id="oUser" onfocus="hUser.style.color='red';" onblur="hUser.style.color='#ccc';" value="<%=U.vqss10%>" size=50 > 不填则默认为后台管理员账号名</td>
		</tr>
		<%If T = 0 Then Style=" style='display:none;'" Else Style=""%>
		<tr<%=Style%>>
			<td class=css_col12>留言IP：<span id='hIp' style="color:#ccc;letter-spacing: 0px;font-size:13px;"></span></td>
			<td class=css_col22><input class='css_input' name="oIp" type="text" id="oIp" onfocus="hIp.style.color='red';" onblur="hIp.style.color='#ccc';" value="<%=U.vIp%>" size=40 > </td>
		</tr>
		<%If T = 0 Then Style=" style='display:none;'" Else Style=""%>
		<tr<%=Style%>>
			<td class=css_col11>留言时间：<span id='hTime' style="color:#ccc;letter-spacing: 0px;font-size:13px;"></span></td>
			<td class=css_col21><input class='css_input' name="oTime" type="text" id="oTime" onfocus="hTime.style.color='red';" onblur="hTime.style.color='#ccc';" value="<%=U.vTime%>" size=40 > </td>
		</tr>
		<%If T = 0 Then Style=" style='display:none;'" Else Style=""%>
		<tr<%=Style%>>
			<td class=css_col12>审核状态：<span id='hState' style="color:#ccc;letter-spacing: 0px;font-size:13px;"></span></td>
			<td class=css_col22><%If IsNull(U.vState) Then U.vState=""%> <span style="width: 100px;"><input <%If  U.vState = 0 Then Response.Write "checked='checked'"%> class='css_input' name="oState" type="radio" id="oState" onfocus="hState.style.color='red';" onblur="hState.style.color='#ccc';" value="0"  >待审</span> <span style="width: 100px;"><input <%If  U.vState = 1 Then Response.Write "checked='checked'"%> class='css_input' name="oState" type="radio" id="oState" onfocus="hState.style.color='red';" onblur="hState.style.color='#ccc';" value="1"  >已审</span> </td>
		</tr>
		<tr class=css_page_list>
			<td colspan=2><input type='submit' name='Submit' value='保存'></td>
		</tr>
	</form>
</table>
<%
End Sub
%>
</body>
</html>
<%
Set Plus = Nothing
%>