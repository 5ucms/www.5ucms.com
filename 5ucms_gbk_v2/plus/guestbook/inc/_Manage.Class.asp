<%

Class Cls_guestbook

	Dim vID	
	Dim vUser
	Dim vqss1
	Dim vqss2
	Dim vqss3
	Dim vqss4
	Dim vqss5
	Dim vqss6
	Dim vqss7
	Dim vqss8
	Dim vqss9
	Dim vqss10	
	Dim vqss11
	Dim vqss12
	Dim vqss13
	Dim vqss14
	Dim vqss15
	Dim vqss16
	Dim vqss17
	Dim vqss18
	Dim vqss19
	Dim vqss20		
	Dim vContent
	Dim vRecomment
	Dim vIp
	Dim vTime
	Dim vState
	Dim LastError

	Private Sub Class_Initialize()
		Call Initialize()
	End Sub

	Private Sub Class_Terminate()
		Call Initialize()
	End Sub

	Public Function Initialize()
		vID = 0		
		vUser = "Guest"
		vContent = ""
		vRecomment = ""
		vIp = ""
		vTime = Now()
		vState = 0
	End Function

	Public Function GetValue()		
		vUser = Request.Form("oUser")
		vqss1 = Request.Form("oqss1")
		vqss2 = Request.Form("oqss2")
		vqss3 = Request.Form("oqss3")
		vqss4 = Request.Form("oqss4")
		vqss5 = Request.Form("oqss5")
		vqss6 = Request.Form("oqss6")
		vqss7 = Request.Form("oqss7")
		vqss8 = Request.Form("oqss8")
		vqss9 = Request.Form("oqss9")
		vqss10 = Request.Form("oqss10")
		vqss11 = Request.Form("oqss11")
		vqss12 = Request.Form("oqss12")
		vqss13 = Request.Form("oqss13")
		vqss14 = Request.Form("oqss14")
		vqss15 = Request.Form("oqss15")
		vqss16 = Request.Form("oqss16")
		vqss17 = Request.Form("oqss17")
		vqss18 = Request.Form("oqss18")
		vqss19 = Request.Form("oqss19")
		vqss20 = Request.Form("oqss20")		
		If Len(vqss10) = 0 Then
			vqss10 = getLogin("admin","username")
		End If
		vContent = Request.Form("oContent")
		vRecomment = Request.Form("oRecomment")
		vIp = Request.Form("oIp")
		vTime = Request.Form("oTime")
		vState = Request.Form("oState")		
		If Len(vUser) < 1 Or Len(vUser) > 10 Then LastError = "留言者的长度请控制在 1 至 10 位" : GetValue = False : Exit Function
		If Len(vContent) < 1 Or Len(vContent) > 250 Then LastError = "留言内容的长度请控制在 1 至 250 位" : GetValue = False : Exit Function
		If Len(vRecomment) > 250 Then LastError = "管理回复的长度请控制在 250 位以内" : GetValue = False : Exit Function
		If Len(vIp) < 7 Or Len(vIp) > 20 Then LastError = "留言IP的长度请控制在 7 至 20 位" : GetValue = False : Exit Function
		If Len(vTime) = 0 Or Not IsDate(vTime) Then LastError = "留言时间只能是时间格式" : GetValue = False : Exit Function
		If Len(vState) = 0 Or Not IsNumeric(vState) Then LastError = "审核状态只能是数字" : GetValue = False : Exit Function
		GetValue = True
	End Function

	Public Function SetValue()
		Dim Rs
		Set Rs = DB("Select * From [{pre}guestbook] Where [ID]=" & vID,1)
		If Rs.Eof Then Rs.Close : Set Rs = Nothing : LastError = "你所需要查询的记录 " & vID & " 不存在!" : SetValue = False : Exit Function
		vUser = Rs("User")
		vqss1 = Rs("qss1")
		vqss2 = Rs("qss2")
		vqss3 = Rs("qss3")
		vqss4 = Rs("qss4")
		vqss5 = Rs("qss5")
		vqss6 = Rs("qss6")
		vqss7 = Rs("qss7")
		vqss8 = Rs("qss8")
		vqss9 = Rs("qss9")
		vqss10 = Rs("qss10")
		vqss11 = Rs("qss11")
		vqss12 = Rs("qss12")
		vqss13 = Rs("qss13")
		vqss14 = Rs("qss14")
		vqss15 = Rs("qss15")
		vqss16 = Rs("qss16")
		vqss17 = Rs("qss17")
		vqss18 = Rs("qss18")
		vqss19 = Rs("qss19")
		vqss20 = Rs("qss20")		
		vContent = Rs("Content")
		vRecomment = Rs("Recomment")
		vIp = Rs("Ip")
		vTime = Rs("Time")
		vState = Rs("State")
		Rs.Close
		Set Rs = Nothing
		SetValue = True
	End Function

	Public Function Modify()
		Dim Rs
		Set Rs = DB("Select * From [{pre}guestbook] Where [ID]=" & vID,3)
		If Rs.Eof Then Rs.Close : Set Rs = Nothing : LastError = "你所需要更新的记录 " & vID & " 不存在!" : Modify = False : Exit Function
		Rs("User") = vUser
		Rs("qss1") = vqss1
		Rs("qss2") = vqss2
		Rs("qss3") = vqss3
		Rs("qss4") = vqss4
		Rs("qss5") = vqss5
		Rs("qss6") = vqss6
		Rs("qss7") = vqss7
		Rs("qss8") = vqss8
		Rs("qss9") = vqss9
		Rs("qss10") = vqss10
		Rs("qss11") = vqss11
		Rs("qss12") = vqss12
		Rs("qss13") = vqss13
		Rs("qss14") = vqss14
		Rs("qss15") = vqss15
		Rs("qss16") = vqss16
		Rs("qss17") = vqss17
		Rs("qss18") = vqss18
		Rs("qss19") = vqss19
		Rs("qss20") = vqss20		
		Rs("Content") = vContent
		Rs("Recomment") = vRecomment
		Rs("Ip") = vIp
		Rs("Time") = vTime
		Rs("State") = vState
		Rs.Update
		Rs.Close
		Set Rs = Nothing
		Modify = True
	End Function

	Public Function Delete()		
		Call DB("Delete From [{pre}guestbook] Where [ID]=" & vID ,0) '# 删除留言
		Delete = True
	End Function
	
'新增批量删除按钮 by cssHhaier
	Public Function DeleteAll(Byval GuestbookIDS)
		Dim Rs
		Set Rs = DB("Select [ID] From [{pre}guestbook] Where ID In (" & GuestbookIDS & ")",1)
		Do While Not Rs.Eof
			Call DB("Delete From [{pre}guestbook] Where [ID]=" & Rs(0) ,0) '# 删除信息
			Rs.MoveNext
		Loop
		Rs.Close
	End Function

	Public Function Change()
		Dim GetField
		GetField = Request("Field")
		If Instr(LCase("[State]"), LCase("[" & GetField & "]")) = 0 Then
			LastError = "参数出错,无法修改记录!" : Change = False : Exit Function
		End If
		Dim Rs
		Set Rs = DB("Select [" & GetField & "] From [{pre}guestbook] Where [ID]=" & vID,3)
		If Rs.Eof Then
			Rs.Close : Set Rs = Nothing : LastError ="所需要更新的记录不存在!" : Change = False : Exit Function
		Else
			If Rs(GetField) = 0 Then Rs(GetField) = 1 Else Rs(GetField) = 0
			Rs.Update : Rs.Close : Set Rs = Nothing
			Change = True
		End If
	End Function
End Class
%>