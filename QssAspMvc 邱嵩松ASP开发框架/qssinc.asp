<%
option explicit  
response.charset = "GB2312"
session.codepage = 936
session.timeout = 1440 
Server.ScriptTimeOut=9999999  
response.CacheControl="Public" 
'缓存的 control header ，可被设置为 "Public" 或 "Private"。
'Private 是默认的，仅有私人缓存可以缓存此页。如果为此设置，代理服务器就不会缓存页面。
'Public 指示公共缓存。如果为此设置，代理服务器会缓存页面。
'********************************************** 
'一些公用定义
dim strobjectfso
strobjectfso = "scripting.filesystemobject"
'********************************************** 
'邱嵩松 ASP开发函数库 v0.1 
'QQ3876307 www.5ucms.com
'LASTUPDATE:150128
'以下信息可以根据实际需要调用或删除
'********************************************** 
%>
<%'格式化函数%>
<!--#include file="qssasp/format.asp"-->
<%'远程获取网页信息%>
<!--#include file="qssasp/gethttp.asp"-->
<%'文件操作%>
<!--#include file="qssasp/fso.asp"-->
<%'验证%>
<!--#include file="qssasp/verify.asp"-->
<%'内存缓存数据%>
<!--#include file="qssasp/cache.asp"-->
<%'ASP解析json数据%>
<!--#include file="qssasp/json.asp"-->
<%'统计计算类%>
<!--#include file="qssasp/count.asp"-->
<%'txt验证时间类%>
<!--#include file="qssasp/timetxt.asp"-->
<%'替换类%>
<!--#include file="qssasp/replace.asp"-->
<%'IP处理类%>
<!--#include file="qssasp/getip.asp"-->
<%'文件编码判断转换类%>
<!--#include file="qssasp/code.asp"-->
<%'正则表达式%>
<!--#include file="qssasp/regex.asp"-->
<%'随机%>
<!--#include file="qssasp/random.asp"-->
<%'提交验证%>
<!--#include file="qssasp/postget.asp"-->
<%'验证上传的文件是否安全%>
<!--#include file="qssasp/checkfilesafe.asp"-->
<%'邮件发送相关%>
<!--#include file="qssasp/email.asp"-->
<%'进制转换类%>
<!--#include file="qssasp/hex.asp"-->