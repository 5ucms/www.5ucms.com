<%
'********************************************** 
'获取远程页面全部信息 无视编码
'150206 QQ3876307 邱嵩松 www.5ucms.com
'********************************************** 
Function GetNewVersion(qurl2)
	if checkisUrl(qurl2) = false then response.Write("网址格式不正确！") : response.End()
	GetNewVersion="获取失败" 
	Dim objPing
	Set objPing = Server.CreateObject("MSXML2.ServerXMLHTTP") 
	objPing.open "GET",qurl2,False 
	objPing.send 
	If objPing.ReadyState=4 Then
		If objPing.Status=200 Then '必须反馈200正常代码才获取
			GetNewVersion=  objPing.responseText
		End If
	End If 
End Function

'探测网址是否可以打开
Function CheckHTTP(ByVal URL)
	Dim Retrieval  
	On Error Resume Next
	CheckHTTP = False
	Set Retrieval = CreateObject("MSXML2.XMLHTTP")
	With Retrieval
		.Open "HEAD", URL, False
		.send
		If .readyState <> 4 Then
			CheckHTTP = False
			Set Retrieval = Nothing
			Exit Function
		End If
		If .Status < 300 Then
			CheckHTTP = True
			Set Retrieval = Nothing
			Exit Function
		Else
			CheckHTTP = False
			Set Retrieval = Nothing
			Exit Function
		End If
	End With
	If Err.Number <> 0 Then
		CheckHTTP = False
		Err.Clear
		Set Retrieval = Nothing
		Exit Function
	End If
	Set Retrieval = Nothing
	Exit Function
End Function

'获取数据块wstr中间a~b之间的数据 必须保证 a 或 b 在 wstr中唯一 否则会出错哟 150130
Function GetHttpPart(wstr,a,b)
  dim qstart,qover
  if len(wstr&"")=0 or len(a&"")=0 or len(b&"")=0  then response.Write("wstr,a,b均不能为空！") : exit function
  if instr(wstr,a)=0 or instr(wstr,b)=0 then response.Write("区间标识A或B不存在！") : exit function
  qstart=Newstring(wstr,a)
  qover=Newstring(wstr,b)
  GetHttpPart = mid(wstr,qstart,qover-qstart)
  GetHttpPart = replace(GetHttpPart,a,"") 
End Function

'获取GB2312版本远程网页信息 不管什么网页状态情况都获取
Function getHTTPPage(Path)
	dim t
	if checkisUrl(Path) = false then response.Write("网址格式不正确！") : response.End()
	t = GetBody(Path)
	getHTTPPage=BytesToBstr(t,"GB2312")
End function

'获取UTF-8版本远程网页信息
Function getHTTPPage2(Path)
	dim t2
	if checkisUrl(Path) = false then response.Write("网址格式不正确！") : response.End()
	t2 = GetBody(Path)
	getHTTPPage2=BytesToBstr(t2,"utf-8")
End function

Function Newstring(wstr,strng)
	Newstring=Instr(lcase(wstr),lcase(strng))
	if Newstring<=0 then Newstring=Len(wstr)
End Function

Function BytesToBstr(body,Cset)
	'On Error Resume Next
	dim objstream
	set objstream = Server.CreateObject("adodb.stream")
	objstream.Type = 1
	objstream.Mode =3
	objstream.Open
	objstream.Write body
	objstream.Position = 0
	objstream.Type = 2
	objstream.Charset = Cset
	BytesToBstr = objstream.ReadText
	objstream.Close
	set objstream = nothing
End Function

Function GetBody(url)
	dim Retrieval
	Set Retrieval = CreateObject("Microsoft.XMLHTTP")
	With Retrieval
	.Open "Get", url, False, "", ""
	.Send
	GetBody = .ResponseBody
	End With
	Set Retrieval = Nothing
End Function 
%>