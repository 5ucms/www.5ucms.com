<%   
'********************************************** 
'判断文件是否安全类
'创150210 改150210
'QQ3876307 邱嵩松 www.5ucms.com
'**********************************************   
'判断上传的文件是否安全
Function checkfilesafe(FilePath)
	checkfilesafe = true '默认正常
	if instr(FilePath,":")=0 then FilePath = server.mappath(FilePath) '自动判断是否为绝对路径 不是则转换之  
	dim sTextAll,MyFile,MyText,sStr,sNoString,i
	set MyFile = server.createobject(strobjectfso)
	if not MyFile.fileexists(FilePath) then response.Write("文件不存在！") : Exit Function '检查文件是否存在 再继续执行 默认结果安全
	set MyText = MyFile.OpenTextFile(FilePath,1) '读取文本文件
	sTextAll = lcase(MyText.ReadAll)
	'开始去掉一些干扰字符 使结果更为准确
	sTextAll = replace(sTextAll,"&","")
	sTextAll = replace(sTextAll,"""","")
	sTextAll = replace(sTextAll,"'","") 
	MyText.close
	set MyFile = nothing 
	'这里放上危险代码 你可以继续增加你认为危险的代码
	sStr=".getfolder|.createfolder|.deletefolder|.createdirectory|.deletedirectory|.saveas|wscript.shell|script.encode|server.|.createobject|execute|activexobject|language=" 
	sNoString = split(sStr,"|") 
	for i= 0 to ubound(sNoString)
	   if instr(sTextAll,sNoString(i)) then 
		 checkfilesafe = False '有任意异常 都提示出错 
	   end if
	next
End Function
'****************例子**********************
'dim FilePath
'FilePath = "test1.asp"
'if not checkfilesafe(FilePath) then
'	'删除操作代码执行
'	response.Write("文件异常 已删除")	
'else
'	response.Write("文件正常")	
'end if	 
%>