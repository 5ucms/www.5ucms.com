<%
'********************************************** 
'正则表达式函数
'创150130 改150725 QQ3876307 邱嵩松 www.5ucms.com
'********************************************** 
'自动闭合HTML标签
Function closeHTML(strContent) 
	Dim arrTags, i, OpenPos, ClosePos, re, strMatchs, j, Match 
	Set re = New RegExp 
	re.IgnoreCase = True 
	re.Global = True 
	arrTags = Array("p", "div", "span", "table", "ul", "font", "b", "u", "i", "h1", "h2", "h3", "h4", "h5", "h6") 
	For i = 0 To UBound(arrTags) 
		OpenPos = 0 
		ClosePos = 0  
		re.Pattern = "\<" + arrTags(i) + "( [^\<\>]+|)\>" 
		Set strMatchs = re.Execute(strContent) 
		For Each Match in strMatchs 
			OpenPos = OpenPos + 1 
		Next 
		re.Pattern = "\</" + arrTags(i) + "\>" 
		Set strMatchs = re.Execute(strContent) 
		For Each Match in strMatchs 
			ClosePos = ClosePos + 1 
		Next 
		For j = 1 To OpenPos - ClosePos 
			strContent = strContent + "</" + arrTags(i) + ">" 
		Next 
	Next 
	closeHTML = strContent 
End Function

'获取所需内容 str为内容 regstr为正则规则 只提取第一个
Function GetCode(str,regstr)
	Dim Reg,serStr,Cols
	Set Reg= new RegExp
	Reg.IgnoreCase = True
	Reg.MultiLine = True
	Reg.Pattern =regstr
	If Reg.test(str) Then '若查询到匹配项
	   Set Cols = Reg.Execute(str)
	   GetCode=Cols(0).SubMatches(0) '使用匹配到的第一个匹配项
	Else 
	   GetCode=""
	End If
	Set Cols = Nothing
	Set Reg = Nothing
End Function

'获取所需内容 str为内容 regstr为正则规则 提取全部 fenge代表分割符
Function GetCodes(str,regstr,fenge)
	Dim Reg,serStr,Matches,Match
	Set Reg= new RegExp
	Reg.IgnoreCase = True
	Reg.Global = True
	Reg.Pattern =regstr
	Set Matches = Reg.Execute(str)
	For Each Match in Matches
		GetCodes = GetCodes & Match.Value & fenge '使用匹配到的第一个匹配项
	Next
	Set Matches = Nothing
	Set Reg = Nothing
End Function 


'过滤大于5位的数字
Function repnum(strng,times)  '以数组返回
	dim regEx,Matches,Match
    i = 0
    Set regEx = New RegExp
    regEx.Pattern = "(\d+)" '"[0-9]"
    regEx.IgnoreCase = True
    regEx.Global = True
    Set Matches = regEx.Execute(strng)
    For Each Match in Matches
		If  Len(Match.Value)>=times Then
		  strng = Replace(strng, Right(Match.Value,times),"****")
		End If 
        i = i + 1
    Next
    repnum = strng
End Function

'去除图片
Function noImg(str) 
	dim re 
	Set re=new RegExp 
	re.IgnoreCase =true 
	re.Global=True 
	re.Pattern="(\<img[^\<]*\>)" 
	str=re.replace(str,"") 
	noImg=str 
	set re=nothing 
End function

'去除www.和.com 将str内的网址部分替换成过滤词str2 但包含strout的词（多个例外用,分割） 不过滤
Function nowww(str,str2,strout)
	Dim re,Matches,Match,domains
	domains = "com|org|cc|net|cn|hk|uk|us|jp|net|org|edu|tv|info|name|biz|la"  '常用网址后缀 不加这个容易识别错误
	Set re=new RegExp 
	re.IgnoreCase =True 
	re.Global=True 
	re.Pattern = "(http(s)?://|ftp://|)([\w-]+\.)+[\w-]+\.("&domains&")?" 
	're.Pattern = "(http(s)?://|ftp://|)([\w-]+\.)+[\w-]+(/[\w- ./?%&=]*)?" 
	Set Matches = re.Execute(str)   
	For Each Match in Matches 
			if instr(strout,",")>0 then
				dim stroutk,k,matchk
				matchk = 0
				stroutk = split(strout,",")
				for k = 0 to ubound(stroutk) 
					if instr(Match.Value,stroutk(k))>0 then matchk = 1 
				next
				if matchk = 0 then str = replace(str,Match.Value,str2) 
			else
				if instr(Match.Value,strout)=0 then str = replace(str,Match.Value,str2)
			end if	 
	Next 
	nowww = str  
	Set re=Nothing 
End function

'去除带图链接和图
Function noImga(str) 
	dim re 
	Set re=new RegExp 
	re.IgnoreCase =true 
	re.Global=True  
	re.Pattern = "<a[^\<]*>\<img[^\<]*\></a>"
	str=re.replace(str,"") 
	noImga=str 
	set re=nothing 
End function

'去除str中 从a到b的全部内容
Function noab(str,a,b) 
	dim re 
	Set re=new RegExp 
	re.IgnoreCase =true 
	re.Global=True 
	re.Pattern = "("&a&")+[^<>]*>[^\0]*("&b&")+"
	str=re.replace(str,"") 
	noab=str 
	set re=nothing 
End function 

'去除style
Function nostyle(str) 
	dim re 
	Set re=new RegExp 
	re.IgnoreCase =true 
	re.Global=True 
	re.Pattern = "(<style)+[^<>]*>[^\0]*(<\/style>)+"
	str=re.replace(str,"") 
	nostyle=str 
	set re=nothing 
End function


'正则表达式 过滤 层 div 标记
Function nodiv(str) 
	dim re 
	Set re=new RegExp 
	re.IgnoreCase =true 
	re.Global=True 
	re.Pattern = "<(\/){0,1}div[^<>]*>"
	str=re.replace(str,"") 
	nodiv=str 
	set re=nothing 
End function

'正则表达式 过滤 链接 a 标记 ：
Function noa(str) 
	dim re 
	Set re=new RegExp 
	re.IgnoreCase =true 
	re.Global=True 
	re.Pattern = "<(\/){0,1}a[^<>]*>"
	str=re.replace(str,"") 
	noa=str 
	set re=nothing 
End function
 
'正则表达式 过滤 字体 font 标记 ：
Function nofont(str) 
	dim re 
	Set re=new RegExp 
	re.IgnoreCase =true 
	re.Global=True 
	re.Pattern = "<(\/){0,1}font[^<>]*>"
	str=re.replace(str,"") 
	nofont=str 
	set re=nothing 
End function

'去除span及内部一切
Function killspan(str) 
	dim re 
	Set re=new RegExp 
	re.IgnoreCase =true 
	re.Global=True 
	re.Pattern = "(<span.*?/span>)+"
	str=re.replace(str,"") 
	killspan=str 
	set re=nothing 
End function

'正则表达式 过滤 span 标记 保留span内部内容 ：
Function nospan(str) 
	dim re 
	Set re=new RegExp 
	re.IgnoreCase =true 
	re.Global=True 
	re.Pattern = "<(\/){0,1}span[^<>]*>"
	str=re.replace(str,"") 
	nospan=str 
	set re=nothing 
End function
 
'正则表达式 过滤 object 标记 ：
Function noobject(str) 
	dim re 
	Set re=new RegExp 
	re.IgnoreCase =true 
	re.Global=True 
	re.Pattern = "<object.*?/object>"
	str=re.replace(str,"") 
	noobject=str 
	set re=nothing 
End function

'正则表达式 过滤 iframe 标记：
Function noiframe(str) 
	dim re 
	Set re=new RegExp 
	re.IgnoreCase =true 
	re.Global=True 
	re.Pattern = "(<iframe){1,}[^<>]*>[^\0]*(<\/iframe>){1,}"
	str=re.replace(str,"") 
	noiframe=str 
	set re=nothing 
End function
 
'正则表达式 过滤 js script ：
Function nojs(str) 
	dim re 
	Set re=new RegExp 
	re.IgnoreCase =true 
	re.Global=True 
	re.Pattern = "(<script){1,}[^<>]*>[^\0]*(<\/script>){1,}"
	str=re.replace(str,"") 
	nojs=str 
	set re=nothing 
End function
 

'正则表达式 过滤 Class 标记 ：
Function noclass(str) 
	dim re 
	Set re=new RegExp 
	re.IgnoreCase =true 
	re.Global=True 
	re.Pattern = "(class=){1,}(""|\'){0,1}\S+(""|\'|>|\s){0,1}"   
	str=re.replace(str,"") 
	noclass=str 
	set re=nothing 
End function 
 
'去除HTML
Function noHTML(strHTML) 
	Dim objRegExp, Match, Matches 
	Set objRegExp = New Regexp  
	objRegExp.IgnoreCase = True 
	objRegExp.Global = True 
	objRegExp.Pattern = "<.+?>" 
	Set Matches = objRegExp.Execute(strHTML) 
	For Each Match in Matches 
	strHtml=Replace(strHTML,Match.value,"") 
	Next 
	noHTML=strHTML 
	Set objRegExp = Nothing 
End Function
%>