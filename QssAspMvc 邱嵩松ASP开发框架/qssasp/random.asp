<%
'********************************************** 
'随机数 必须以http://开头的
'QQ3876307 邱嵩松 www.5ucms.com
'建150130 改150520
'********************************************** 
'获取一个随机数 范围可定
Function RndNumber(MaxNum,MinNum)
 Randomize 
 RndNumber=int((MaxNum-MinNum+1)*rnd+MinNum)
 RndNumber=RndNumber
End Function

'从以way分割的字符串str中取出一个值
function rndk(str,way) 
	dim s,s1,n,n1 
	s=str
	s1=split(s,way) 
	Randomize 
	n=Int((ubound(s1) - lbound(s1) + 1) * Rnd + lbound(s1)) 
	Randomize 
	n1=Int((10 - 1 + 1) * Rnd) 
	rndk=s1(n1 mod ubound(s1)+1)
end function

'获取一个指定位数的随机字符串 150520
Function gen_key(digits)	
	dim output,num
	dim char_array(50)
	char_array(0) = "a"
	char_array(1) = "b"
	char_array(2) = "c"
	char_array(3) = "d"
	char_array(4) = "e"
	char_array(5) = "f"
	char_array(6) = "g"
	char_array(7) = "h"
	char_array(8) = "i"
	char_array(9) = "j"
	char_array(10) = "k"
    char_array(11) = "l"
    char_array(12) = "m"
    char_array(13) = "n"
    char_array(14) = "o"
    char_array(15) = "p"
    char_array(16) = "q"
    char_array(17) = "r"
    char_array(18) = "s"
    char_array(19) = "t"
    char_array(20) = "u"
    char_array(21) = "v"
    char_array(22) = "w"
    char_array(23) = "x"
    char_array(24) = "y"
    char_array(25) = "z"
    char_array(26) = "0"
    char_array(27) = "1"
    char_array(28) = "2"
    char_array(29) = "3"
    char_array(30) = "4"
    char_array(31) = "5"
    char_array(32) = "6"
    char_array(33) = "7"
    char_array(34) = "8"
    char_array(35) = "9"
	
	'可以继续加下去，但一般这样够用了
	randomize 
 
	do while len(output) < digits
	num = char_array(Int((35 - 0 + 1) * Rnd + 0)) '上边到35 这里就填35 更多 对应改之
	output = output + num
	loop
	
	'Set return
	gen_key = output
End Function
%>