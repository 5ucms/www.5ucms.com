If Exists (Select * From dbo.sysobjects Where id = object_id(N'[dbo].[{pre}Channel]') and OBJECTPROPERTY(id, N'IsUserTable') = 1) Drop Table [dbo].[{pre}Channel]
If Exists (Select * From dbo.sysobjects Where id = object_id(N'[dbo].[{pre}Content]') and OBJECTPROPERTY(id, N'IsUserTable') = 1) Drop Table [dbo].[{pre}Content]
If Exists (Select * From dbo.sysobjects Where id = object_id(N'[dbo].[{pre}Comment]') and OBJECTPROPERTY(id, N'IsUserTable') = 1) Drop Table [dbo].[{pre}Comment]
If Exists (Select * From dbo.sysobjects Where id = object_id(N'[dbo].[{pre}Sitelink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1) Drop Table [dbo].[{pre}Sitelink]
If Exists (Select * From dbo.sysobjects Where id = object_id(N'[dbo].[{pre}Label]') and OBJECTPROPERTY(id, N'IsUserTable') = 1) Drop Table [dbo].[{pre}Label]
If Exists (Select * From dbo.sysobjects Where id = object_id(N'[dbo].[{pre}Admin]') and OBJECTPROPERTY(id, N'IsUserTable') = 1) Drop Table [dbo].[{pre}Admin]
If Exists (Select * From dbo.sysobjects Where id = object_id(N'[dbo].[{pre}Upload]') and OBJECTPROPERTY(id, N'IsUserTable') = 1) Drop Table [dbo].[{pre}Upload]
If Exists (Select * From dbo.sysobjects Where id = object_id(N'[dbo].[{pre}Diypage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1) Drop Table [dbo].[{pre}Diypage]
If Exists (Select * From dbo.sysobjects Where id = object_id(N'[dbo].[{pre}Tags]') and OBJECTPROPERTY(id, N'IsUserTable') = 1) Drop Table [dbo].[{pre}Tags]
If Exists (Select * From dbo.sysobjects Where id = object_id(N'[dbo].[{pre}Bots]') and OBJECTPROPERTY(id, N'IsUserTable') = 1) Drop Table [dbo].[{pre}Bots]
If Exists (Select * From dbo.sysobjects Where id = object_id(N'[dbo].[{pre}Config]') and OBJECTPROPERTY(id, N'IsUserTable') = 1) Drop Table [dbo].[{pre}Config]
If Exists (Select * From dbo.sysobjects Where id = object_id(N'[dbo].[{pre}Plus]') and OBJECTPROPERTY(id, N'IsUserTable') = 1) Drop Table [dbo].[{pre}Plus]

Create Table [{pre}Channel] ([ID] Int IDENTITY (1, 1) NOT NULL PRIMARY KEY,[Cid] Int Default 0,[FatherID] Int Default 0,[ChildID] text,[ChildIDs] text,[DeepPath] Int Default 0,[Name] Varchar(500),[Order] Int Default 0,[Table] Varchar(40),[Domain] Varchar(200),[OutSideLink] Int Default 0,[Templatechannel] Varchar(200),[Templateclass] Varchar(200),[Templateview] Varchar(200),[Ruleindex] Varchar(200),[Rulechannel] Varchar(200),[Ruleview] Varchar(200),[Picture] Varchar(200),[Keywords] Varchar(200),[Description] Varchar(500),[Needcreate]  Int Default 0,[ModeEXT] Varchar(500))
Create Table [{pre}Content] ([ID] Int IDENTITY (1, 1) NOT NULL PRIMARY KEY,[Cid] Int Default 0,[Sid] Int Default 0,[Title] Varchar(500),[Style] Varchar(40),[Author] Varchar(200),[Source] Varchar(500),[Jumpurl] Varchar(400),[Keywords] Varchar(500),[Description] Varchar(500),[Commend] Int Default 0,[Indexpic] Varchar(500),[Views] Int Default 0,[Diggs] Int Default 0,[Comments] Int Default 0,[IsComment] Int Default 0,[Order] Int Default 0,[Filepath] Varchar(500),[Viewpath] Varchar(500),[Diyname] Varchar(400),[Createtime] DateTime Default {date},[Modifytime] DateTime Default {date},[Display] Int Default 0,[ModeIndex] Varchar(500))
Create Table [{pre}Comment] ([ID] Int IDENTITY (1, 1) NOT NULL PRIMARY KEY,[Aid] Int Default 0,[Cid] Int Default 0,[User] Varchar(20),[Content] Varchar(500),[Recomment] Varchar(500),[Ip] Varchar(40),[Time] DateTime Default {date},[State] Int Default 0)
Create Table [{pre}Sitelink] ([ID] Int IDENTITY (1, 1) NOT NULL PRIMARY KEY,[Text] Varchar(40),[Description] Varchar(500),[Link] Varchar(500),[Order] Int Default 0,[Replace] Int Default 0,[Target] Int Default 0,[State] Int Default 0)
Create Table [{pre}Label] ([ID] Int IDENTITY (1, 1) NOT NULL PRIMARY KEY,[Name] Varchar(100),[Info] Varchar(500),[Code] text)
Create Table [{pre}Admin] ([ID] Int IDENTITY (1, 1) NOT NULL PRIMARY KEY,[Username] Varchar(40),[Password] Varchar(100),[Levels] Varchar(400),[ManagePlus] Varchar(400),[ManageChannel] Varchar(400),[DiyMenu] text,[Uploadfileexts] Varchar(200),[Uploadfilesize] Varchar(100),[CheckCode] Varchar(200))
Create Table [{pre}Upload] ([ID] Int IDENTITY (1, 1) NOT NULL PRIMARY KEY,[Aid] Int Default 0,[Cid] Int Default 0,[Dir] Varchar(500),[Ext] Varchar(40),[Time] DateTime Default {date})
Create Table [{pre}Diypage] ([ID] Int IDENTITY (1, 1) NOT NULL PRIMARY KEY,[Dir] Varchar(400),[tpl] Varchar(200),[Title] Varchar(400),[Keywords] Varchar(400),[Description] Varchar(500),[Html] text)
Create Table [{pre}Tags] ([ID] Int IDENTITY (1, 1) NOT NULL PRIMARY KEY,[Name] Varchar(100),[Count] Int Default 0,[Modifytime] DateTime Default {date})
Create Table [{pre}Bots] ([ID] Int IDENTITY (1, 1) NOT NULL PRIMARY KEY,[BotName] Varchar(100),[LastDate] DateTime Default {date})
Create Table [{pre}Config] ([ID] Int IDENTITY (1, 1) NOT NULL PRIMARY KEY,[Title] Varchar(100),[Name] Varchar(100),[Value] text,[Values] Varchar(500),[Data] Varchar(40),[Form] Varchar(40),[Description] Varchar(500),[Order] Int Default 0)
Create Table [{pre}Plus] ([ID] Int IDENTITY (1, 1) NOT NULL PRIMARY KEY,[Name] Varchar(100))

CREATE INDEX [IX_{pre}Content_Cid] ON [{pre}Content] ([Cid])
CREATE INDEX [IX_{pre}Content_Title] ON [{pre}Content] ([Title])
CREATE INDEX [IX_{pre}Content_Indexpic] ON [{pre}Content] ([Indexpic])
CREATE INDEX [IX_{pre}Content_Order] ON [{pre}Content] ([Order])
CREATE INDEX [IX_{pre}Content_Display] ON [{pre}Content] ([Display])