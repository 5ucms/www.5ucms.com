<!--#Include File="../Inc/Const.Asp"-->
<%

Dim ID,Key,Rs,i

Dim Plus
Dim PlusName : PlusName = Request("plusname")
PlusName = Replace(PlusName,"'","")

On Error Resume Next

Select Case LCase(Request("Act"))
Case "chktitle" ' 检查标题
	Key = Replace(Request("Key"),"'","")
	If Len(Key) > 0 Then
		Set Rs = DB("Select [ID] From [{pre}Content] Where [Title]='" & Key & "'",1)
		If Rs.Eof Then
			Response.Write "True|标题“" & Key & "可以正常使用！"
		Else
			Response.Write "False|标题为“" & Key & "”的文章已存在！"
		End If
		Rs.Close : Set Rs = Nothing
	End If
Case "modeext"
	dim ecid,eid,es,modeext,modeindex
	ecid=Replace(Request("cid"),"'","")
	eid=Replace(Request("id"),"'","")
	if len(ecid)=0 or cstr(ecid)="0" then
		response.write "True|"
	else
		set rs=db("select [modeext] from [{pre}channel] where id=" & ecid,1)
		if rs.eof then
			response.write "True|"
		else
			if len(eid)=0 or cstr(eid)="0" then
				modeindex=""
			else
				set es=db("select [modeindex] from [{pre}content] where id=" & eid,1)
				if not es.eof then modeindex=es(0) else modeindex=""
				es.close
			end if
			if len(rs(0)&"")=0 then 
				response.write "True|"
			else
				response.write "True|" & vbcrlf
				modeext=split(rs(0),vbcrlf)
				for i=0 to ubound(modeext)
					if instr(modeext(i),":")>0 then
					response.write "<div style='float:left;width:206px;margin:4px 0;color:green;'>" & split(modeext(i),":")(0) & ": "
					response.write "<input style='width:180px;border:1px solid #cccccc;padding:2px;' name='ext" & split(modeext(i),":")(1) & "' value=""" & replace(getextval(split(modeext(i),":")(1),modeindex),"|","#｜#") & """ /></div>"
					end if
				next
			end if
		end if
		rs.close
	end if
	
Case "chkdiyname" ' 标签自定义文件名
	Key = Replace(Request("Key"),"'","")
	If Len(Key) > 0 Then
		If IsNumeric(Key) Or IsNumeric(Replace(Key,"-","")) Then
			Response.Write "False|自定义文件名“" & Key & "”不允许为纯数字！"
		Else
			ID = Request("ID")
			If Len(ID) > 0 And IsNumeric(ID) Then
				Set Rs = DB("Select [ID] From [{pre}Content] Where [ID]<>" & ID & " And [Diyname]='" & Key & "'",1)
			Else
				Set Rs = DB("Select [ID] From [{pre}Content] Where [Diyname]='" & Key & "'",1)
			End If
			If Rs.Eof Then
				Response.Write "True|自定义文件名“" & Key & "”可以使用！"
			Else
				Response.Write "False|自定义文件名“" & Key & "”已存在,请重试！"
			End If
			Rs.Close : Set Rs = Nothing
		End If
	End If
case "contentstate" ' 内容状态
	ID = Request("ID") : Dim Cfile
	If IsNumeric(ID) And Len(ID) >= 1 Then
		Set Rs = DB("Select [Display] From [{pre}Content] Where [ID]=" & ID,3)
		If Not Rs.Eof Then
			If Rs(0) = 0 Then
				Cfile = true
				Rs(0) = 1 : Response.Write "True|显示"
			Else
				Rs(0) = 0 : Response.Write "True|<font color=blue>隐藏</font>"
			End If
			Rs.Update
		Else
			Response.Write "False|文章不存在！"
		End If
		Rs.Close : Set Rs = Nothing
		If Cfile Then Call CreateContent(ID,0)
	Else
		Response.Write "False|参数格式不正确！"
	End If
case "contentcommend" '内容推荐
	ID = Request("ID")
	If IsNumeric(ID) And Len(ID) >= 1 Then
		Set Rs = DB("Select [Commend] From [{pre}Content] Where [ID]=" & ID,3)
		If Not Rs.Eof Then
			If Rs(0) = 0 Then
				Rs(0) = 1 : Response.Write "True|<font color=red>推荐</font>"
			Else
				Rs(0) = 0 : Response.Write "True|普通"
			End If
			Rs.Update
		Else
			Response.Write "False|评论不存在！"
		End If
		Rs.Close : Set Rs = Nothing
	Else
		Response.Write "False|参数格式不正确！"
	End If

Case "plusinstall" ' 安装插件
	If Not ChkLevel("setting") Then Response.write "False|无权安装" : Response.End
	Set Plus = New Cls_Plus
	Call Plus.Open(PlusName)
	If Plus.ErrorCode = 0 Then
		Dim Install
		Install = Plus.Main("install")
		Install = Replace(Install,Vbcr,Vbcrlf)
		Install = Replace(Install,Vblf,Vbcrlf)
		Install = Replace(Install,"	","")

		Install = Split(Install,Vbcrlf)
		On Error Resume Next : Err.Clear
		For i = 0 To Ubound(Install)
			If LCase(DataBaseType & ":") = LCase(Left(Install(i),Len(DataBaseType)+1)) Then Call DB(Right(Install(i),Len(Install(i))-(Len(DataBaseType)+1)),0)
		Next

		If Err Then
			Response.Write "True|安装失败(" & Err.Description & ")"
			Err.Clear
		Else
			For i = 0 To Plus.ConfigLength
				If LCase(Plus.ConfigItem(i,"name")) = "state" Then
					Call Plus.ConfigSave(i,1) : Exit For
				End If
			Next
			Response.Write "True|安装成功"
			Set Rs = DB("Select [Name] From [{pre}PLus] Where [Name]='" & PlusName & "'",3)
			If Rs.Eof Then Rs.AddNew : Rs(0) = PlusName : Rs.Update : Rs.CLose
		End If

	Else
		Resposne.Write "True|读取配置失败"
	End If
Case "plusuninstall" ' 卸载插件
	If Not ChkLevel("setting") Then Response.write "False|无权卸载" : Response.End
	Set Plus = New Cls_Plus
	Call Plus.Open(PlusName)
	If Plus.ErrorCode = 0 Then
		Dim UnInstall
		UnInstall = Plus.Main("uninstall")
		UnInstall = Replace(UnInstall,Vbcr,Vbcrlf)
		UnInstall = Replace(UnInstall,Vblf,Vbcrlf)
		UnInstall = Replace(UnInstall,"	","")
		UnInstall = Split(UnInstall,Vbcrlf)
		On Error Resume Next : Err.Clear
		For i = 0 To Ubound(UnInstall)
			If LCase(DataBaseType & ":") = LCase(Left(UnInstall(i),Len(DataBaseType)+1)) Then Call DB(Right(UnInstall(i),Len(UnInstall(i))-(Len(DataBaseType)+1)),0)
		Next
		If Err Then
			Response.Write "True|卸载失败(" & Err.Description & ")":Err.Clear
		Else
			For i = 0 To Plus.ConfigLength
				If LCase(Plus.ConfigItem(i,"name")) = "state" Then
					Call Plus.ConfigSave(i,0) : Exit For
				End If
			Next
			Response.Write "True|卸载成功"
			Call DB("Delete From [{pre}Plus] Where [Name]='" & PlusName & "'",0) '删除
		End If
	Else
		Resposne.Write "True|读取配置失败"
	End If
Case Else
	Response.Write "False|未指定的查询操作！"
End Select
If Err Then Response.Write "False|ERR: " & Err.Description
If IsObject(Conn) Then Conn.Close
%>