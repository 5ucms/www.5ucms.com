<%
' 创建列表页面
Function CreateChannel(ByVal Cid, ByVal Page)

	'增加识别判断 150514 用于独立手机站
	dim str,wapath,qssch
	If instr(Page,"$")>0 then '手机机模式
		str = split(Page,"$")
		Page = str(0)
		wapath = str(1)
		qssch = qsscreatehtml(wapath,1) '读取WAP站生成数据 150520 必须此顺序
		wapath = qsscreatehtml(wapath,0) '读取WAP站生成数据 150520
	else	'PC站模式
		wapath = ""
	end if 
	'增加识别判断 150514 用于独立手机站 以下带 wapath 的部分都是改过的
	
    Dim Rs, Ns
    Set Rs = DB("Select [ID],[Name],[ChildID],[ChildIDs],[Picture],[Keywords],[Description],[Domain],[Cid],[FatherID],[DeepPath],[FatherId] From [{pre}Channel] Where [ID]=" & Cid, 1)
    If Rs.EOF Then Response.Write "栏目不存在": Conn.Close: CreateChannel = False: Response.End

    ' 读取对应模板
    Dim NTemplate
    If Len(Rs(2)) > 0 Then NTemplate = GetChannel(Cid, "Templatechannel") Else NTemplate = GetChannel(Cid, "Templateclass") ' 获取封面或列表模板
	if len(wapath)>0 then NTemplate = replace(NTemplate,installdir & templatedir,installdir & wapath & "/" & templatedir) : NTemplate = NTemplate & "$" & wapath & ":" &qssch  '150514 用于独立手机站
	'                      将默认的模板绝对路径 转化为 手机站的 对应模板 据绝对路径                                           ：     添加后缀方便后方程序识别区分
	
    ' 缓存模板信息
    Dim TPL, HTML, Tid, Turl, Tmp
    Set TPL = New Cls_Template
    If Not ChkCache(Cid & "_" & NTemplate) Then
        Call TPL.Load(NTemplate) ' 载入模板
		TPL.Content = ReplaceX(TPL.Content,"{field:title}",rs("name"))
        TPL.Content = TPL.Parser_Tags("\{field:(.+?)\}", TPL.Content, Rs) ' 本栏目信息替换
		
		TPL.Content = ReplaceX(TPL.Content, "{tag:sitepath}", getsitepath(rs("id"),wapath& ":" &qssch)) '#当前路径 150521 支持手机站浏览模式
		
        Call TPL.Parser_My
        Call TPL.Parser_Sys
        Call TPL.Parser_Com
        Call SetCache(Cid & "_" & NTemplate, TPL.Content)
    End If
    TPL.Content = GetCache(Cid & "_" & NTemplate) ' 获取缓存模板信息
    TPL.Cid = Cid
    TPL.Page = Page
    Call TPL.Parser_Page
    Call TPL.Parser_Com
    Call TPL.Parser_IF
    HTML = TPL.Content
    
	'150531 栏目页第1页不显示page值，第2页起显示
	if page = 1 then
    	HTML = Replace(HTML,"{page}","")
	else
		HTML = Replace(HTML,"{page}","第"&page&"页")
	end if
	
	if len(wapath&"")=0 then qssch = Createhtml  '150516 支持手机站浏览模式
	Dim Tmps, TmpU,PageFile

	If qssch = 1 Then
		' 保存文件 
		If Page = 1 Then
			PageFile = GetChannel(Cid, "Ruleindex")
			if installdir = "/" then PageFile = "/"& wapath &PageFile else PageFile = replace(PageFile,installdir,installdir & wapath & "/")
			Call CreateFile(HTML, PageFile & "Index." & Defaultext) ' 保存封面
		Else
			PageFile = ReplaceX(GetChannel(Cid, "Ruleindex") & GetChannel(Cid, "Rulechannel"), "{page}", Page)
			If Right(PageFile, 1) = "/" Then PageFile = PageFile & "Index." & Defaultext
			Tmps = Split(PageFile, "/"): TmpU = Tmps(UBound(Tmps))
			If InStr(TmpU, ".") = 0 Then PageFile = PageFile & "." & Defaultext
			if installdir = "/" then PageFile = "/"& wapath &PageFile else PageFile = replace(PageFile,installdir,installdir & wapath & "/")
			Call CreateFile(HTML, PageFile) ' 创建列表页
		End If
		CreateChannel = HTML ' 也返回HTML
	Else
		CreateChannel = HTML ' 动态返回
	End If
	
    Set TPL = Nothing
    Rs.Close: Set Rs = Nothing
End Function

' 获取列表分页记录数
Function GetListSize(ByVal Tmp)
    If Not ChkCache("GetListSize_" & Tmp) Then
        Dim TPL, Reg, Matches, Match
        Set TPL = New Cls_Template
        Set Reg = New RegExp
        Reg.Ignorecase = True
        Reg.Global = True
        Call TPL.Load(Tmp) ' 载入模板
        Reg.Pattern = "<!--Page:\{(.+?)\}-->([\s\S]*?)<!--Page-->"
        Set Matches = Reg.Execute(TPL.Content)
        For Each Match In Matches
            GetListSize = TPL.GetAttr(Match.SubMatches(0), "size", True)
        Next
        If Len(GetListSize) = 0 Then GetListSize = 10
        If Not IsNumeric(GetListSize) Then GetListSize = 10 Else GetListSize = Int(GetListSize)
        If GetListSize < 1 Then GetListSize = 10
        Call SetCache("GetListSize_" & Tmp, GetListSize)
    Else
        GetListSize = Int(GetCache("GetListSize_" & Tmp))
    End If
End Function
%>

