<%
'********************************************** 
'统计tag出现的次数
'150127 QQ3876307 邱嵩松 www.5ucms.com
'********************************************** 
'计算str2在 数组str1内一共出现了多少次
Function CountStr(str1,str2) 
	 dim tmp,i,j 
	 if str1="" or isnull(str1) then 
		j=0 
	 elseif str2="" or isnull(str2) then 
		j=1 
	 else 
		tmp=split(str1,str2) 
		j=ubound(tmp)
	  end if 
	  countstr=j
End function
%>