<%
'********************************************** 
'vbs cls_Cache类
'150127 QQ3876307 邱嵩松 www.5ucms.com
' 属性valid，是否可用，取值前判断 
' 属性name，cache名，新建对象后赋值 
' 方法add(值,到期时间)，设置cache内容 
' 属性value，返回cache内容 
' 属性blempty，是否未设置值 
' 方法makeEmpty，释放内存，测试用 
' 方法equal(变量1)，判断cache值是否和变量1相同 
' 方法expires(time)，修改过期时间为time 
' 邱嵩松修改的缓存类 可嵌入5UCMS
' ===下方为例子===
' Set myCache = new cls_Cache
' myCache.name= qssurl '定义缓存名称 
' if myCache.valid then '如果缓存有效
'  content = myCache.value&"<br>(本站缓存数据)" '读取缓存内容
' else
'  content = 赋值
'  myCache.add content,dateadd("n",1000,now) '将内容赋值给缓存，并设置缓存有效期是当前时间＋1000分钟
' end if
' set clsCache=nothing
' ===上方为例子===
' 清空缓存 myCache.makeEmpty()  
'********************************************** 

Class cls_Cache 
	private obj 'cache内容 
	private expireTime '过期时间 
	private expireTimeName '过期时间application名 
	private cacheName 'cache内容application名 
	private path 'uri 
	
	private sub class_initialize() 
		path=request.servervariables("url") 
		path=left(path,instrRev(path,"/")) 
	end sub 
	
	private sub class_terminate() 
	end sub 
	
	public property get blEmpty 
		'是否为空 
		if isempty(obj) then 
			blEmpty=true 
		else 
			blEmpty=false 
		end if 
	end property 
	
	public property get valid 
		'是否可用(过期) 
		if isempty(obj) or not isDate(expireTime) then 
			valid=false  
		'elseif CDate(expireTime) < now() then 
		elseif datediff("s",CDate(expireTime),now())>0 then 
			valid=false 
		else 
			valid=true 
		end if 
	end property 
	
	public property let name(str) 
		'设置cache名 
		cacheName=str & path 
		obj=application(cacheName) 
		expireTimeName=str & "expires" & path 
		expireTime=application(expireTimeName) 
	end property 
	
	public property let expires(tm) 
		'重设置过期时间 
		expireTime=tm 
		application.lock 
		application(expireTimeName)=expireTime 
		application.unlock 
	end property 
	
	public sub add(var,expire) 
		'赋值 
		if isempty(var) or not isDate(expire) then 
			exit sub 
		end if 
		obj=var 
		expireTime=expire 
		application.lock 
		application(cacheName)=obj 
		application(expireTimeName)=expireTime 
		application.unlock 
	end sub 
	
	public property get value 
		'取值 
		if isempty(obj) or not isDate(expireTime) then 
			value=null 
		elseif datediff("s",CDate(expireTime),now())>0 then 
			value=null 
		else 
			value=obj 
		end if 
	end property 
	
	public sub makeEmpty() 
		'释放application 
		application.lock 
		application(cacheName)=empty 
		application(expireTimeName)=empty 
		application.unlock 
		obj=empty 
		expireTime=empty 
	end sub 
	
	public function equal(var2) 
		'比较 
		if typename(obj)<>typename(var2) then 
			equal=false 
		elseif typename(obj)="Object" then 
			if obj is var2 then 
				equal=true 
			else 
				equal=false 
			end if 
		elseif typename(obj)="Variant()" then 
			if join(obj,"^")=join(var2,"^") then 
				equal=true 
			else 
				equal=false 
			end if 
		else
			if obj=var2 then 
				equal=true 
			else 
				equal=false 
			end if 
		end if 
	end function 

End Class 


'简易类

' 清除缓存
function clscache()
	clscachex:clscachex
end function
function clscachex()
	dim cacheobj
	application.lock
	for each cacheobj in application.contents
		if cstr(left(cacheobj, len(Cacheflag))) = cstr(Cacheflag) then application.contents.Remove (cacheobj)
	next
	application.unlock
end function

' 设置缓存
function setcache(byval cachename,byval cachevalue)
	dim cachedata
	cachename = lcase(filterstr(cachename))
	cachedata = application(Cacheflag & cachename)
	if isarray(cachedata) then
		cachedata(0) = Cachevalue
		cachedata(1) = now()
	else
		Redim cachedata(2)
		cachedata(0) = Cachevalue
		cachedata(1) = now()
	end if
	application.lock
	application(Cacheflag & cachename) = cachedata
	application.unlock
end function

' 获取缓存
function getcache(byval cachename)
	dim cachedata
	cachename = lcase(filterstr(cachename))
	cachedata = application(Cacheflag & cachename)
	if isarray(cachedata) then getcache = cachedata(0) else getcache = ""
end function

' 检测缓存
function chkcache(byval cachename)
	dim cachedata
	chkcache = false
	cachename = lcase(filterstr(cachename))
	cachedata = application(Cacheflag & cachename)
	if not isarray(cachedata) then exit function
	if not IsDate(cachedata(1)) then exit function
	if DateDiff("s", CDate(cachedata(1)), now()) < 60 * Cachetime then chkcache = true
end function
%>