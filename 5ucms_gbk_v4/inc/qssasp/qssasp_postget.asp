<%
'********************************************** 
'验证提交 相关函数
'150209 QQ3876307 邱嵩松 www.5ucms.com
'********************************************** 
'//过滤非法的SQL字符
Function ReplaceBadChar(strChar)
    If strChar = "" Or IsNull(strChar) Then
        ReplaceBadChar = ""
        Exit Function
    End If
    Dim strBadChar, arrBadChar, tempChar, i
    strBadChar = "+,',--,%,^,&,?,(,),<,>,[,],{,},/,\,;,:," & Chr(34) & "," & Chr(0) & ""
    arrBadChar = Split(strBadChar, ",")
    tempChar = strChar
    For i = 0 To UBound(arrBadChar)
        tempChar = Replace(tempChar, arrBadChar(i), "")
    Next
    tempChar = Replace(tempChar, "@@", "@")
    ReplaceBadChar = tempChar
End Function

'//检测是否外部提交数据
Function Chk_SQL_Post(byval strTemp) 
    StrTemp = LCase(StrTemp)
    If Instr(StrTemp,"select%20") or Instr(StrTemp,"insert%20") or Instr(StrTemp,"delete%20from") or Instr(StrTemp,"count(") or Instr(StrTemp,"drop%20table") or Instr(StrTemp,"update%20") or Instr(StrTemp,"truncate%20") or Instr(StrTemp,"asc(") or Instr(StrTemp,"mid(") or Instr(StrTemp,"char(") or Instr(StrTemp,"xp_cmdshell") or Instr(StrTemp,"exec%20master") or Instr(StrTemp,"net%20localgroup%20administrators") or Instr(StrTemp,"net%20user") or Instr(StrTemp,"%20or%20") or Instr(StrTemp,"'") or Instr(StrTemp,"%20") or Instr(StrTemp,"""") or Instr(StrTemp,"“") or Instr(StrTemp,"”") or Instr(StrTemp,"：") or Instr(StrTemp,";") or Instr(StrTemp,";") or Instr(StrTemp,",") or Instr(StrTemp,"，")  or Instr(StrTemp,"%") then
    Response.Write "发现SQL注入，已禁止！" : Response.end
	End if 
End Function

'//检测是否外部提交数据
Function Chk_Post()
	Dim Server_V1,Server_V2		
	Server_V1=Cstr(Request.ServerVariables("HTTP_REFERER"))
	Server_V2=Cstr(Request.ServerVariables("SERVER_NAME"))		
	If Mid(Server_V1,8,Len(Server_V2))<>Server_V2 Then
		response.redirect "default.asp"
		response.End 
	End If 
End Function

'检测敏感字符串(以,号隔开);返回:True(含有敏感词)/False 
Function BadWord(str,BadWordList)
	BadWord=False
	Dim arrBadWord
		arrBadWord=Split(BadWordList,",",-1,1)
	Dim regEx
	Set regEx=New RegExp
	regEx.IgnoreCase = True   
	regEx.Global = True
	Dim Match
	Dim I
	For I=0 To UBound(arrBadWord)
		If arrBadWord(I)<>"" Then
			regEx.Pattern=arrBadWord(I)
			Set Match=regEx.Execute(str)
			If Match.Count Then
				BadWord=True
				Exit For
			End If
		End If
	Next
End Function

'POST数据到指定：网址 数据 编码
Function Post(url,data,qsscharset)
	dim Https
	if qsscharset <> "gb2312" then qsscharset = "utf-8"
	set Https=server.createobject("MSXML2.XMLHTTP")
	Https.open "POST",url,false
	Https.setRequestHeader "Content-Type","application/x-www-form-urlencoded"
	Https.send data
	if Https.readystate=4 then
		dim objstream
		set objstream = Server.CreateObject("adodb.stream")
		objstream.Type = 1
		objstream.Mode =3
		objstream.Open
		objstream.Write Https.responseBody
		objstream.Position = 0
		objstream.Type = 2
		objstream.Charset = qsscharset
		Post = objstream.ReadText
		objstream.Close
		set objstream = nothing
		set https=nothing
	end if
End Function

'POST数据到指定2：网址 数据 编码 这个比较好用
function PostHTTPPage(url,data) 
    dim Http 
    set Http=server.createobject("MSXML2.SERVERXMLHTTP.3.0")
    Http.open "POST",url,false 
    Http.setRequestHeader "CONTENT-TYPE", "text/plain" 
    Http.send(data) 
    if Http.readystate<>4 then 
        exit function 
    End if
    PostHTTPPage=bytesToBSTR(Http.responseBody,"utf-8") 
    set http=nothing 
    if err.number<>0 then err.Clear 
End function

%>