<%
'********************************************** 
'进制转换类
'QQ3876307 邱嵩松 www.5ucms.com
'始150520  改150520
'********************************************** 
'判断是否为有效的十六进制代码
function isvalidhex(str)
dim c
isvalidhex=true
str=ucase(str)
if len(str)<>3 then isvalidhex=false:exit function
if left(str,1)<>"%" then isvalidhex=false:exit function
   c=mid(str,2,1)
if not (((c>="0") and (c<="9")) or ((c>="A") and (c<="Z"))) then isvalidhex=false:exit function
   c=mid(str,3,1)
if not (((c>="0") and (c<="9")) or ((c>="A") and (c<="Z"))) then isvalidhex=false:exit function
end function

'二进制转换为十六进制的asp代码
function c2to16(x)
    i=1
    for i=1 to len(x) step 4
       c2to16=c2to16 & hex(c2to10(mid(x,i,4)))
    next
end function

'二进制转换为十进制的asp代码
function c2to10(x)
    c2to10=0
    if x="0" then exit function
      i=0
    for i= 0 to len(x) -1
       if mid(x,len(x)-i,1)="1" then c2to10=c2to10+2^(i)
    next
end function

'十六进制转换为二进制的asp代码
function c16to2(x)
     i=0
     for i=1 to len(trim(x))
       tempstr= c10to2(cint(int("&h" & mid(x,i,1))))
       do while len(tempstr)<4
          tempstr="0" & tempstr
       loop
       c16to2=c16to2 & tempstr
    next
end function

%>