<%
'********************************************** 
'文件编码判断转换类
'QQ3876307 邱嵩松 www.5ucms.com
'始150128  改150520
'********************************************** 
'判断路径文件的编码
Function charset(path) 
  path = Server.MapPath(path) '转换为绝对路径
  set objstream=server.createobject("adodb.stream") 
  objstream.Type=1 
  objstream.mode=3 
  objstream.open 
  objstream.Position=0 
  objstream.loadfromfile path 
  bintou=objstream.read(2) 
  If AscB(MidB(bintou,1,1))=&HEF And AscB(MidB(bintou,2,1))=&HBB Then 
 	 charsetr="utf-8" 
  ElseIf AscB(MidB(bintou,1,1))=&HFF And AscB(MidB(bintou,2,1))=&HFE Then 
 	 charset="unicode" 
  Else 
 	 charset="gb2312" 
  End If 
  objstream.close 
  set objstream=nothing 
End function


'UTF-8转GB2312
function UTF2GB(UTFStr) 
for Dig=1 to len(UTFStr) 
   '如果UTF8编码文字以%开头则进行转换 
   if mid(UTFStr,Dig,1)="%" then 
      'UTF8编码文字大于8则转换为汉字 
     if len(UTFStr) >= Dig+8 then 
        GBStr=GBStr & ConvChinese(mid(UTFStr,Dig,9)) 
        Dig=Dig+8 
     else 
       GBStr=GBStr & mid(UTFStr,Dig,1) 
     end if 
   else 
      GBStr=GBStr & mid(UTFStr,Dig,1) 
   end if 
next 
UTF2GB=GBStr
end function  

'GB2312转UTF8的asp代码  将GB编码文字转换为UTF8编码文字
Function toUTF8(szInput)
     Dim wch, uch, szRet
     Dim x
     Dim nAsc, nAsc2, nAsc3
     '如果输入参数为空，则退出函数
     If szInput = "" Then
         toUTF8 = szInput
         Exit Function
     End If
     '开始转换
      For x = 1 To Len(szInput)
         '利用mid函数分拆GB编码文字
         wch = Mid(szInput, x, 1)
         '利用ascW函数返回每一个GB编码文字的Unicode字符代码
         '注：asc函数返回的是ANSI 字符代码，注意区别
         nAsc = AscW(wch)
         If nAsc < 0 Then nAsc = nAsc + 65536
    
         If (nAsc And &HFF80) = 0 Then
             szRet = szRet & wch
         Else
             If (nAsc And &HF000) = 0 Then
                 uch = "%" & Hex(((nAsc \ 2 ^ 6)) Or &HC0) & Hex(nAsc And &H3F Or &H80)
                 szRet = szRet & uch
             Else
                'GB编码文字的Unicode字符代码在0800 - FFFF之间采用三字节模版
                 uch = "%" & Hex((nAsc \ 2 ^ 12) Or &HE0) & "%" & _
                             Hex((nAsc \ 2 ^ 6) And &H3F Or &H80) & "%" & _
                             Hex(nAsc And &H3F Or &H80)
                 szRet = szRet & uch
             End If
         End If
     Next         
     toUTF8 = szRet
End Function

'GB2312中文转unicode(&#)的asp代码  将GB编码文字转换为unicode编码文字
function chinese2unicode(Str)
   dim i
   dim Str_one
   dim Str_unicode
   if(isnull(Str)) then
      exit function
   end if
   for i=1 to len(Str)
     Str_one=Mid(Str,i,1)
     Str_unicode=Str_unicode&chr(38)
     Str_unicode=Str_unicode&chr(35)
     Str_unicode=Str_unicode&chr(120)
     Str_unicode=Str_unicode& Hex(ascw(Str_one))
     Str_unicode=Str_unicode&chr(59)
   next
   chinese2unicode=Str_unicode
end function  

'URL地址编码解码函数 150520
Function URLDecode(enStr)
dim deStr
dim c,i,v
deStr=""
for i=1 to len(enStr)
   c=Mid(enStr,i,1)
   if c="%" then
    v=eval("&h"+Mid(enStr,i+1,2))
    if v<128 then
     deStr=deStr&chr(v)
     i=i+2
    else
     if isvalidhex(mid(enstr,i,3)) then
      if isvalidhex(mid(enstr,i+3,3)) then
       v=eval("&h"+Mid(enStr,i+1,2)+Mid(enStr,i+4,2))
       deStr=deStr&chr(v)
       i=i+5
      else
       v=eval("&h"+Mid(enStr,i+1,2)+cstr(hex(asc(Mid(enStr,i+3,1)))))
       deStr=deStr&chr(v)
       i=i+3
      end if
     else
      destr=destr&c
     end if
    end if
   else
    if c="+" then
     deStr=deStr&" "
    else
     deStr=deStr&c
    end if
   end if
next
URLDecode=deStr
end function
%>