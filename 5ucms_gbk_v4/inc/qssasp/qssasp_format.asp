<%
'********************************************** 
' 数据转换 处理
'150127 QQ3876307 邱嵩松 www.5ucms.com
'********************************************** 
'140123 解决小于0的小数显示时，前边不带0的bug
Function fmtnumber(nums)
 nums = cstr(nums)    '转换为字符
 if instr(nums,".") > 0 then  '如果中间有点号就说明是数字
  if left(nums,1) = "." then '如果截取的第一个符号是点号，就说明点号前面的0被省略了，那下面就是添加一个 例: .1218
   nums = "0" & nums
  else
   nums = nums    '这里验证就是第一个符号不是点号，就直接显示数据 例：19.1218  00.1218
  end if
 else
  nums = nums     '没有点号直接显示数据 例:191218
 end if
 fmtnumber = nums
end function


'这里将生成的关键词字符串，进行除重复词汇
function qssjoin(str)
	dim str1,i,j,key1,key2
	str1=split(str, ",")
		for i= 0 to ubound(str1)
			if len(str1(i))>0 then
				for j=i+1 to ubound(str1)
					if str1(i)=str1(j) then 
						str1(j)= ""
					end if
				next
			end if	
		next
	key1 = ""	
	key2 = ""
	for i= 0 to ubound(str1) 
		if str1(i) <> "" then 
			key2 = str1(i) 
			key1=key1&key2&"," 
		end if 
	next 
	qssjoin = key1
end function

'对一段指定的字符串应用 HTML 编码 防出错 15102
function qssHtmlEnCode(byval str)
	if len(str&"")=0 then 
		qssHtmlEnCode = ""
	else
		qssHtmlEnCode = Server.HtmlEnCode(str)
	end if
end function

%>