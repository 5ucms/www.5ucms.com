<%
'********************************************** 
'replace 安全替换优化函数 无视错误
'150127 QQ3876307 邱嵩松 www.5ucms.com
'********************************************** 
Function qssreplace(str,a,b) 
	'将str字符串中的a换成b
   if len(str&"") = 0 then qssreplace = "" : exit function
   if len(a&"") = 0 then qssreplace = str : exit function
   if len(b&"") = 0 then b = ""
   qssreplace = replace(str,a,b)
end function 

' 过滤非法字符
function filterstr(byval str)
	filterstr = lcase(str)
	filterstr = replace(filterstr, " ", "")
	filterstr = replace(filterstr, "'", "")
	filterstr = replace(filterstr, """", "")
	filterstr = replace(filterstr, "=", "")
	filterstr = replace(filterstr, "*", "")
	filterstr = replace(filterstr, "%", "")
	filterstr = replace(filterstr, "&", "")  
end function

%>
 