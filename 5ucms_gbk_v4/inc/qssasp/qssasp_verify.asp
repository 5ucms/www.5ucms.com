<%
'********************************************** 
'验证各种值
'150127 QQ3876307 邱嵩松 www.5ucms.com
'********************************************** 
'验证是否为网址
Function checkisUrl(tmpString) 
      dim c,i 
      checkisUrl = true 
      tmpString=Lcase(trim(tmpString)) 
      if left(tmpString,7)<>"http://" then checkisUrl = false : exit function 
      for i = 8 to Len(checkisUrl) 
            c = Lcase(Mid(tmpString, i, 1)) 
            if InStr("abcdefghijklmnopqrstuvwxyz_-./\", c) <= 0 and not IsNumeric(c) then 
                  checkisUrl = false 
                  exit function 
            end if 
      next 
      if Left(tmpString, 1) = "." or Right(tmpString, 1) = "." then 
            checkisUrl = false 
            exit function 
      end if

      if InStr(tmpString, ".") <= 0 then 
            checkisUrl = false  
            exit function 
      end if

      if InStr(checkisUrl, "..") > 0 then 
            checkisUrl = false 
      end if 
end function 
%>