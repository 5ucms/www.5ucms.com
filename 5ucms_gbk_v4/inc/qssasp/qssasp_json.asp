<%
'********************************************** 
'json数据处理 下方例子
'150127 QQ3876307 邱嵩松 www.5ucms.com
'Call InitScriptControl 
'Set objTest = getJSONObject(strTest)  'strTest为获取到的json数据 下方是调用例子
'content = "注册邮箱："&objTest.result.contact_email&"<br>注册时间："&objTest.result.dom_insdate&"<br>过期时间："&objTest.result.dom_expdate&"<br>注册人："&objTest.result.registrar&"<br>注册商："&objTest.result.sponsoring_registrar 
'********************************************** 
Dim sc4Json,objTest
'开启json数据模式
Sub InitScriptControl
	Set sc4Json = Server.CreateObject("MSScriptControl.ScriptControl")
	sc4Json.Language = "JavaScript"
	sc4Json.AddCode "var itemTemp=null;function getJSArray(arr, index){itemTemp=arr[index];}"
End Sub 
'解析JSON内容
Function getJSONObject(strJSON)
	sc4Json.AddCode "var jsonObject = " & strJSON
	Set getJSONObject = sc4Json.CodeObject.jsonObject
End Function
'解析JSON子内容 名称 子名称 位置数字
Sub getJSArrayItem(objDest,objJSArray,index)
	On Error Resume Next
	sc4Json.Run "getJSArray",objJSArray, index
	Set objDest = sc4Json.CodeObject.itemTemp
	If Err.number=0 Then Exit Sub
	objDest = sc4Json.CodeObject.itemTemp
End Sub
'待解析的JSON数据  
%>