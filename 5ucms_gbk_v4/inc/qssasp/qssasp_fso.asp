<%
'********************************************** 
'文件操作类
'150127 QQ3876307 邱嵩松 www.5ucms.com
'********************************************** 
'读取文件内容
Function LoadFile(LoadFileRoot)
	Dim Filename,fso,hndFile  
	Filename = Server.MapPath(LoadFileRoot)  
	Set fso = CreateObject("Scripting.FileSystemObject") 
	If Not fso.FileExists(Filename) Then response.write("指定文件" & LoadFileRoot & "不存在!") :response.end 
	set hndFile = fso.OpenTextFile(Filename)
 	 if not hndFile.atendofstream then   
 		LoadFile = hndFile.ReadAll
     end if
	Set hndFile = Nothing 
	Set fso = Nothing 
	If LoadFile = "" Then  response.write("不能读取指定文件" & LoadFileRoot & "或文件为空!"):response.end
End Function 

'判断文件是否存在  150412更新
Function FileExist(m_Root)
	FileExist = false
	dim MyFileObject,m_Root2
	m_Root2 = m_Root
	m_Root2 = server.mappath(m_Root2)
	'创建FSO对象
	Set MyFileObject=server.createobject(strobjectfso)
	IF MyFileObject.FileExists(m_Root2) then
		FileExist = true 
	END IF
End Function 

' 创建文件
function qsscreatefile(byval content,byval filedir)
	filedir = replace(filedir, "\", "/") : filedir = replace(filedir, "//", "/")
	if right(filedir, 1) = "/" then filedir = filedir & "index.html"
	call createfolder(filedir) 
	dim obj : set obj = server.createobject("adod" & "b.S" & "tream")
	obj.type = 2
	obj.open
	obj.charset = response.charset
	obj.position = obj.Size
	obj.writeText = content
	obj.savetofile server.mappath(filedir), 2
	obj.close
	if err then err.clear: createfile = false else createfile = true
	set obj = nothing
end function

 
' 创建文件夹
function createfolder(byval dirpath)
	dirpath = replace(dirpath, "\", "/") : dirpath = replace(dirpath, "//", "/")
	if not chkcache("create_folder_" & dirpath) then
		dim subpath, pathdeep, i
		dirpath = replace(server.mappath(dirpath), server.mappath("/"), "")
		subpath = split(dirpath, "\")
		pathdeep = pathdeep & server.mappath("/")
		for i = 1 to ubound(subpath) - 1
			pathdeep = pathdeep & "/" & subpath(i)
			if not fso.folderexists(pathdeep) then fso.createfolder pathdeep
		next
		call setcache("create_folder_" & dirpath, "true")
	end if
end function

' 删除文件
function deletefile(byval filedir)
	if len(filedir) = 0 or isnull(filedir) then exit function
	filedir = replace(filedir, "\", "/") : filedir = replace(filedir, "//", "/")
	if right(filedir, 1) = "/" then
		deletefile = deletefolder(filedir)
	else
		on error resume next
		fso.deletefile server.mappath(filedir)
		if err then err.clear: deletefile = false else deletefile = true
	end if
end function

' 删除文件夹
function deletefolder(byval dirpath)
	on error resume next
	fso.deletefolder server.mappath(dirpath)
	if err then err.clear: deletefolder = false else deletefolder = true
end function

'获取文件创建时间
function GetCreatedTime(dir)
	dim f,fs
	Set fs=server.createobject(strobjectfso)
	Set f=fs.GetFile(Server.MapPath(dir))  
	getcreatedtime =  f.DateCreated   
end function

'获取文件修改时间
function GetModifiedTime(dir)
	dim f,fs
	Set fs=server.createobject(strobjectfso)
	Set f=fs.GetFile(Server.MapPath(dir))  
	GetModifiedTime =  f.DateLastModified 
end function
%>