<%

Class Cls_qssLog

	Dim vID	
	Dim vUser	
	Dim vContent
	Dim vRecomment
	Dim vIp
	Dim vTime
	Dim vState,vuid
	Dim LastError

	Private Sub Class_Initialize()
		Call Initialize()
	End Sub

	Private Sub Class_Terminate()
		Call Initialize()
	End Sub

	Public Function Initialize()
		vID = 0		
		vUser = "Guest"
		vContent = ""
		vRecomment = ""
		vIp = ""
		vTime = Now()
		vState = 0
	End Function

	Public Function GetValue()		
		vUser = Request.Form("oUser")
		vContent = Request.Form("oContent")
		vRecomment = Request.Form("oRecomment")
		vIp = Request.Form("oIp")
		vTime = Request.Form("oTime")
		vState = Request.Form("oState")		
		If Len(vUser) < 1 Or Len(vUser) > 255 Then LastError = "事由的长度请控制在 1 至 255 位" : GetValue = False : Exit Function
		If Len(vContent) < 1 Or Len(vContent) > 255 Then LastError = "内容的长度请控制在 1 至 255 位" : GetValue = False : Exit Function
		If Len(vRecomment) > 255 Then LastError = "管理备注的长度请控制在 255 位以内" : GetValue = False : Exit Function
		If Len(vIp) < 7 Or Len(vIp) > 20 Then vIP = GetIP
		If Len(vTime) = 0 Or Not IsDate(vTime) Then LastError = "举报时间只能是时间格式" : GetValue = False : Exit Function
		If Len(vState) = 0 Or Not IsNumeric(vState) Then LastError = "审核状态只能是数字" : GetValue = False : Exit Function
		GetValue = True
	End Function

	Public Function SetValue()
		Dim Rs
		Set Rs = DB("Select * From [{pre}qssLog] Where [ID]=" & vID,1)
		If Rs.Eof Then Rs.Close : Set Rs = Nothing : LastError = "你所需要查询的记录 " & vID & " 不存在!" : SetValue = False : Exit Function
		vUser = Rs("User")
		vuid = Rs("uid")	
		vContent = Rs("Content")
		vRecomment = Rs("Recomment")
		vIp = Rs("Ip")
		vTime = Rs("Time")
		vState = Rs("State")
		Rs.Close
		Set Rs = Nothing
		SetValue = True
	End Function
	
	Public Function Create()
		'记录全为新增，无需判断旧记录，这酸爽。。。BY qq 3876307
		Set Rs = DB("Select * From [{pre}qssLog]",3)
		Rs.AddNew
		Rs("User") = vUser
		Rs("Content") = vContent
		Rs("Recomment") = vRecomment
		Rs("Ip") = vIp
		Rs("uid") =vuid
		Rs("Time") = vTime
		Rs("State") = vState
		Rs.Update
		Rs.Close
		Set Rs = Nothing
		Create= True
	End Function


	Public Function Modify()
		Dim Rs
		Set Rs = DB("Select * From [{pre}qssLog] Where [ID]=" & vID,3)
		If Rs.Eof Then Rs.Close : Set Rs = Nothing : LastError = "你所需要更新的记录 " & vID & " 不存在!" : Modify = False : Exit Function
		'Rs("User") = vUser
		'Rs("Content") = vContent
		Rs("Recomment") = vRecomment
		'Rs("Ip") = vIp
		'Rs("Time") = vTime
		'Rs("uid") =vuid 会员ID禁止修改
		'Rs("State") = vState
		'判断加分部分 审核一次
 
		
		Rs.Update
		Rs.Close
		Set Rs = Nothing
		Modify = True
	End Function

	Public Function Delete()		
		Call DB("Delete From [{pre}qssLog] Where [ID]=" & vID ,0) '# 删除举报
		Delete = True
	End Function
	
'新增批量删除按钮 by cssHhaier
	Public Function DeleteAll(Byval qssLogIDS)
		Dim Rs
		Set Rs = DB("Select [ID] From [{pre}qssLog] Where ID In (" & qssLogIDS & ")",1)
		Do While Not Rs.Eof
			Call DB("Delete From [{pre}qssLog] Where [ID]=" & Rs(0) ,0) '# 删除信息
			Rs.MoveNext
		Loop
		Rs.Close
	End Function

	Public Function Change()
		Dim GetField
		GetField = Request("Field")
		If Instr(LCase("[State]"), LCase("[" & GetField & "]")) = 0 Then
			LastError = "参数出错,无法修改记录!" : Change = False : Exit Function
		End If
		Dim Rs
		Set Rs = DB("Select [" & GetField & "] From [{pre}qssLog] Where [ID]=" & vID,3)
		If Rs.Eof Then
			Rs.Close : Set Rs = Nothing : LastError ="所需要更新的记录不存在!" : Change = False : Exit Function
		Else
			If Rs(GetField) = 0 Then Rs(GetField) = 1 Else Rs(GetField) = 0
			Rs.Update : Rs.Close : Set Rs = Nothing
			Change = True
		End If
	End Function
End Class
%>