<!--#include file="../../Inc/Const.Asp"-->
<%
Dim Plus
Set Plus = New Cls_Plus
Plus.Open("search") ' 打开配置文件
Plus.CheckUser ' 权限检查
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GB2312" />
<title>管理</title>
<link href="../../inc/Images/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="Inc/_Manage.js"></script>
</head>
<body>
<%
Dim Rs,Sql,i,j,GuestbookIDS

Dim ID :ID = Request("ID")
Dim Url : Url = "_Manage.Asp"
Dim Page : Page = Request("Page")
If Len(Page) = 0 Or Not IsNumeric(Page) Then Page = 1
If Page < 1 Then Page = 1

Select Case LCase(Request("Act"))
Case "delete"
	Call Tags_Delete()
Case "dodelete" '150626新增批量删除 
	GuestbookIDS = Replace(Replace(Request("ids"),"'","")," ","") 
	Call DeleteAll(GuestbookIDS)
	Call Tags_Main()
Case "clear"
	Call Tags_Clear()
Case "add"
	GuestbookIDS = Replace(Replace(Request("keys"),"'","")," ","") 
	Call Tags_Add(GuestbookIDS)	
	Call Tags_Main()
Case Else
	Call Tags_Main()
End Select

Sub Tags_Delete()
	If Len(ID) > 0 And IsNumeric(ID) Then Call DB("Delete From [{pre}Tags] Where [ID]=" & ID ,0)
	Conn.Close : Set Conn = Nothing : Response.Redirect Url
End Sub

'批量添加 150627
Sub Tags_Add(Byval GuestbookIDS) 
	dim Ns,str,i
	GuestbookIDS = replace(GuestbookIDS,chr(13),chr(10))
	str = split(GuestbookIDS,chr(10))
	for i = 0 to ubound(str)
		if len(str(i))>1 then
			Set Ns = DB("Select [Name],[Count],[Modifytime] From [{pre}Tags] Where [Name]='" & str(i) & "'",3)
			if Ns.Eof then Ns.AddNew
			Ns(0) = str(i)
			if Len(Ns(1)) = 0  Or IsNull(Ns(1)) then Ns(1) = 1 else Ns(1) = Ns(1) + 1
			Ns(2) = Now()
			Ns.Update : Ns.CLose : Set Ns = Nothing
		end if
	next
	response.Write("添加成功!")
End Sub

'批量删除 执行 150626
Sub DeleteAll(Byval GuestbookIDS)
	Dim Rs
	Set Rs = DB("Select [ID] From [{pre}Tags] Where ID In (" & GuestbookIDS & ")",1)
	Do While Not Rs.Eof
		Call DB("Delete From [{pre}Tags] Where [ID]=" & Rs(0) ,0) '# 删除信息 
		Rs.MoveNext
	Loop
	Rs.Close
End Sub

Sub Tags_Clear()
		'# 清理 Tags　
		On Error Resume Next
		Dim TagsNumber : TagsNumber = Request("t") '# 可保存的 TAGS 条数
		If Len(TagsNumber) = 0 Or Not IsNumeric(TagsNumber) Then TagsNumber = 128 Else TagsNumber = Int(TagsNumber)
		Set Rs = DB("Select [Modifytime] From [{pre}Tags] Order By [Modifytime] Desc",2)
		If Rs.RecordCount > TagsNumber Then
			Rs.AbsolutePosition = TagsNumber
			Select Case LCase(DataBaseType)
			Case "access" Call DB("Delete From [{pre}Tags] Where [Modifytime]<#" & Rs(0) & "#",1)
			Case "mssql" Call DB("Delete From [{pre}Tags] Where [Modifytime]<'" & Rs(0) & "'",1)
			End Select
		End If
		Rs.CLose : Set Rs = Nothing : Conn.Close : Set Conn = Nothing : Response.Redirect Url
End Sub

Sub Tags_Main()
	Set Rs = DB("Select [ID],[Name],[Count],[Modifytime] From [{pre}Tags] Order By [Count] Desc",2)
	%>
	<table width=100% border=0 cellpadding=3 cellspacing=1 class=css_table bgcolor='#E1E1E1'>
	<tr class=css_menu>
	<td colspan=6><table width=100% border=0 cellpadding=4 cellspacing=0 class=css_main_table>
	<form name=frmClear method=post action=<%=Url%>?Act=Clear>
	<tr>
	<td class=css_main><a href="<%=Url%>">站内搜索关键字排行</a> <a href="keywordsToTag.asp" target="_blank">导入文章关键词</a></td>
	<td class=css_search>保留关键字个数 <input name=t type=text id=t size=10 value="128"> <input type=submit name=Submit value=清理关键字> </td>
	</tr>
	</form>
	</table>
    </td>
	</tr>
    
    <form name=frm2 method=post action=_manage.asp>
	<tr>
        <td class='css_top'><input type="checkbox" name="chkall" id="chkall" onclick="CheckAll()" class="checkbox"></td>
        <td class='css_top'>ID</td>
        <td class='css_top'>关键字</td>
        <td class='css_top'>搜索次数</td>
        <td class='css_top'>最后一次</td>
        <td class='css_top'>删除</td>
	</tr>
	<%
	If Rs.Eof Then
	%>
	<tr class=css_norecord>
	<td colspan=6>没有任何记录！</td>
	</tr>
	<%
	Else
	Dim i
	Rs.PageSize = 15
	Rs.AbsolutePage = Page
	For i = 1 To Rs.PageSize
	If Rs.Eof Then Exit For
	%>
	<tr>
        <td class='css_list'><input type="checkbox" name="ids" value="<%=Rs("ID")%>" class="checkbox"></td>
        <td class='css_list'><%=Rs("ID")%></td>
        <td class='css_list'><%=Rs("Name")%></td>
        <td class='css_list'><%=Rs("Count")%></td>
        <td class='css_list'><%=Rs("Modifytime")%></td>
        <td class='css_list'><input name=delete type=button onclick="if(confirm('您确定要删除这条记录吗?')){location.href='<%=Url%>?Act=Delete&ID=<%=Rs("ID")%>&Page=<%=Page%>';}" value=删除></td>
	</tr>
	<%
	Rs.MoveNext
	Next
	End If
	%>

	<tr class=css_page_list>
	<td colspan=6><input name="" type="button" class="inputs" onclick="GuestbookDo('DODelete');" value="批量删除" /> <%=PageList(Rs.PageCount,Page,Rs.RecordCount,15,Url & "?Page={p}")%></td>
	</tr>
	</form>

    <tr class=css_page_list>
	  <td colspan=5> 
      <form name=frm method=post action=_manage.asp?act=add>
      <textarea class='css_textarea' name="keys" type="text" cols="60" rows="6" ></textarea><br /> 
      <input type='submit' name='Submit' value='提交'>
      </form></td>
	  <td><p>批量插入关键词<br />
	    每行一个词<br />
	    每个字至少2个字符<br />
	    重复的会自动过滤 搜索次数上会+1<br />
      然后前台可以用标签调用 相关标签例子如下</p> </td>
	</tr>
    
    <tr class=css_page_list>
      <td colspan=6 align="left"><p><strong>按热门程度调用</strong></p>
        <p>&lt;!--tags:{ $row=6 $table=tags $order=[count] desc}--&gt;<br />
          &lt;a href=&quot;{sys:plusurl}search/index.asp?keyword=[tags:name $function=urlencode]&quot;&gt;[tags:name]&lt;/a&gt;<br />
          &lt;!--tags--&gt;</p>
        <p><strong>按随机调用</strong></p>
        <p>&lt;!--tags:{ $row=6 $Sql=select * from [{pre}tags] where [count]&gt;0 Order by right(cstr(Rnd(-int([ID] +rnd(-timer())*100)))*1000*Now(),2)&nbsp;}--&gt;<br />
  &lt;a href=&quot;{sys:plusurl}search/index.asp?keyword=[tags:name $function=urlencode]&quot;&gt;[tags:name]&lt;/a&gt;<br />
  &lt;!--tags--&gt;</p>
      <p><strong>关键字搜索</strong></p>
      <p>内容页用 {field:keywords $function=tags}&nbsp;列表页用 [page:keywords $function=tags]</p></td>
      </tr>
	</table>
	<%
	Rs.Close : Set Rs = Nothing
	Conn.CLose : Set Conn = Nothing
End Sub
%>
</body>
</html>
<%
Set Plus = Nothing
%>