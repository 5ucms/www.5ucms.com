<!--#Include File="../Inc/Const.Asp"-->
<%Call ChkLogin("login")%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="zh-CN" lang="zh-CN">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
<meta http-equiv="Content-Language" content="zh-CN" />
<meta http-equiv="X-UA-Compatible" content="IE=EDGE" /> 
<meta name="robots" content="none" />
<title><%=webname%> - 站群CMS后台管理系统</title>
<link href="css/admin2.css" rel="stylesheet" type="text/css" />
<script src="js/common.js" type="text/javascript"></script>
<script src="js/c_admin_js_add.js" type="text/javascript"></script>
<link rel="stylesheet" href="css/jquery.bettertip.css" type="text/css" media="screen" />
<script src="js/jquery.bettertip.pack.js" type="text/javascript"></script>
<script src="js/jquery-ui.custom.min.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="css/jquery-ui.custom.css"/>
</head>
<body>
<div id="header">
  <div class="top">
    <div class="logo"><a href="http://www.5ucms.com/" title="5ucms" target="_blank"><img src="images/logo.png" alt="5UCMS"/></a></div>
    <div class="user"> <img src="images/avatar.png" width="40" height="40" id="avatar" alt="Avatar" />
      <div class="username">管理员：<%=getLogin("admin","username")%></div>
      <div class="userbtn"><a class="profile" href="http://www.5ucms.org/help/" title="" target="_blank">操作帮助</a>&nbsp;&nbsp;<a class="logout" href="Login.Asp?Act=Logout" title="">退出登录</a></div>
    </div>
    <div class="menu">
      <ul id="topmenu">
<li id="topmenu1"><a target="main" title="打开后台默认首页 管理插件 查看统计信息" href="admin_main.asp" target="_self">后台首页</a></li>
<li id="topmenu2"><a href="admin_refresh.Asp?Act=ClsCache" target="refresh" title="后台操作完成后需要点击此处使更新生效">更新缓存</a></li>  
<%if createhtml>=1 and Createhtml <= 3 then%><li id="topmenu3"><a href="admin_refresh.Asp?Act=Createindex" target="refresh" title="重新生成首页">更新首页</a></li>  <%end if%> 
<li id="topmenu5"><a title="查看您网站的首页地址 可以系统配置中设定地址" href="<%=Indexpath%>" target="_blank">前台首页</a></li> 
      </ul>
    </div>
  </div>
</div>
<div id="main">
<div class="main_left">
  <ul id="leftmenu">
  
<%If Len(getLogin("admin","managechannel")) > 0 Then%> 
<li id="nav_new"><a target="main" id="aArticleEdt" href="Admin_Content.Asp?Act=Guide"><span>新建文章</span></a></li> 
<li id="nav_article"><a target="main" id="aArticleMng" href="Admin_Content.Asp"><span>文章管理</span></a></li> 
<%End If%>

<%If ChkLevel("diypage") Then%>
<li id="nav_page"><a target="main" id="aPageMng" href="Admin_Diypage.Asp"><span>自定义页</span></a></li>
<%End If%>

<%If ChkLevel("channel") Then%>	
<li id="nav_category"><a target="main" id="aCategoryMng" href="Admin_Channel.Asp"><span>栏目管理</span></a></li>
<%End If%>

<%If ChkLevel("label") Then%>
<li id="nav_tags"><a target="main" id="aTagMng" href="Admin_Label.Asp"><span>标签管理</span></a></li>
<%End If%>

<%If ChkLevel("upload") Then%>
<li id="nav_accessories"><a target="main" id="aCommentMng" href="Admin_Upload.Asp"><span>上传文件</span></a></li>
<%End If%>
 
<%If ChkLevel("sitelink") Then%>				
<li id="nav_comments"><a target="main" id="aFileMng" href="Admin_Sitelink.Asp"><span>内链管理</span></a></li>
<%End If%>

<%If ChkLevel("admin") Then%>
<li id="nav_user"><a target="main" id="aUserMng" href="Admin_Admin.Asp"><span>管理员管理</span></a></li>
<%End If%> 
 
<li id="nav_function"><a target="main" id="aFunctionMng" href="admin_main.asp"><span>插件管理</span></a></li> 

<!--
<li id="nav_file"><a id="aAppcentre" href=""><span style="background-image:url('images/cube1.png')">应用中心</span></a></li>

<li id="newQQConnect"><a id="anewQQConnect" href=""><span style="background-image:url('images/connect_logo_1.png')">QQ互联</span></a></li>
-->

<%If ChkLevel("create") Then%>
<li id="nav_appcentre"><a target="main" id="aSiteFileMng" href="Admin_Createhtml.Asp"><span style="background-image:url('images/folder_1.png')">静态发布</span></a></li>
<%End If%>


<%If ChkLevel("setting") Then%>
<li id="nav_plugin"><a target="main" id="aPlugInMng" href="Admin_Setting.Asp"><span>系统配置</span></a></li>
<%End If%>

<%If ChkLevel("form") Then%>
<li id="nav_themes"><a target="main" id="aThemeMng" href="Admin_form.Asp"><span>自定义模型</span></a></li>
<%End If%>  

  </ul>
  
<iframe src="admin_refresh.asp" width="140" name="refresh" height="50" id="refresh" scrolling="no" frameborder="0" allowtransparency="true"></iframe>

</div>


<div class="main_right">
 

 <iframe src="admin_main.asp" width="100%" name="main" height="800" id="main"  scrolling="auto" frameborder="0" allowtransparency="true"></iframe>
	

</div>

<script language="javascript">window.setInterval("reinitIframe('main')",300);</script> 
 

</body>
</html> 