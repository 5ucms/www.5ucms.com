<%
Class Cls_Special

	Dim vID
	Dim vSpecialName
	Dim vKeywords
	Dim vDescription
	Dim vSpecialPhoto
	Dim vFileName
	Dim vTemplate
	Dim vDateLine
	Dim vOrder
	Dim vFilepath
	Dim LastError

	Private Sub Class_Initialize()
		Call ChkLogin("login")
		Call Initialize()
	End Sub

	Private Sub Class_Terminate()
		Call Initialize()
	End Sub

	Public Function Initialize()
		vID = 0
		vSpecialName = ""
		vKeywords = ""
		vDescription = ""
		vSpecialPhoto = ""
		vFileName = ""
		vTemplate = ""
		vDateLine = Now()
		vOrder = 0
	End Function

	Public Function GetValue()
		vSpecialName = Trim(Request.Form("oSpecialName"))
		vKeywords = Request.Form("oKeywords")
		vDescription = Request.Form("oDescription")
		vSpecialPhoto = Request.Form("oSpecialPhoto")
		vFileName = Request.Form("oFileName")
		vTemplate = Request.Form("oTemplate")
		vOrder = Request.Form("oOrder")
		
		If Len(vOrder) = 0 Or Not IsNumeric(vOrder) Then vOrder = 0
		If Len(vSpecialName) < 1 Or Len(vSpecialName) > 250 Then LastError = "专题名称的长度请控制在 1 至 250 位" : GetValue = False : Exit Function
		If Len(vKeywords) > 200 Then LastError = "关键字的长度请控制在 0 至 200 位" : GetValue = False : Exit Function
		If Len(vKeywords) = 0 Then vKeywords = ""
		If Len(vDescription) = 0 Then LastError = "内容不能为空" : GetValue = False : Exit Function
		If Len(vSpecialPhoto) > 250 Then LastError = "专题图片的长度请控制在 0 至 250 位" : GetValue = False : Exit Function
		If Len(vFileName) = 0 And Len(vFileName) > 50 Then LastError = "文件名称请控制在1 至 50 位" : GetValue = False : Exit Function
		
		' 内容过滤
		vDescription = ReplaceX(vDescription,"(http://" & Request.ServerVariables("server_name") & Installdir & "UploadFile)",Installdir & "UploadFile") '# 过滤下,实在郁闷的FckEditor

		' 远程抓图
		If Request.Form("oRemotepic") = "Save" Then vContent = ReplaceRemoteUrl(vContent)

		' 自定义文件名处理
		vFileName = Replace(vFileName," ","-") : vFileName = Replace(vFileName,"?","") : vFileName = Replace(vFileName,"&","") : vFileName = Replace(vFileName,"=","") : vFileName = Replace(vFileName,"__","_") : vFileName = Replace(vFileName,"__","_") : vFileName = Replace(vFileName,"/","") : vFileName = Replace(vFileName,"\","") : vFileName = Replace(vFileName,"*","") : vFileName = Replace(vFileName,",","")
		
		GetValue = True
	End Function

	Public Function SetValue()
		Dim Rs
		Set Rs = DB("Select * From [{pre}Special] Where [ID]=" & vID,1)
		If Rs.Eof Then Rs.Close : Set Rs = Nothing : LastError = "你所需要查询的记录 " & vID & " 不存在!" : SetValue = False : Exit Function
		vSpecialName = Rs("SpecialName")
		vKeywords = Rs("Keywords")
		vDescription = Rs("Description")
		vSpecialPhoto = Rs("SpecialPhoto")
		vFileName = Rs("FileName")
		vTemplate = Rs("Template")
		vOrder = Rs("Order")
		vDateLine = Rs("DateLine")
		SetValue = True
	End Function

	Public Function Create()
	
  		dim t1:t1=timer()
	
		Dim Rs,Sid
		If Len(vFileName) > 0 Then
			Set Rs = DB("Select [ID] From [{pre}Special] Where [FileName]='" & vFileName & "'",1)
			If Not Rs.Eof Then vFileName = vFileName & "_" & Mid(MD5(Now(),16),5,8)
			Rs.Close
		End If
		Set Rs = DB("Select * From [{pre}Special]",3)
		Rs.addnew
		Rs("SpecialName") = vSpecialName
		Rs("Keywords") = vKeywords
		Rs("Description") = vDescription
		Rs("SpecialPhoto") = vSpecialPhoto
		Rs("FileName") = vFileName
		Rs("Template") = vTemplate
		Rs("Order") = vOrder
		Rs("DateLine") = Now()
		Rs.Update : Rs.Close

		Sid = DB("Select Top 1 [ID] From [{pre}Special] Order By [ID] Desc",1)(0) '# 获取新添加的文章ID编号
			Call CreateSpecial(Sid) '# 创建文件
		Set Rs = Nothing

		Create = True
		
		'response.write formatnumber((timer()-t1),5)
		'response.end
	End Function

	Public Function Modify()
		Dim Rs,DelOldData,DelID,oKeywords
		Set Rs = DB("Select * From [{pre}Special] Where [ID]=" & vID,3)
		If Rs.Eof Then Rs.Close : Set Rs = Nothing : LastError = "你所需要更新的记录 " & vID & " 不存在!" : Modify = False : Exit Function
		oKeywords = Rs("Keywords")
		Rs("SpecialName") = vSpecialName
		Rs("keywords") = vKeywords
		Rs("description") = vDescription
		Rs("FileName") = vFileName
		Rs("Template") = vTemplate
		Rs("SpecialPhoto") = vSpecialPhoto
		Rs("DateLine") = now()
		Rs("Order") = vOrder
		Rs.Update : Rs.Close
		Call CreateSpecial(vID) '# 创建文件
		Modify = True
	End Function

	Public Function Delete()
		Call SetValue()
		vFilepath=DB("Select FileName From [{pre}Special] Where [ID]=" & vID,1)(0)
		If Len(vFilepath)>3 Then Call DeleteFile("/special/"&vFilepath)
		Call DB("Update [{pre}Upload] Set [Aid]=0 Where [Aid]=" & vID ,0) '# 图片需要清理
		Call DB("Delete From [{pre}Special] Where [ID]=" & vID ,0) '# 删除信息
		Delete = True
	End Function

	Public Function Change()
		Dim GetField
		GetField = Request("Field")
		If Instr(LCase("[Commend],[Display]"), LCase("[" & GetField & "]")) = 0 Then LastError = "参数出错,无法修改记录!" : Change = False : Exit Function
		Dim Rs
		Set Rs = DB("Select [" & GetField & "] From [{pre}Content] Where [ID]=" & vID,3)
		If Rs.Eof Then
			Rs.Close : Set Rs = Nothing : LastError ="所需要更新的记录不存在!" : Change = False : Exit Function
		Else
			If Rs(GetField) = 0 Then Rs(GetField) = 1 Else Rs(GetField) = 0
			Rs.Update : Rs.Close : Set Rs = Nothing
			Change = True
		End If
	End Function

	Public Function DeleteAll(Byval ContentIDS)
		Dim Rs
		Set Rs = DB("Select * From [{pre}Special] Where ID In (" & ContentIDS & ")",1)
		Do While Not Rs.Eof
			Call DeleteFile("/special/"&Rs("FileName"))
			Call DB("Delete From [{pre}Special] Where [ID]=" & Rs("ID") ,0) '# 删除信息
			Rs.MoveNext
		Loop
		Rs.Close
	End Function

	Public Function EchoMoveHtml(Byval ContentIDS)
		Response.Write "<table width=100% border=0 cellpadding=3 cellspacing=1 class=css_table bgcolor='#E1E1E1'>"
		Response.Write "<tr class=css_menu>"
		Response.Write "<td colspan=2><table width=100% border=0 cellpadding=4 cellspacing=0 class=css_main_table>"
		Response.Write "<tr>"
		Response.Write "<td class=css_main><a href=#>文章移动</a></td>"
		Response.Write "</tr>"
		Response.Write "</table></td>"
		Response.Write "</tr>"
		Response.Write "<form name=frm method=post action=Admin_Content.Asp?Act=DOMoveit&ID=" & ContentIDS & ">"
		Response.Write "<tr>"
		Response.Write "<td width='200' class='css_list'><div align='right'>目标栏目：</div></td>"
		Response.Write "<td class='css_list'><div align='left'>" & SelectChannel(0," class='css_select' name='Tocid' type='text' id='Tocid' ",getLogin("admin","managechannel")) & "</div></td>"
		Response.Write "</tr>"
		Response.Write "<tr>"
		Response.Write "<td class='css_list'>&nbsp;</td>"
		Response.Write "<td class='css_list'><div align='left'><input type='submit' name='Submit' value='移动'></div></td>"
		Response.Write "</tr>"
		Response.Write "<tr class=css_page_list>"
		Response.Write "<td colspan=2>&nbsp;</td>"
		Response.Write "</tr>"
		Response.Write "</form>"
		Response.Write "</table>"
	End Function 

	Public Function MoveContent(Byval NewCid,Byval ContentIDS)
		If IsNumeric(NewCid) And Len(NewCid) > 0 And NewCid <> 0 Then
			Dim Rs,Ns
			Set Rs = DB("Select [ID],[Cid] From [{pre}Content] Where ID In (" & ContentIDS & ")",3)
			Do While Not Rs.Eof
				If LCase(GetChannel(NewCid,"Table")) <> LCase(GetChannel(Rs(1),"Table")) Then
					Set Ns = DB("Select [Content] From [" & GetChannel(Rs(1),"Table") & "] Where [Aid]=" & Rs(0),1) '# 原表内容
					If Not Ns.Eof Then Tmp = Ns(0) Else Tmp = "" '# 暂存
					Ns.Close
					Set Ns = DB("Select * From [" & GetChannel(NewCid,"Table") & "] Where [Aid]=" & Rs(0),3) '# 查新表
					If Ns.Eof Then Ns.AddNew : Ns("Aid") = Rs(0) '# 转新表
					Ns("Cid") = Tocid : Ns("Content") = Tmp : Ns.Update : Ns.Close '# 更新
					Call DB("Delete From [" & GetChannel(Rs(1),"Table") & "] Where [Aid]=" & Rs(0),0) '# 原表删除
				End If
				Call DB("Update [" & GetChannel(Rs(1),"Table") & "] Set [Cid]=" & NewCid & " Where [Aid]=" & Rs(0),0) '# 更新内容表
				Call DB("Update [{pre}Upload] Set [Cid]=" & NewCid & " Where [Aid]=" & Rs(0),0) '# 更新上传文件表
				Call DB("Update [{pre}Comment] Set [Cid]=" & NewCid & " Where [Aid]=" & Rs(0),0) '# 更新上传文件表
				Rs(1) = NewCid '# 更新
				Rs.Update
				Rs.MoveNext
			Loop
		End If
	End Function
	
End Class
%>