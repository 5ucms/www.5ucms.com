function login()
{
	var username = $("#Username").val();
	var password = $("#Password").val();
	var vcode = $("#SRK_yanzheng").val();
	$.ajax({
		url: "ajax/login.asp?Act=Login&username="+escape(username)+"&password="+password+"&vcode="+vcode+"&rand="+new Date()+"",
		type: 'GET',
		dataType: 'html',
		timeout: 10*1000,
		beforeSend: function(XMLHttpRequest)
		{
			$("#logmsg").html("登录中……");
		},
		error: function(){
			$("#logmsg").html("Error!");
		},
		success: function(data){
			if(data=='success')
			{
				alert('success');
			}else
			{
				$("#logmsg").html(data);
			}
		}
	});
	
}
function LoginOut()
{
	$.ajax({
		url: "ajax/login.asp?Act=Logout&"+new Date()+"",
		type: 'GET',
		dataType: 'html',
		timeout: 10*1000,
		beforeSend: function(XMLHttpRequest)
		{
			$("#logmsg").html("正在注销……");
		},
		error: function(){
			$("#logmsg").html("Error!");
		},
		success: function(data){
			if(data=='success')
			{
				alert('success');
			}else
			{
				$("#logmsg").html(data);
			}
		}
	});
}

function menu(URL)
{
$('#leftFrame',parent.document.body).attr('src',URL)
}

function ChkPost(){
	var vTitle = $("#oTitle").val();
	if (vTitle=="") {alert('文章标题不能为空!');return false;}
	if (frm.oCid.value==0) {alert('请选择栏目!');return false;}
	if (frm.oCid.value==-1) {alert('大栏目不允许发布文章!');return false;}
	if (frm.oCid.value==-2) {alert('你没有当前栏目的管理权限!');return false;}
	frm.submit();
}

function getField()
{
	var FormID = $("#oFormID").val();
	$.ajax({
		url: "ajax.asp?Act=getmyfield&FormID="+FormID+"&"+new Date()+"",
		type: 'GET',
		dataType: 'html',
		timeout: 10*1000,
		beforeSend: function(XMLHttpRequest)
		{
			$("#extmode").attr("style","block");
			$("#myField").html("loading……");
		},
		error: function(){
			$("#extmode").attr("style","block");
			$("#myField").html("Error!");
		},
		success: function(data){
			$("#extmode").attr("style","block");
			if(data=='success')
			{
				alert('success');
			}else
			{
				$("#myField").html(data);
			}
		}
	});
	
}

function updateOrder(action,ID)
{
	if(action=='updateorder')
	{
		var OrderID = $("#OrderID").val();
	}
	$.ajax({
		url: "ajax.asp?Act="+action+"&ID="+ID+"&OrderID="+OrderID+"&rand="+new Date()+"",
		type: 'GET',
		dataType: 'html',
		timeout: 10*1000,
		beforeSend: function(XMLHttpRequest)
		{
			$("#tOrder_"+ID).html("<img src='images/load.gif'>");
		},
		error: function(){
			$("#tOrder_"+ID).html("Error!");
		},
		success: function(data){
				$("#tOrder_"+ID).html(data);
		}
	});
}
// 批量操作  ///////////////////
$(function() { 
   $("#chkall").click(function() { 
		var flag = $(this).attr("checked"); 
		$("[name=ids]:checkbox").each(function() { 
			$(this).attr("checked", flag); 
		}); 
	}); 

	var $subcheck = $("[name=ids]:checkbox"); 
	var $check = $("#chkall"); 
	$subcheck.click(function(){ 
		var flag = true; 
		$subcheck.each(function(){ 
			if(!this.checked) 
			{ 
				flag = false; 
			} 
		}); 
		$check.attr("checked",flag); 
	}); 
});

//保存或恢复数据
function cmsdata(action)
{
	if (action=="save")
	{
		var oEditor = FCKeditorAPI.GetInstance('oContent');
		var oHtml = oEditor.GetXHTML();//获取当前内容
		//$("#saveinfo").html(escape(oHtml));
		//return false;
		$.ajax({
			url: "ajax.asp",
			type: 'POST',
			dataType: 'html',
			data: "Act="+action+"&content="+escape(oHtml)+"&rand="+new Date()+"",
			timeout: 10*1000,
			beforeSend: function(XMLHttpRequest)
			{
				$("#saveinfo").html("<img src='images/load.gif'>");
			},
			error: function(){
				$("#saveinfo").html("保存失败");
			},
			success: function(data){
					$("#saveinfo").html(data);
			}
		});
	}else
	{
		$.ajax({
			url: "ajax.asp?Act="+action+"&rand="+new Date()+"",
			type: 'GET',
			dataType: 'html',
			timeout: 10*1000,
			beforeSend: function(XMLHttpRequest)
			{
				$("#saveinfo").html("<img src='images/load.gif'>");
			},
			error: function(){
				$("#saveinfo").html("恢复失败");
			},
			success: function(data){
					var oEditor = FCKeditorAPI.GetInstance('oContent');
					oEditor.InsertHtml(data);//获取当前内容
					$("#saveinfo").html("恢复成功");
			}
		});
	}
}