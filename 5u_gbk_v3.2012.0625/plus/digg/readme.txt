
安装此插件将在内容表增加字段 Diggs (数字)

如果安装本插件,可以在程序里使用 [标签名:Diggs] 和 {field:Diggs} 在标签里也要吧使用 [diggs] desc 或 [diggs] asc

在文章页Head区需要加入JS代码为(前提你需要载入AJAX.JS)
<style>
.diggArea {padding: 3px 0px 0px 3px; margin:0px 12px 0px 0px; background-image:url('http://127.0.0.2/Images/Default/diggbg2.gif'); width:57px; height:55px; float:left; }
.diggNum {margin:0px; text-align:center; padding:0px; font-size:12px; font-weight:bold; color:#C00 }
.diggLink {text-align:center;font-size:14px;font-weight:bold;margin: 0px;padding: 0px;}
.diggLink a {font-size:14px; font-weight:bold;}
</style>
<script type="text/javascript">
var taget_obj = ""
function Digg(divId,aid){
	taget_obj = document.getElementById(divId+''+aid);
	makeRequest('{sys:installdir}plus/digg/digg.asp?id=' + aid + '&rand=' + Math.random(),"Diggreturn","get","");
}
function Diggreturn(){
	if (http_request.readyState == 4) {
		if (http_request.status == 200) {
			taget_obj.innerHTML = http_request.responseText;
		}
	}
	
}
</script>

在内容区需要加入的代码为
<div id='digg{field:id}' class='diggArea'>
	<div class='diggNum'>{field:diggs}</div>
	<div class="diggLink"><a href="javascript:Digg('digg',{field:id});">顶一下</a></div>
</div>

具体参考 template/default/article.html
