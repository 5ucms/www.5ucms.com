<%@LANGUAGE="VBSCRIPT" CODEPAGE="936"%>
<%
option explicit
response.charset = "gb2312"
session.codepage = 936
session.timeout = 1440
server.scripttimeout = 9999

dim scriptstart
    scriptStart = timer()

dim dataquery
    dataquery = 0

dim language
    language = "zh-cn"

dim webname
    webname = "5u网络"

dim installdir
    installdir = "/"

dim templatedir
    templatedir = "template/company"

dim indexname
    indexname = "首页"

dim indexview
    indexview = "/"

dim indextemplate
    indextemplate = "index.html"

dim indexpath
    indexpath = "/"

dim httpurl
    httpurl = "http://127.0.0.1"

dim defaultext
    defaultext = "html"

dim sitepathsplit
    sitepathsplit = " > "

dim picwatermarkimg
    picwatermarkimg = "/inc/images/watermark.gif"

dim picwatermarkalpha
    picwatermarkalpha = 0.5

dim picwatermarktype
    picwatermarktype = 0

dim createhtml
    createhtml = 0

dim maxpagenum
    maxpagenum = 2000

dim autopinyin
    autopinyin = 1

dim getenglishstate
    getenglishstate = 1

dim remotepic
    remotepic = 1

dim indexpicmode
    indexpicmode = 1

dim prenextmode
    prenextmode = 1

dim changdiyname
    changdiyname = 1

dim descriptionupdate
    descriptionupdate = 1

dim seodir
    seodir = 0

dim templatecache
    templatecache = 0

dim cacheflag
    cacheflag = "9C459F"

dim cachetime
    cachetime = 60

dim rewriteext
    rewriteext = ".html"

dim rewritechannel
    rewritechannel = "channel"

dim rewritecontent
    rewritecontent = ""

dim ccunionid
    ccunionid = 0

dim seotitle
    seotitle = "5u网络,5u建站,网络推广,网站建设"

dim indexkeywords
    indexkeywords = "5u网络,5u网络文章管理系统,ASP,ACCESS,MYSQL,Rewrite"

dim indexdescription
    indexdescription = "5u网络文章管理系统基于ASP+ACCESS/MSSQL 技术开发,免费,开源,程序小巧,功能强大,可用于博客,企业站,信息综合类的建设,使用本系统请保留官方 www.5u.hk 的链接"

dim author
    author = "Admin"

dim source
    source = "本站|http://127.0.0.1"

dim aspjpegobj
	aspjpegobj = False
%>
<!--#include file="language/zh-cn.asp"-->