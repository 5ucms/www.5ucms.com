﻿<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<%
option explicit
response.charset = "UTF-8"
session.codepage = 65001
session.timeout = 1440
server.scripttimeout = 9999

dim scriptstart
    scriptStart = timer()

dim dataquery
    dataquery = 0

dim language
    language = "zh-cn"

dim webname
    webname = "无忧"

dim installdir
    installdir = "/"

dim templatedir
    templatedir = "template"

dim indexname
    indexname = "首页"

dim indexview
    indexview = "index.html"

dim indextemplate
    indextemplate = "index.html"

dim indexpath
    indexpath = "/"

dim httpurl
    httpurl = ""

dim defaultext
    defaultext = ".html"

dim sitepathsplit
    sitepathsplit = ">"

dim picwatermarkimg
    picwatermarkimg = ""

dim picwatermarkalpha
    picwatermarkalpha = 0

dim picwatermarktype
    picwatermarktype = 0

dim createhtml
    createhtml = 1

dim maxpagenum
    maxpagenum = 0

dim autopinyin
    autopinyin = 0

dim getenglishstate
    getenglishstate = 0

dim remotepic
    remotepic = 1

dim indexpicmode
    indexpicmode = 0

dim prenextmode
    prenextmode = 0

dim changdiyname
    changdiyname = 0

dim descriptionupdate
    descriptionupdate = 0

dim seodir
    seodir = 0

dim templatecache
    templatecache = 0

dim cacheflag
    cacheflag = "d8d90d"

dim cachetime
    cachetime = 600

dim rewriteext
    rewriteext = ".html"

dim rewritechannel
    rewritechannel = "channel"

dim rewritecontent
    rewritecontent = ""

dim ccunionid
    ccunionid = 0

dim indexkeywords

dim indexdescription

dim author
    author = "admin"

dim source
    source = "本站"

dim aspjpegobj
	aspjpegobj = False
%>
<!--#include file="language/zh-cn.asp"-->