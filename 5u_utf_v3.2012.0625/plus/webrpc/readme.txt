
说明: 用于ET发布,无忧小助手

ET发布说明: 不要使用模拟发布,如果是用的无忧GB2312版本,在基本设置里打上"使用GB2312字符编码"的勾,ET使用中遇以的问题可以到ET官方论坛 et.zzcity.net 和 无忧论坛 bbs.5u.hk 交流

基本设置:

	文章检查网址: http://127.0.0.3/plus/webrpc/chk.asp?title=%title%

	发布地址: http://127.0.0.3/plus/webrpc/post.asp

参数设置:

	标题参数名: title

	正文参数名: content

	用户参数名: adminname

	密码参数名: adminpass

	来源URL参数名: 本系统未使用
	
	文件列表参数名: 本系统未使用

	附加参数队列:
		cid		栏目ID
		author		文章作者,可为空
		source		文本来源,可为空
		display		值1表示前台直接可显示,值0表示文章默认为隐藏
		keywords	文章关键字,多个用英文状态下的逗号分隔,可为空
		remotepic	值1表示自动保存远程文件,值0表示不保存远程文件
		createhtml	值1表示在生成静态模式下时自动生成HTML页面,值0表示无论如何都不生成

	帐号密码队列:
		后台登录帐号和密码

ET 发布已经官方测试可正常使用~